﻿/*******************************************************************************
 * 作者: hao.wang
 * 时间: 2013-10-23 10:13:33
 * 描述说明:
 *     日志服务契约
 * 更改历史:
 *     
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace ChinaScope.CSF.Common
{
    [ServiceContract(Namespace=Constants.ServiceNamespace)]
    public interface ILogService
    {
        [OperationContract]
        string Health();

        [OperationContract(Name="LogList",IsOneWay=true)]
        void Log(List<AbstractLogInfo> logInfo);

        [OperationContract(Name="LogOne",IsOneWay=true)]
        void Log(AbstractLogInfo logInfo);
    }
}
