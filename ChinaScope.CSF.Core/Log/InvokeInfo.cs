﻿/*******************************************************************************
 * 作者: hao.wang
 * 时间: 2013-10-23 10:13:33
 * 描述说明:
 *     调用信息
 * 更改历史:
 *     
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ChinaScope.CSF.Common
{
    [DataContract(Namespace=Constants.DataContractNamespace)]
    [KnownType(typeof(ApplicationContext))]
    public abstract class InvokeInfo:AbstractLogInfo
    {
        [DataMember]
        public long ExecutionTime { get; set; }

        [DataMember]
        public bool IsSuccessful { get; set; }

        [DataMember]
        public string MethodName { get; set; }

        [DataMember]
        public ApplicationContext ApplicationContext { get; set; }
    }
}
