﻿/*******************************************************************************
 * 作者: hao.wang
 * 时间: 2013-10-23 10:13:33
 * 描述说明:
 *     客户端信息
 * 更改历史:
 *     
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Common
{
    internal interface IClientInfo
    {
        string ClientTypeName { get; set; }
        string ContractName { get; set; }
    }
}
