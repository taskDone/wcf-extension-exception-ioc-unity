﻿/*******************************************************************************
 * 作者: hao.wang
 * 时间: 2013-10-23 10:13:33
 * 描述说明:
 *     异常信息
 * 更改历史:
 *     
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ChinaScope.CSF.Common
{
    [DataContract(Namespace=Constants.DataContractNamespace)]
    public abstract class ExceptionInfo:AbstractLogInfo
    {
        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string StackTrace { get; set; }
    }
}
