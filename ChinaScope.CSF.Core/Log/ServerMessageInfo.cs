﻿/*******************************************************************************
 * 作者: hao.wang
 * 时间: 2013-10-23 10:13:33
 * 描述说明:
 *     服务端消息信息
 * 更改历史:
 *     
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ChinaScope.CSF.Common
{
    [DataContract(Namespace=Constants.DataContractNamespace)]
    public class ServerMessageInfo:MessageInfo,IServerInfo
    {
        [DataMember]
        public string ServiceName { get; set; }
    }
}
