﻿/*******************************************************************************
 * 作者: hao.wang
 * 时间: 2013-10-23 10:13:33
 * 描述说明:
 *     本地日志服务
 * 更改历史:
 *     
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using System.Diagnostics;

namespace ChinaScope.CSF.Common
{
    public class LocalLogService:ILogService
    {
        private Logger logger;
        private static object locker = new object();
        public LocalLogService(string logName)
        {
            logger = LogManager.GetLogger(logName);
        }
        public string Health()
        {
            throw new NotImplementedException("本地日志服务不支持此操作!");
        }

        public void Log(List<AbstractLogInfo> logInfo)
        {
            logInfo.ForEach(log => Log(log));
        }

        public void Log(AbstractLogInfo logInfo)
        {
            if (logInfo is ExceptionInfo)
            {
                logger.Error(logInfo);
            }
            else
            {
                logger.Info(logInfo);
            }
        }

        public static void Log(string text)
        {
            lock (locker)
            {
                //Debug.WriteLine("-------------------Start-------------------");
                //Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                //Debug.WriteLine(text);
                //Debug.WriteLine("-------------------End-------------------");
                Console.WriteLine();
                Console.WriteLine("-------------------Start-------------------");
                Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                Console.WriteLine(text);
                Console.WriteLine("-------------------End-------------------");
                Console.WriteLine();
            }
        }
    }
}
