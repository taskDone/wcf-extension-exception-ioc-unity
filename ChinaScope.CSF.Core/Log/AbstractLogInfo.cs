﻿/*******************************************************************************
 * 作者: hao.wang
 * 时间: 2013-10-23 10:13:33
 * 描述说明:
 *     日志类
 * 更改历史:
 *     
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ChinaScope.CSF.Common
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    [KnownType(typeof(StartInfo))]
    [KnownType(typeof(ClientStartInfo))]
    [KnownType(typeof(ServerStartInfo))]
    [KnownType(typeof(InvokeInfo))]
    [KnownType(typeof(ClientInvokeInfo))]
    [KnownType(typeof(ServerInvokeInfo))]
    [KnownType(typeof(MessageInfo))]
    [KnownType(typeof(ClientMessageInfo))]
    [KnownType(typeof(ServerMessageInfo))]
    [KnownType(typeof(ExceptionInfo))]
    [KnownType(typeof(ClientExceptionInfo))]
    [KnownType(typeof(ServerExceptionInfo))]
    public abstract class AbstractLogInfo
    {
        [DataMember]
        public string ID { get; set; }

        [DataMember]
        public string RequestIdentity { get; set; }

        [DataMember]
        public DateTime Time { get; set; }

        [DataMember]
        public string MachineName { get; set; }

        [DataMember]
        public string MachineIP { get; set; }

        [DataMember]
        public string ExtraInfo { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            this.GetType().GetProperties().ToList().ForEach(p =>
                {
                    var oValue = p.GetValue(this, null);
                    sb.AppendLine(p.Name + ": " + oValue);
                    if (oValue is Dictionary<string,string>)
                    {
                        var dic = oValue as Dictionary<string, string>;
                        foreach (var item in dic)
                        {
                            sb.AppendLine(" " + item.Key + ": " + item.Value);
                        }
                    }
                });
            return sb.ToString();
        }
    }
}
