﻿/*******************************************************************************
 * 作者: hao.wang
 * 时间: 2013-10-23 10:13:33
 * 描述说明:
 *     服务端异常信息
 * 更改历史:
 *     
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ChinaScope.CSF.Common
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class ServerExceptionInfo:ExceptionInfo,IServerInfo
    {
        [DataMember]
        public string ServiceName { get; set; }

        [DataMember]
        public string InnerException { get; set; }
    }
}
