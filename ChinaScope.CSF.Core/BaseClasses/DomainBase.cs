﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Common.Infrastrcture;

namespace ChinaScope.CSF.Common
{
    public abstract class DomainBase : MarshalByRefObject
    {
    }
    public abstract class DomainBase<TDomain> : DomainBase
    {
        public TDomain Domain { get; private set; }

        public DomainBase(TDomain domain)
        {
            this.Domain = domain;
            
        }
    }
}
