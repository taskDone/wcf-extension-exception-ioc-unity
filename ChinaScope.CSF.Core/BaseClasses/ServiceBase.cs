﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Common
{
    public abstract class ServiceBase : MarshalByRefObject
    {
    }
    public abstract class ServiceBase<TDao> : ServiceBase
    {
        public TDao Dao { get; private set; }
        public ServiceBase(TDao dao)
        {
            this.Dao = dao;
        }
    }
}
