﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Common.Extensions;

namespace ChinaScope.CSF.Common
{
    public abstract class ServiceProxyBase:MarshalByRefObject
    {
    }
    public abstract class ServiceProxyBase<TChannel> : ServiceProxyBase
    {
        public OperationInvoker<TChannel> Invoker { get; private set; }
        public ServiceProxyBase(string endpointConfigurationName)
        {
            this.Invoker = new OperationInvoker<TChannel>(endpointConfigurationName);
        }
    }
}
