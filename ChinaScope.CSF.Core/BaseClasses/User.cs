﻿using System;

namespace ChinaScope.CSF.Common.BaseClasses
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks></remarks>
    [Serializable]
    public class User
    {
        /// <summary>
        /// 
        /// </summary>
        public static readonly User Invalid = new User("");

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <remarks></remarks>
        public User()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> class.
        /// </summary>
        /// <param name="code">The code.</param>
        /// <remarks></remarks>
        public User(string code)
        {
            Code = code;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        /// <remarks></remarks>
        public string Name { get; set; }


        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        /// <remarks></remarks>
        public string PassWord { get; set; }

        /// <summary>
        /// Gets the code.
        /// </summary>
        /// <remarks></remarks>
        public string Code { get; set; }

        /// <summary>
        /// Gets the code.
        /// </summary>
        /// <remarks></remarks>
        public string Environment { get; set; }

        public string Range { get; set; }

        /// <summary>
        /// Gets or sets the role.
        /// </summary>
        /// <value>The role.</value>
        /// <remarks></remarks>
        public int? Role { get; set; }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">与当前的 <see cref="T:System.Object"/> 进行比较的 <see cref="T:System.Object"/>。</param>
        /// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.</returns>
        /// <remarks></remarks>
        public override bool Equals(object obj)
        {
            var rhs = obj as User;
            if (null != rhs)
            {
                return Code == rhs.Code;
            }
            return false;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>A <see cref="System.String"/> that represents this instance.</returns>
        /// <remarks></remarks>
        public override string ToString()
        {
            return string.Format("{0}_{1}_{2}", Name, Code, (int) Role);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
        /// <remarks></remarks>
        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified user is valid.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns><c>true</c> if the specified user is valid; otherwise, <c>false</c>.</returns>
        /// <remarks></remarks>
        public static bool IsValid(User user)
        {
            return !Invalid.Equals(user);
        }

        /// <summary>
        /// Parses the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static User Parse(string path)
        {
            return Common.GetUserFromFile(path);
        }
    }
}