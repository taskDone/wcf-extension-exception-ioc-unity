﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Common.Infrastrcture
{
    public interface IDependencyResolver : IDisposable
    {
        void Register<T>(T instance);
        void Inject<T>(T existing);
        //T Resolve<T>(Type type);
        T Resolve<T>(Type type, string name);
        T Resolve<T>();
        T Resolve<T>(string name);
        object Resolve(Type type);
        IEnumerable<T> ResolveAll<T>();
    }
}
