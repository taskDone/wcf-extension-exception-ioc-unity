﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Common.Infrastrcture
{
    public static class IoC
    {
        private static IDependencyResolver _resolver;

        public static void InitializeWith(IDependencyResolverFactory factory)
        {
            _resolver = factory.CreateInstance();
        }

        public static void Register<T>(T instance)
        {
            _resolver.Register(instance);
        }

        public static void Inject<T>(T existing)
        {
            _resolver.Inject(existing);
        }

        //public static T Resolve<T>(Type type)
        //{
        //    return _resolver.Resolve<T>(type);
        //}
        public static object Resolve(Type type)
        {
            return _resolver.Resolve(type);
        }
        public static T Resolve<T>(Type type, string name)
        {
            return _resolver.Resolve<T>(type, name);
        }

        public static T Resolve<T>()
        {
            return _resolver.Resolve<T>();
        }

        public static T Resolve<T>(string name)
        {
            return _resolver.Resolve<T>(name);
        }

        public static IEnumerable<T> ResolveAll<T>()
        {
            return _resolver.ResolveAll<T>();
        }

        public static void Reset()
        {
            if (_resolver != null)
            {
                _resolver.Dispose();
            }
        }
    }
}
