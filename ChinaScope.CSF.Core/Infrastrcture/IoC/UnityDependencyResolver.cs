﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Microsoft.Practices.Unity.InterceptionExtension;
using System.Configuration;
using System.Collections.ObjectModel;

namespace ChinaScope.CSF.Common.Infrastrcture
{
    public class UnityDependencyResolver : DisposableResource, IDependencyResolver
    {
        private readonly IUnityContainer _container;

        public UnityDependencyResolver()
            : this(new UnityContainer().AddNewExtension<Interception>())
        {
            UnityConfigurationSection configuration = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            configuration.Configure(_container);
        }

        public UnityDependencyResolver(IUnityContainer container)
        {
            _container = container;
        }

        public void Register<T>(T instance)
        {
            _container.RegisterInstance(instance);
        }

        public void Inject<T>(T existing)
        {
            _container.BuildUp(existing);
        }

        //public T Resolve<T>(Type type)
        //{
        //    return (T)_container.Resolve(type);
        //}

        public T Resolve<T>(Type type, string name)
        {
            return (T)_container.Resolve(type, name);
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public T Resolve<T>(string name)
        {
            return _container.Resolve<T>(name);
        }
        public object Resolve(Type type)
        {
            return _container.Resolve(type);
        }
        public IEnumerable<T> ResolveAll<T>()
        {
            IEnumerable<T> namedInstances = _container.ResolveAll<T>();
            T unnamedInstance = default(T);

            try
            {
                unnamedInstance = _container.Resolve<T>();
            }
            catch (ResolutionFailedException)
            {
            }

            if (Equals(unnamedInstance, default(T)))
            {
                return namedInstances;
            }

            return new ReadOnlyCollection<T>(new List<T>(namedInstances) { unnamedInstance });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _container.Dispose();
            }

            base.Dispose(disposing);
        }


        
    }
}
