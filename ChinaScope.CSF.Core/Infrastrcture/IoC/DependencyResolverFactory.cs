﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace ChinaScope.CSF.Common.Infrastrcture
{
    public class DependencyResolverFactory : IDependencyResolverFactory
    {
        private readonly Type _resolverType;

        //public DependencyResolverFactory(string resolverTypeName)
        //{
        //    _resolverType = Type.GetType(resolverTypeName, true, true);
        //}
        public DependencyResolverFactory(Type resolverType)
        {
            _resolverType = resolverType;
        }
        //public DependencyResolverFactory()
        //    : this(ConfigurationManager.AppSettings["dependencyResolverTypeName"])
        //{
        //}
        public DependencyResolverFactory():this(typeof(UnityDependencyResolver))
        {

        }

        public IDependencyResolver CreateInstance()
        {
            try
            {
                return Activator.CreateInstance(_resolverType) as IDependencyResolver;
            }
            catch (Exception exception)
            {
                
            }
            return null;
        }
    }
}
