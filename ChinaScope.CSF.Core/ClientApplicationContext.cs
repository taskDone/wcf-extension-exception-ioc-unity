﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Remoting.Messaging;

namespace ChinaScope.CSF.Common
{
    [CollectionDataContract(Namespace=Constants.DataContractNamespace,ItemName="Context")]
    public class ClientApplicationContext:ApplicationContext
    {
        public string ServerMachineName
        {
            get { return base["ServerMachineName"]; }
            set { base["ServerMachineName"] = value; }
        }

        public string ServerVersion
        {
            get { return base["ServerVersion"]; }
            set { base["ServerVersion"] = value; }
        }

        public static ClientApplicationContext Current
        {
            get { return CallContext.GetData(CallContextKey) as ClientApplicationContext; }
            set { CallContext.SetData(CallContextKey, value); }
        }
    }
}
