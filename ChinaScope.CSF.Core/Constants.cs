﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Common
{
    public class Constants
    {
        public const string DataContractNamespace = "http://www.chinascopefinancial.com/csf";
        public const string ServiceContractNamespace = "http://www.chinascopefinancial.com/csf";
        public const string ServiceNamespace = "http://www.chinascopefinancial.com/csf";
    }
}
