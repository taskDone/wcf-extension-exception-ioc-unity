﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Activation;

namespace ChinaScope.CSF.Common.Extensions
{
    public class ExceptionHandlingServiceHostFactory : ServiceHostFactory
    {
        protected override System.ServiceModel.ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            return new ExceptionHandlingServiceHost(serviceType, baseAddresses);
        }
    }
}
