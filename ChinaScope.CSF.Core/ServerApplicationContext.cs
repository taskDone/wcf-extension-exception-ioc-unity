﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Remoting.Messaging;

namespace ChinaScope.CSF.Common
{
    [CollectionDataContract(Namespace = Constants.DataContractNamespace, ItemName = "Context")]
    public class ServerApplicationContext : ApplicationContext
    {
        public string ClientMachineName
        {
            get { return base["ClientMachineName"]; }
            set { base["ClientMachineName"] = value; }
        }

        public string ClientVersion
        {
            get { return base["ClientVersion"]; }
            set { base["ClientVersion"] = value; }
        }

        public static ServerApplicationContext Current
        {
            get { return CallContext.GetData(CallContextKey) as ServerApplicationContext; }
            set { CallContext.SetData(CallContextKey, value); }
        }
    }
}
