﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Common
{
    public interface IBootstrapperTask
    {
        void Execute();
    }
}
