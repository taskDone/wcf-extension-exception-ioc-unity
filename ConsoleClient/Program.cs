﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ChinaScope.CSF.Contract.ServiceInterface;

namespace ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ChannelFactory<IStockService> channelFactory = new ChannelFactory<IStockService>("shardholderservice"))
            {
                IStockService proxy = channelFactory.CreateChannel();
                using (proxy as IDisposable)
                {
                    var stock = proxy.GetStockByTicker("600600");
                    Console.WriteLine(stock.CompanyName.cn);
                    Console.ReadKey();
                }
            }
        }
    }
}
