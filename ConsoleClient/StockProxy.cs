﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.ServiceInterface;
using ChinaScope.CSF.Contract.UIDeriveEntity.Stock;

namespace ConsoleClient
{
    public class StockProxy : ServiceProxyBase<IStockService>, IStockService
    {
        public StockProxy()
            : base("stockservice")
        {

        }

        public ChinaScope.CSF.Contract.Entity.ShareHolder.Stock GetStockByTicker(string ticker)
        {
            return this.Invoker.Invoke(p => p.GetStockByTicker(ticker));
        }


        public List<DeriveStock> GetStocksByConditions(string ticker, string secu, string cov, string market,
            string stockstatus, string adr,
            string stat, string uptStart, string uptEnd)
        {
            return
                this.Invoker.Invoke(
                    p => p.GetStocksByConditions(ticker, secu, cov, market, stockstatus, adr, stat, uptStart, uptEnd));
        }


        public DeriveStock GetItemByCode(string code)
        {
            return this.Invoker.Invoke(p => p.GetItemByCode(code));
        }

        public bool SaveItems(List<DeriveStock> saveList)
        {
            return this.Invoker.Invoke(p => p.SaveItems(saveList));
        }


        public List<IDCNEN> GetOrglstByName(string name)
        {
            return this.Invoker.Invoke(p => p.GetOrglstByName(name));
        }


        public List<ChinaScope.CSF.Contract.Entity.DictEntity.DictMarket> GetAllDictMarket()
        {
            return this.Invoker.Invoke(p => p.GetAllDictMarket());
        }


        public List<DeriveStock> GetAllStocks()
        {
            return this.Invoker.Invoke(p => p.GetAllStocks());
        }


        public IDCNEN GetOrgByName(string name)
        {
            return this.Invoker.Invoke(p => p.GetOrgByName(name));
        }


        public List<ChinaScope.CSF.Contract.Entity.DictEntity.DictListingStatus> GetAllDictMarketStatus()
        {
            return this.Invoker.Invoke(p => p.GetAllDictMarketStatus());
        }


        public List<OrgTick> GetOrgidAndTickByName(string name)
        {
            return this.Invoker.Invoke(p => p.GetOrgidAndTickByName(name));
        }


        public List<DeriveStock> GetAllUnPublishItems()
        {
            return this.Invoker.Invoke(p => p.GetAllUnPublishItems());
        }


        public List<DeriveStock> GetItemsBybid(string bid)
        {
            return this.Invoker.Invoke(p => p.GetItemsBybid(bid));
        }
    }
}
