﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Reflection;
using System.ServiceModel.Configuration;
using System.ServiceModel;
using ChinaScope.CSF.Common.Infrastrcture;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Dao.Common;
using ChinaScope.CSF.Dao.InterMediary;
using ChinaScope.CSF.Dao.Portfolio;
using ChinaScope.CSF.Dao.ResearchReport;

namespace ChinaScope.CSF.Console.Host
{
    class CSFService
    {
        static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        
        List<ServiceHost> hosts = null;

        public void Initialization()
        {
            Bootstrapper.Run();
        }

        public void Start()
        {
            hosts = new List<ServiceHost>();

            // 配置文件中，读取system.serviceModel节点，加载服务
            Configuration conf = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            ServiceModelSectionGroup svcmode = (ServiceModelSectionGroup) conf.GetSectionGroup("system.serviceModel");
            string serviceNameSpace = "ChinaScope.CSF.Service";

            foreach (ServiceElement el in svcmode.Services.Services)
            { 
                Type svcType = Type.GetType(el.Name + "," + serviceNameSpace);
                if (svcType == null)
                    throw new Exception("Invalid Service Type" + el.Name + "in configuration file.");

                var host = new ServiceHost(svcType);
                hosts.Add(host);
            }

            foreach (var h in hosts)
            {
                string serviceName = h.Description.ServiceType.FullName;
                h.Opened += (sender, e) =>
                {
                    logger.Info("{0} 已经启动", serviceName);
                };
                h.Closed += (sender, e) =>
                {
                    logger.Info("{0} 已经关闭", serviceName);
                };
                h.Open();
            }

            #region 初始化缓存变量(全局缓存，以后可以直接存在缓存中)

            if (CachesCollection.ShareHolderTypesCaches == null)
            {
                BasePortfolioDao dao = new BasePortfolioDao();
                CachesCollection.ShareHolderTypesCaches = dao.GetAllShareholerTyps();
            }
            if (CachesCollection.ResearchOrgs == null)
            {
                ResearchOrgDao dao = new ResearchOrgDao();
                CachesCollection.ResearchOrgs = dao.SelectResearchOrg();
            }
            if (CachesCollection.InterMediaryTyps == null)
            {
                DictIntermediaryTypeDao dao = new DictIntermediaryTypeDao();
                CachesCollection.InterMediaryTyps = dao.GetAllmediaryTyps();
            }
            if (CachesCollection.BaseStocks == null)
            {
                BaseStockDao dao = new BaseStockDao();
                CachesCollection.BaseStocks = dao.GetAll();
            }

            #endregion
        }


        public void Stop()
        {
            if (hosts != null && hosts.Count >0)
            {
                foreach (var h in hosts)
                {
                    h.Close();
                }
            }
        }
    }
}
