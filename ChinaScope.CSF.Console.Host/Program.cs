﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Dao.Portfolio;
using Topshelf;

namespace ChinaScope.CSF.Console.Host
{
    class Program
    {
        static readonly string ServiceName = "CSF_WCF_Service";
        static readonly string ServiceDisplay = "VSTO端WCF服务";
        static readonly string ServiceDesc = "此服务承载WCF数据服务";

        private static void Main(string[] args)
        {
            var host = HostFactory.New(x =>
                {
                    x.Service<CSFService>(s=>
                        {
                            s.ConstructUsing(building =>
                            {
                                var service = new CSFService();
                                service.Initialization();
                                return service;
                            });
                            s.WhenStarted(service => service.Start());
                            s.WhenStopped(service => service.Stop());
                        });
                    x.RunAsLocalSystem();
                    x.SetServiceName(ServiceName);
                    x.SetDisplayName(ServiceDisplay);
                    x.SetDescription(ServiceDesc);
                });
            host.Run();

            #region 一个端口一个服务方式

            //comps
            //using (ServiceHost host1 = new ServiceHost(typeof(CompsService)))
            //{
            //    host1.Open();
            //    System.Console.WriteLine("StockService Started...");

            //}

            //conerstone
            //using (ServiceHost host = new ServiceHost(typeof (ConerStoneService)))
            //{
            //    host.Opened += (sender, e) =>
            //    {
            //        System.Console.WriteLine("StockService Started...");
            //    };
            //    host.Open();

            //    //portfolio
            //    //using (ServiceHost host2 = new ServiceHost(typeof(PortfolioService)))
            //    //{
            //    //    host2.Open();
            //    //    System.Console.ReadKey();
            //    //}

            //    System.Console.ReadKey();
            //}

            #endregion

            #region 多个服务方式

            //Configuration conf = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            //    //改这句,指定的服务配置文件

            //ServiceModelSectionGroup svcmode = (ServiceModelSectionGroup) conf.GetSectionGroup("system.serviceModel");

            //ServiceHost hhost = null;
            //foreach (ServiceElement el in svcmode.Services.Services)
            //{
            //    string serviceNameSpace = "ChinaScope.CSF.Service"; //el.Name.Split('.')[0];
            //    Type svcType = Type.GetType(el.Name + "," + serviceNameSpace);
            //    if (svcType == null)
            //        throw new Exception("Invalid Service Type" + el.Name + "in configuration file.");

            //    hhost = new ServiceHost(svcType);

            //    hhost.Opened += delegate
            //    {
            //        System.Console.WriteLine(el.Name + "服务已经启动了");

            //    };

            //    hhost.Open();

            //}
            //System.Console.Read();


            #endregion

        }
    }
}
