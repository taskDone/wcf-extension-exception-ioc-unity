﻿/*******************************************************************************
 * 作者: hao.wang
 * 时间: 2013-10-30 15:29:21
 * 描述说明:
 *     记录日志的配置类
 * 更改历史:
 *     
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using System.Runtime.CompilerServices;
using System.Web.Caching;
using System.Xml;
using System.Xml.Linq;

namespace ChinaScope.CSF.LogCenter
{
    internal class ConfigProvider
    {
        private static readonly string LogCenterConfigFile = "LogServer.config";
        private static readonly string CacheConfigFilePathDependencyKey = "CacheConfigFilePathDependencyKey";

        [MethodImpl(MethodImplOptions.Synchronized)]
        public static void Init()
        {
            try
            {
                string configFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, LogCenterConfigFile);
                HttpRuntime.Cache.Insert(CacheConfigFilePathDependencyKey, "",
                    new CacheDependency(configFilePath),
                    Cache.NoAbsoluteExpiration,
                    Cache.NoSlidingExpiration,
                    CacheItemPriority.Default,
                    LogCenterConfigUpdate);
                XDocument xDoc = XDocument.Load(configFilePath);
                XElement xLogServer = xDoc.Root.Element("LogServer");
                if (xLogServer != null)
                {
                    if (xLogServer.Element("WorkThreadCount") != null)
                    {
                        int.TryParse(xLogServer.Element("WorkThreadCount").Value, out ConfigProvider.WorkThreadCount);
                    }

                    if (xLogServer.Element("WorkThreadSleepInterval") != null)
                    {
                        int.TryParse(xLogServer.Element("WorkThreadSleepInterval").Value, out ConfigProvider.WorkThreadSleepInterval);
                    }

                    if (xLogServer.Element("SubmitToServerBatchSize") != null)
                    {
                        int.TryParse(xLogServer.Element("SubmitToServerBatchSize").Value, out ConfigProvider.SubmitToServerBatchSize);
                    }

                    if (xLogServer.Element("MemoryQueueMaxCount") != null)
                    {
                        int.TryParse(xLogServer.Element("MemoryQueueMaxCount").Value, out ConfigProvider.MemoryQueueMaxCount);
                    }
                    if (xLogServer.Element("MongoConnectionString") != null)
                    {
                        ConfigProvider.MongoConnectionString = xLogServer.Element("MongoConnectionString").Value;
                    }
                }
            }
            catch (Exception)
            {
                
            }
        }
        private static void LogCenterConfigUpdate(string key, object value, CacheItemRemovedReason resean)
        {
            Init();
        }
        /// <summary>
        /// 工作线程数
        /// </summary>
        public static int WorkThreadCount = Environment.ProcessorCount * 2;

        /// <summary>
        /// 工作线程休眠时间，单位毫秒
        /// </summary>
        public static int WorkThreadSleepInterval = 50;

        /// <summary>
        /// 批量提交记录的大小
        /// </summary>
        public static int SubmitToServerBatchSize = 50;

        /// <summary>
        /// 内存队列最大记录数
        /// </summary>
        public static int MemoryQueueMaxCount = 500;

        public static string MongoConnectionString = "";
    }
}
