﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace ChinaScope.CSF.LogCenter
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.Title = "LogCenter";
            MemoryQueue.Start();

            using (ServiceHost host = new ServiceHost(typeof(LogService)))
            {
                host.Opened += (sender, e) =>
                    {
                        Console.WriteLine("LogService is opened...");
                    };
                host.Open();
                Console.Read();
            }
        }
    }
}
