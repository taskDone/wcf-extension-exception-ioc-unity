﻿/*******************************************************************************
 * 作者: hao.wang
 * 时间: 2013-10-29 17:32:27
 * 描述说明:
 *     
 * 更改历史:
 *     
*******************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using ChinaScope.CSF.Common;

namespace ChinaScope.CSF.LogCenter
{
    internal class MongoDBAccess
    {
        private static MongoServer MongoDBConn = MongoServer.Create(new MongoUrl("mongodb://192.168.0.224/:27017"));

        public void SaveData(List<AbstractLogInfo> data)
        {
            try
            {
                string dbName = "VstoLog";
                MongoDatabase db = MongoDBConn.GetDatabase(dbName);
                foreach (var item in data)
                {
                    string collectionName = "";
                    try
                    {
                        collectionName = item.GetType().Name;
                        MongoCollection<AbstractLogInfo> categories = db.GetCollection<AbstractLogInfo>(collectionName);
                        categories.Insert<AbstractLogInfo>(item);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("库:" + dbName + " 表:" + collectionName + " 出现异常." + ex.ToString());
                        Console.WriteLine("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            }
        }
    }
}
