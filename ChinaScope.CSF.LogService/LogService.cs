﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using ChinaScope.CSF.Common;
using System.Diagnostics;

namespace ChinaScope.CSF.LogCenter
{
    [ServiceBehavior(IncludeExceptionDetailInFaults=true,
        ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode= InstanceContextMode.PerCall,
        Namespace = Constants.ServiceContractNamespace)]
    public class LogService:ILogService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public string Health()
        {
            return "OK";
        }

        public void Log(List<AbstractLogInfo> logInfo)
        {
            //// TODO 目前精简版
            //Console.WriteLine("===============Log START===============");
            //Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + logInfo.First().GetType().Name);
            //logInfo.ForEach(Console.WriteLine);
            //Console.WriteLine("===============Log End===============");

            //Trace.WriteLine("++++++++++++++++++++++++");
            //logger.Info("===============Log START===============");
            //logger.Info(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + logInfo.First().GetType().Name);
            //logInfo.ForEach(logger.Info);
            //logger.Info("===============Log End===============");
            if (logInfo == null || logInfo.Count == 0)
            {
                return;
            }
            MemoryQueue.BatchEnqueue(logInfo);
        }

        public void Log(AbstractLogInfo logInfo)
        {
            Log(new List<AbstractLogInfo> { logInfo });
        }
    }
}
