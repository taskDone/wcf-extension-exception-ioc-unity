﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.CorpNature;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using ChinaScope.CSF.Contract.Entity.Sam;
using DictIndustry = ChinaScope.CSF.Contract.Entity.DictEntity.DictIndustry;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface ISamService
    {
        [OperationContract]
        FinSamStyle GetSamStyle(string argTik, string argRpt);

        [OperationContract]
        IEnumerable<FinSamProduct> GetSamProducts(string argTik, string argRpt, string argQt, int argCtyp,
            int argStdTyp, int argLimit);

        [OperationContract]
        FinCorpNature GetCorpByTick(string argTik);

        [OperationContract]
        IEnumerable<FinSamProduct> GetSamProductsByYear(string argSecu, string argRpt, string argQt, string argYear,
            int argCtyp, int argStdTyp);

        [OperationContract]
        FinSamProduct GetSamProductByDate(string argSecu, string argRpt, string argDate, string argMonth,
            string argQuarter,
            string argYear, int argCtyp, int argStdTyp);

        [OperationContract]
        FinSamProduct GetSamProduct(long id);

        [OperationContract]
        List<DictSamProduct> GetAllSamProducts();

        [OperationContract]
        List<DictSamProductDef> GetAllSamProductDefs();

        [OperationContract]
        string InsertSamProductDef(DictSamProductDef argDef);

        [OperationContract]
        void SaveSamProduct(DictSamProduct argSamProduct);

        [OperationContract]
        DictSamProductDef GetSamProductDefByNameAndSecu(string argName, string argSecu);

        [OperationContract]
        DictSamProduct GetSamProductByCode(string argCode);

        [OperationContract]
        string GetSamProductNameByCode(string argCode);

        [OperationContract]
        List<DictModel> GetDicts(string tableName);

        [OperationContract]
        long GetSequence(string argTable);

        [OperationContract]
        void InsertStyle(FinSamStyle argStyle);

        [OperationContract]
        void SaveStyle(FinSamStyle argStyle);

        [OperationContract]
        string GetFp(string fy, string y);

        [OperationContract]
        void SaveFinSamProduct(List<FinSamProduct> argList);

        [OperationContract]
        List<DictIndustry> GetAllIndustries();

        [OperationContract]
        bool DeleteFinSamProduct(string argId);

        [OperationContract]
        string IsUnLocked(string rpt, string ticker, int consolidation, int type, string eqenvironment);

        [OperationContract]
        string InsertLockedRecords(VstoDsLock insertitem);

        [OperationContract]
        string ReleaseLock(string rpt, string ticker, int consolidation, int type, string environment);

        [OperationContract]
        void RemoveLockById(List<string> argId);

        [OperationContract]
        List<DictModel> GetSysDicts(string argTable);

        [OperationContract]
        List<VstoDsLock> GetAllLockItems(string rpt, string ticker, int consolidation, int type);

        [OperationContract]
        List<MonVstoDsFileInfo> GetFileInfo(string ticker, string rpt, int consolidation, int typ, string envir);

        [OperationContract]
        List<DictIndexReason> GetSamReason(int type);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="flag">0:insert,1:update</param>
        /// <returns></returns>
        [OperationContract]
        bool SaveSamReason(DictIndexReason reason,int flag);

        [OperationContract]
        bool SaveIndexChangeMessage(List<IndexChangeMsg> indexChangeMsgs);

        [OperationContract]
        List<IndexChangeMsg> GetIndexChangeMessageNoCalc();

        [OperationContract]
        bool SaveIndexValuation(List<IndexValuation> indexChangeMsgs);

        [OperationContract]
        Tuple<double, double> GetFinRevenueMargin(FinSamProduct finSamProduct, string itemValue, string code);

        [OperationContract]
        Tuple<double, double> GetLatelyFinRevenueMargin(string secu, string rpt);

        [OperationContract]
        Tuple<double, double> GetLatelyRevenueMargin(string argTik, string argRpt, string code);
    }
}
