﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.ServiceModel;

using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.PEVC;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using ChinaScope.CSF.Contract.Entity.ShareHolder;
using ChinaScope.CSF.Contract.UIDeriveEntity.CED;
using ChinaScope.CSF.Contract.UIDeriveEntity.Dict;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    /// <summary>
    /// Dict表所有相关接口服务均可以写在此类中
    /// </summary>
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IDictService
    {
        #region DictCommon相关接口  
        
        /// <summary>
        /// 获取整张数据表数据
        /// </summary>
        [OperationContract]
        List<DeriveDictCommon> GetAll();

        /// <summary>
        /// 按照查询条件进行数据筛选
        /// </summary>
        [OperationContract]
        List<DeriveDictCommon> GetDictsByCondition(string name, string cat, string stat);

        /// <summary>
        /// 根据类别cat获取其类别下的所有分类
        /// </summary>
        //[OperationContract]
        //List<DeriveDictCommon> GetAllByCat(string cat);

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="saveList">保存的数据列表</param>
        /// <returns></returns>
        [OperationContract]
        bool SaveItems(List<DeriveDictCommon> saveList);

        /// <summary>
        /// 根据编码code获取单条数据，主要用于添加时判重
        /// </summary>
        [OperationContract]
        List<DictCommon> GetCommonsByCode(string code);

        /// <summary>
        /// 根据类别cat获取其类别下的所有分类
        /// </summary>
        /// <param name="cat">分类</param>
        /// <returns></returns>
        [OperationContract]
        List<DictCommon> GetAllByCats(string cat);

        /// <summary>
        /// 根据中文名获取其所有数据
        /// </summary>
        /// <param name="szhname">中文名</param>
        /// <returns></returns>
        [OperationContract]
        List<DictCommon> GetAllBySzh(string szhname);

        /// <summary>
        /// 根据英文名获取其所有数据
        /// </summary>
        /// <param name="enname">英文名</param>
        /// <returns></returns>
        [OperationContract]
        List<DictCommon> GetAllByEn(string enname);

        /// <summary>
        /// 获取所有类别集合
        /// </summary>
        [OperationContract]
        List<string> GetAllCats();

        #endregion




    }
}
