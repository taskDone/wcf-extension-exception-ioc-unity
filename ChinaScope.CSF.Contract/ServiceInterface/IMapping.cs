﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    public interface IMapping
    {
        string _id { get; set; }
        string bid { get; set; }
    }

    #region attribute
    public class ModelAlias : Attribute
    {
        public string ModelName { get; set; }
        public Type EnumType { get; set; }
        public string DictTypeCd { get; set; }
        //public ModelAlias(string modelName)
        //{
        //    this.ModelName = modelName;
        //}
    }
    public class ExcelBindingAttribute : Attribute
    {
        public int ColumnNo { get; set; }
        public bool ReadOnly { get; set; }
        public int DisplayUnit { get; set; }
        public string DefaultValue { get; set; }
        public bool Required { get; set; }
        public bool Number { get; set; }
    }
    #endregion

    public class DisplayAttribute : System.Attribute
    {
        public string Display { get; set; }
        public DisplayAttribute()
        {

        }
        public DisplayAttribute(string display)
        {
            Display = display;
        }
    }

    public enum HType : int
    {
        [Display("直接持股")]
        直接持股 = 0,
        [Display("间接持股")]
        间接持股 = 1
    }

    public enum TermType : int
    {
        [Display("否")]
        否 = 0,
        [Display("是")]
        是 = 1
    }

    //public enum ActiveType:Boolean
    //{
    //    [Display("否")]
    //    否 = true,
    //    [Display("是")]
    //    是 = false
    //}

    public enum Sts : int
    {
        [Display("锁定")]
        锁定 = 0,
        [Display("继续")]
        继续 = 1,
        [Display("停止")]
        停止 = 2
    }

    public enum PublishType : int
    {
        [Display("已删除")]
        已删除 = 0,
        [Display("未发布")]
        未发布 = 1,
        [Display("已审核")]
        已审核 = 2,
        [Display("已发布")]
        已发布 = 3
    }
}
