﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.ServiceModel;

using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.PEVC;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using ChinaScope.CSF.Contract.Entity.ShareHolder;
using ChinaScope.CSF.Contract.UIDeriveEntity.CED;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IDictCedIndicatorTypeService
    {
        [OperationContract]
        List<DictCedIndicatorTypePart> GetresultsByConditions(string argTyp, string argName, string argStat,
            string StartDate, string endDate);

        /// <summary>
        /// 获取两张表里的cat类型
        /// </summary>
        [OperationContract]
        List<string> GetCats();

        /// <summary>
        /// 获取两张表里的中文名称
        /// </summary>
        [OperationContract]
        DataTable GetAllSzhs();

        /// <summary>
        /// 添加
        /// </summary>
        [OperationContract]
        void Insert(List<DictCedIndicatorTypePart> argModelParts);

        /// <summary>
        /// 保存
        /// </summary>
        [OperationContract]
        void Save(List<DictCedIndicatorTypePart> argModelParts);


        /// <summary>
        /// 根据中文名称获取条目(完全匹配)
        /// </summary>
        [OperationContract]
        DictCedIndicatorTypePart GetItemBySzh(string argNameSzh, string tablename);

        /// <summary>
        /// 根据英文名称获取条目(完全匹配)
        /// </summary>
        [OperationContract]
        DictCedIndicatorTypePart GetItemByEn(string argNameEn, string tablename);

         /// <summary>
        /// 根据类型中文名称获取条目(完全匹配)
        /// </summary>
        [OperationContract]
        DictCedIndicatorTypePart GetItemByCatszh(string argCatSzh, string tablename);
        
        /// <summary>
        /// 根据类型编码称获取条目(完全匹配)
        /// </summary>
        [OperationContract]
        DictCedIndicatorTypePart GetItemByCatcd(string argCatSzh, string tablename);

        /// <summary>
        /// 根据类型英文名称获取条目(完全匹配)
        /// </summary>
        [OperationContract]
        DictCedIndicatorTypePart GetItemByCaten(string argCatEn, string tablename);

          /// <summary>
        /// 根据编码获取条目(完全匹配)
        /// 主要是判重作用
        /// </summary>
        [OperationContract]
        DictCedIndicatorTypePart GetItemBycd(string argCode, string tablename);

        /// <summary>
        /// 获取两张表dict_ced_indicator_type/dict_ced_source中的cat
        /// </summary>
        [OperationContract]
        List<DiffTableCatsInfo> GetAllCatsInfo();

        /// <summary>
        /// 某一级分类下是否存在指定二级排序
        /// </summary>
        [OperationContract]
        DictCedIndicatorTypePart IsExistSameOrder(string table, string catcd, long order);


    }
}
