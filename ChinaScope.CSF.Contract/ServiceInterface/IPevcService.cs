﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CED;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.PEVC;
using ChinaScope.CSF.Contract.Entity.Sam;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IPevcService
    {
        #region DictCurrencyDao相关

        [OperationContract]
        List<DictCurrency> GetAll();

        [OperationContract]
        DictCurrency SelectDictCurrencyByName(string argName);

        [OperationContract]
        DictCurrency SelectDictCurrencyByCode(string argCode);

        [OperationContract]
        DictCurrency SelectDictCurrencyByEName(string argEName);


        #endregion

        #region DictIndustryDao相关

        [OperationContract]
        DictIndustry SelectIndustryByCn(string cn);

        [OperationContract]
        List<DictIndustry> SelectIndustryByCd(string cd);

        [OperationContract]
        List<DictIndustry> SelectIndustryAll();

        [OperationContract]
        List<DictIndustry> SelectIndustryByPublisher(string argType);

        [OperationContract]
        DictIndustry SelectIndustryByPublisherAndCn(string argType, string argName);

        #endregion

        #region DictIndustryGbDao相关

        [OperationContract]
        DictIndustryGb SelectGBIndustryByCn(string cn);

        [OperationContract]
        DictIndustryGb SelectGBIndustryByCd(string code);

        [OperationContract]
        List<DictIndustryGb> SelectGBIndustryAll();

        #endregion

        #region DictMagnitudeDao相关

        [OperationContract]
        DictMagnitude SelectDictMagnitudeByName(string argName);

        [OperationContract]
        List<DictMagnitude> SelectMagnitudeAll();

        #endregion

        #region DictMarketDao相关

        [OperationContract]
        DictMarket SelectMarketByCn(string cn);

        [OperationContract]
        List<DictMarket> SelectMarketAll();

        #endregion

        #region DictRegionDao相关

        [OperationContract]
        List<DictRegion> GetAallDictRegions();

        [OperationContract]
        DictRegion SelectPevcOrgByCn(string cn);

        [OperationContract]
        DictRegion SelectPevcOrgById(string argID);

        #endregion

        #region PevcFundDao相关

        [OperationContract]
        List<PevcFund> SelectPevcFund(Dictionary<string, string> strSearch);

        [OperationContract]
        bool InsertPevcFunds(List<PevcFund> lstPevcDicts);

        [OperationContract]
        bool SavePevcFunds(List<PevcFund> lstPevcDicts);

        [OperationContract]
        bool DeletePevcFunds(List<string> lstPevcDicts);

        [OperationContract]
        PevcFund SelectByCnName(string cname);

        [OperationContract]
        List<PevcFund> SelectLikeCnName(string cname);

        #endregion

        #region PevcFundLPDao相关

        [OperationContract]
        List<PevcFundLP> SelectPevcFundLPs(Dictionary<string, string> strSearch);

        [OperationContract]
        bool InsertPevcFundLPs(List<PevcFundLP> lstPevcfundlps);

        [OperationContract]
        bool SavePevcFundLPs(List<PevcFundLP> lstPevcfundlps);

        [OperationContract]
        bool DeletePevcFundLPs(List<string> lstPevcfundlps);

        [OperationContract]
        PevcFundLP SelectPevcFundLPByCn(string cn);

        #endregion

        #region InstitutionInfoDao相关

        [OperationContract]
        List<PevcOrg> SelectPevcOrgs(Dictionary<string, string> strSearch);

        [OperationContract]
        bool InsertPevcOrgs(List<PevcOrg> lstPevcorgs);

        [OperationContract]
        bool SavePevcOrgs(List<PevcOrg> lstPevcOrgs);

        [OperationContract]
        bool DeletePevcOrgs(List<string> lstPevcOrgs);

        [OperationContract]
        PevcOrg SelectOrgByCn(string cn);

        #endregion

        #region ManagementDao相关

        [OperationContract]
        List<PevcManagement> SelectManagements(Dictionary<string, string> strSearch);

        [OperationContract]
        bool InsertPevcManagement(List<PevcManagement> lstPevcDicts);

        [OperationContract]
        bool SavePevcManagement(List<PevcManagement> lstPevcDicts);

        [OperationContract]
        bool DeletePevcManagement(List<string> lstPevcDicts);

        [OperationContract]
        List<PevcManagement> SelectPevcMagByCnName(string cname);

        [OperationContract]
        PevcManagement SelectManagementByCnName(string cname);

        #endregion

        #region OrgDao相关

        [OperationContract]
        List<Org> SelectOrg(string strSearchText, string tableTag);

        [OperationContract]
        Org SelectOneOrg(string strSearchText, string tableTag);

        [OperationContract]
        PevcOrg SelectPevcOrgByName(string argName);

        [OperationContract]
        List<PevcOrg> SelectPevcOrgAll();

        #endregion

        #region PevcPeopleDao相关

        [OperationContract]
        List<PevcPeople> SelectPeoples(string name);

        [OperationContract]
        List<PevcPeople> SelectPeople(Dictionary<string, string> strSearch);

        [OperationContract]
        bool InsertPeoples(List<PevcPeople> lstPevcDicts);

        [OperationContract]
        bool SavePeoples(List<PevcPeople> lstPevcDicts);

        [OperationContract]
        bool DeletePeoples(List<string> lstPevcDicts);

        [OperationContract]
        PevcPeople SelectById(string id);

        #endregion

        #region PevcDictDao相关

        [OperationContract]
        List<PevcDict> SelectPDict(Dictionary<string, string> strSearch);

        [OperationContract]
        PevcDict SelectPevcDictByTypeAndValue(string argType, string argValue);

        [OperationContract]
        PevcDict SelectPevcDictByValue(string argValue);

        [OperationContract]
        PevcDict SelectPevcDictByTypAndCode(string argType, string argCode);

        [OperationContract]
        List<PevcDict> SelectPevcDictByType(string argType);

        [OperationContract]
        bool InsertPDicts(List<PevcDict> lstPevcDicts);

        [OperationContract]
        bool SavePDicts(List<PevcDict> lstPevcDicts);

        [OperationContract]
        bool DeletePDicts(List<string> lstPevcDicts);

        #endregion

        #region PevcFinDao相关

        [OperationContract]
        List<PevcFin> SelectPevcFin(Dictionary<string, string> strSearch);

        [OperationContract]
        bool InsertPevcFins(List<PevcFin> lstPevcDicts);

        [OperationContract]
        bool SavePevcFins(List<PevcFin> lstPevcDicts);

        [OperationContract]
        bool DeletePevcFins(List<string> lstPevcDicts);

        [OperationContract]
        PevcFin SelectFinByCnName(string cname);

        #endregion

        #region PevcProjectShareholderDao相关

         [OperationContract]
        List<PevcProjectShareholder> SelectPevcProj(Dictionary<string, string> strSearch);

         [OperationContract]
        bool InsertPevcProjs(List<PevcProjectShareholder> lstPevcDicts);

         [OperationContract]
        bool SavePevcProjs(List<PevcProjectShareholder> lstPevcDicts);

         [OperationContract]
        bool DeletePevcProjs(List<string> lstPevcDicts);

         [OperationContract]
        PevcProjectShareholder SelectProjByCnName(string cname);

        #endregion

         #region ProjectDao相关

         [OperationContract]
         List<PevcProject> SelectPevcP(Dictionary<string, string> strSearch);

        [OperationContract]
        bool InsertPevcPs(List<PevcProject> lstPevcDicts);

        [OperationContract]
        bool SavePevcPs(List<PevcProject> lstPevcDicts);

        [OperationContract]
        bool DeletePevcPs(List<string> lstPevcDicts);

        [OperationContract]
        PevcProject SelectPByCnName(string cname);

        [OperationContract]
        List<PevcProject> SelectPLikeCnName(string cname);

         #endregion

        #region ProjectDetailDao相关

        [OperationContract]
        List<PevcProjectDetail> SelectPevcPD(Dictionary<string, string> strSearch);

        [OperationContract]
        bool InsertPevcPDs(List<PevcProjectDetail> lstPevcDicts);

        [OperationContract]
        bool SavePevcPDs(List<PevcProjectDetail> lstPevcDicts);

        [OperationContract]
        bool DeletePevcPDs(List<string> lstPevcDicts);

        [OperationContract]
        PevcProjectDetail SelectPDByCnName(string cname);

        #endregion

        #region DictProductCedDao相关

        [OperationContract]
        List<DictProductCed> GetCedByName(string name);

        [OperationContract]
        List<DictProductCed> GetAllCedProducts();

        [OperationContract]
        List<DictSamProduct> GetAllSamProduct();

        #endregion
    }
}
