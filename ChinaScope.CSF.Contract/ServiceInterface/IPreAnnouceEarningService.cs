﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.UIDeriveEntity.PreAnnounceEarning;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IPreAnnouceEarningService
    {
        [OperationContract]
        List<DerivePreAnnEarning> GetByCondition(string secu, string rpt, string y, string startlmt, string endlmt,
            string stat,string typ);

        [OperationContract]
        bool SaveItems(List<DerivePreAnnEarning> saveList);

        /// <summary>
        /// 获取所有未发布数据的信息
        /// 默认最多取5000条
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DerivePreAnnEarning> GetAllUnPublishItems();

        /// <summary>
        /// 获取stock信息
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        BaseStock GetStockInfoByCode(string code);

        /// <summary>
        /// 根据ticker获取stock信息
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        Dictionary<string, string> GetStockLstByTicker(string ticker);

        /// <summary>
        /// 获取所有业绩预告类型
        /// 去重的
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<string> GetAllTyplst();
    }
}
