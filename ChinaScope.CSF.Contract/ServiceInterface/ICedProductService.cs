﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.UIDeriveEntity.CED;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface ICedProductService
    {
        [OperationContract]
        List<DictCedProductPart> GetProductByConditions(string argName, string argSequence, string argType, DateTime argUps, DateTime argUpe, string argPubStat);

        [OperationContract]
        void Save(List<DictCedProductPart> argList);

        [OperationContract]
        void Insert(List<DictCedProductPart> lstData);
    }
}
