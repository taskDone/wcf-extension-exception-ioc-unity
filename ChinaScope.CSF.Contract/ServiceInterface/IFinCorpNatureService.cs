﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CorpNature;
using ChinaScope.CSF.Contract.UIDeriveEntity.CorpNature;
using ChinaScope.CSF.Contract.Entity.CommonEntity;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IFinCorpNatureService
    {
        [OperationContract]
        List<DeriveFinCorpNature> GetAllcorps();

        [OperationContract]
        List<DeriveFinCorpNature> GetCorpsByConditions(string secu, string exchage, string catlog, string subcatlog,
            string stat, string uptStart, string uptEnd, string mkt);

        [OperationContract]
        bool SaveItems(List<DeriveFinCorpNature> saveList);

        [OperationContract]
        DeriveFinCorpNature GetCorpBySecu(string secu);

         [OperationContract]
        IDCNEN GetOrgByID(string id);

      
        /// <summary>
        /// 获取所有未发布数据的信息
        /// 默认最多取5000条
        /// </summary>
        /// <returns></returns>
         [OperationContract]
         List<DeriveFinCorpNature> GetAllUnPublishItems();


         #region 其他表相关数据服务
         
         /// <summary>
         /// 根据orgid和ticker有且仅确定一条stock
         /// 相同orgid有多个ticker，不同市场
         /// </summary>
         /// <param name="orgid">机构id</param>
         /// <param name="ticker">ticker</param>
         /// <returns></returns>
         [OperationContract]
         string GetStockByOrgidAndTick(string orgid, string ticker);

        /// <summary>
        /// 根据orgid获取所有ticker集合
        /// </summary>
        /// <param name="argOrgId">orgid</param>
        /// <returns></returns>
        [OperationContract]
        List<BaseStock> GetTickersByOrgId(string argOrgId);

        #endregion

    }
}
