﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CED;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.Sam;
using ChinaScope.CSF.Contract.UIDeriveEntity.SupplyChain;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface ISupplyChain
    {
        [OperationContract]
        List<SupplyChainRelation> GetAllRealtions();

        #region SupplyChainRelation

        [OperationContract]
        List<DeriveSupplyChain> GetSupplyChainRelationByCondition(string name);

        [OperationContract]
        List<DictSamProduct> GetAllSamlst();

        /// <summary>
        /// 对指定产品保存起上下游
        /// 如果没有产品name的指定,则在构造上下游关系时无法指定
        /// </summary>
        /// <param name="saveitems">保存的列表</param>
        /// <param name="name">原始产品</param>
        /// <returns>是否保存成功</returns>
        [OperationContract]
        bool SaveChainItems(List<DeriveSupplyChain> saveitems, CDCNEN name);

        /// <summary>
        /// 刷新产业链表中sam不存在与sam表的数据,没有就删除并发布
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        void RefleshRelationSam();

        #endregion

        #region SupplyChainDriver

        [OperationContract]
        List<DeriveSupplyDriver> GetSupplyChainDriverByCondition(string name, string cedname);

        [OperationContract]
        List<CedIndicator> GetCedByFuzzyName(string name);

        [OperationContract]
        bool SaveDriverItems(List<DeriveSupplyDriver> saveitems);

        [OperationContract]
        List<CedIndicator> GetCedCodeByName(string name);

        /// <summary>
        /// 刷新产业驱动表中sam不存在与sam表的数据,没有就删除并发布
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        void RefleshDriverSam();

        #endregion
    }
}
