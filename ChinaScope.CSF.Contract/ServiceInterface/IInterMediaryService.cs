﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.UIDeriveEntity.InterMediary;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IInterMediaryService
    {
        [OperationContract]
        List<DictIntermediaryType> GetAllTypes();

        [OperationContract]
        List<DeriveInterMediary> GetInterMediarysByConditions(string medinaryname, string orgname, string orgticker,
            string typ, string stat, string uptStart, string uptEnd, string mkt);

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <param name="saveList">保存的数据列表</param>
        /// <returns></returns>
        [OperationContract]
        bool SaveItems(List<DeriveInterMediary> saveList);

        /// <summary>
        /// 根据用户录入的中文名或者英文名获取中英文集合
        /// </summary>
        /// <param name="orgszh">机构名（包括中/英文）</param>
        /// <returns>中英文集合</returns>
        [OperationContract]
        IDCNEN GetOrgNameByQuery(string orgszh);

        /// <summary>
        /// 根据用户录入的中文名或者英文名获取中介表内已填写的相同的
        /// </summary>
        /// <param name="orgszh">机构名（包括中/英文）</param>
        /// <returns>中英文集合</returns>
        [OperationContract]
        IDCNEN GetInterMedNameByQuery(string intermname);

        /// <summary>
        /// 生成的随机interorgid是否存在
        /// </summary>
        /// <param name="interorgid">中介机构id</param>
        /// <returns>是否存在</returns>
        [OperationContract]
        bool IsInterOrgidExist(string interorgid);
 
    }
}
