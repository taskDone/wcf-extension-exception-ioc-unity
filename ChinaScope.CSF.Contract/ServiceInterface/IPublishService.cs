﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.Publish;
using MongoDB.Bson;
using PublishModel = ChinaScope.CSF.Contract.Entity.Publish.PublishModel;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    [ServiceKnownType(typeof (ObjectId))]
    public interface IPublishService
    {
        [OperationContract]
        bool Publish(string argTable, Dictionary<string, object> argDic, PublishOption argType,
            List<OplogIndexer> argIndex = null, List<OplogCache> argCaches = null);

        [OperationContract]
        bool Delete(string argTable, Dictionary<string, object> argDic, PublishOption argOption,
            List<OplogIndexer> argIndex = null, List<OplogCache> argCaches = null);
    }
}
