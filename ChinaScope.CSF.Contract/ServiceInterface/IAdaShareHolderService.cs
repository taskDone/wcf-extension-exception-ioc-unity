﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IAdaShareHolderService
    {
        #region DictShareholderType

        [OperationContract]
        List<DictShareholderType> GetDictShareVaryAll();

        [OperationContract]
        DictShareholderType GetDictVaryByName(string typename);

        #endregion

        #region BaseFund

        [OperationContract]
        string GetFundNameByID(string id);

        [OperationContract]
        Dictionary<string, string> GetFundIDByName(string name, string orgid);

        [OperationContract]
        List<string> GetIDSByFuzzyName(string fuzzystr);

        #endregion

        #region BaseOrg

        [OperationContract]
        List<string> orgGetIDSByFuzzyName(string fuzzystr);

        [OperationContract]
        string GetOrgNameByID(string id);

        //名称相等查询
        [OperationContract]
        Dictionary<string, string> GetOrgIDByName(string name);

        //名称模糊匹配查询
        [OperationContract]
        Dictionary<string, string> GetOrgIDsByfuzzyname(string name);

        [OperationContract]
        BaseOrg GetOrgByName(string id);


        #endregion

        #region BasePeopl

        [OperationContract]
        BasePeople GetPeopleByID(string id);

        [OperationContract]
        string GetPeopleNameByID(string id);

        [OperationContract]
        Dictionary<string, string> GetIDByPeopleNameAndorgid(string name, string orgid);

        [OperationContract]
        List<string> peopGetIDSByFuzzyName(string fuzzystr);

        [OperationContract]
        string GetPeopleEnOrChNameByID(string id, int flag);

        #endregion

        #region BasePortfolio

        [OperationContract]
        string GetPortfolioNameByID(string id);

        [OperationContract]
        Dictionary<string, string> GetIDByPortfolioName(string name, string orgid);

        [OperationContract]
        List<string> portfGetIDSByFuzzyName(string fuzzystr);

        #endregion

        #region BaseStock

        [OperationContract]
        string GetOrgIDByTicker(string ticker);

        [OperationContract]
        string GetOrgIDBySecu(string secucode);

        [OperationContract]
        List<string> GetTickerByOrgID(string orgid);

        [OperationContract]
        IDCNEN GetOrgInfoByOrgID(string orgid);

        [OperationContract]
        Dictionary<string, List<string>> GetSeculstByOrgidlst(List<string> orgidlst);

        [OperationContract]
        List<BaseStock> GetStockorgIDByName(string name);

        /// <summary>
        /// 获取所有上市公司的orgid列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<string> GetAllOrgids();

        #endregion

        #region BaseShareVary

        [OperationContract]
        ShareVaryNameAndTotal GetHolderNameByorgid(string orgid, string orgdate);

        [OperationContract]
        List<BaseShareVary> GetHolderlstNameByorgid(string orgid, string crtstart,
            string crtend);

        [OperationContract]
        bool SaveAddVarys(List<BaseShareVary> savelist);

        /// <summary>
        /// 根据单个orgid获取股本信息
        /// </summary>
        /// <param name="orgid">单个orgid</param>
        [OperationContract]
        BaseShareVary GetLastedHolderVary(string orgid);

        /// <summary>
        /// 根据多个orgid获取所有股本信息
        /// </summary>
        /// <param name="orgidlst">多个orgid</param>
        [OperationContract]
        List<BaseShareVary> GetVarysByOrgidlst(List<string> orgidlst);

        [OperationContract]
        BaseShareVary GetVaryByID(string id);

        [OperationContract]
        string GetNextChangeDateByID(string orgid, string UptChangeDate);

        /// <summary>
        /// 获取所有股本表中未发布的集合
        /// 超过配置文件设定值时取配置文件中的条数
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [OperationContract]
        List<BaseShareVary> GetAllUnPublishVaryItems();

        /// <summary>
        /// 去重股本表base_share_vary全表所有机构重复的股本
        /// 按照orgid + share + total共同决定是否为同一条
        /// </summary>
        [OperationContract]
        void RefleshSameVary(List<string> codelst);


        #endregion

        #region BaseShareholderDef

        [OperationContract]
        List<BaseShareholderDef> QueryShareholderDef(List<string> hids, List<string> types,
            string fuzzyquery, string uptstart, string uptend, bool checken, bool checkbaseid, bool checkticker,
            string crtstarttime, string crtendtime, bool checktype);

        [OperationContract]
        bool SaveAddDefs(List<BaseShareholderDef> savelist);

        [OperationContract]
        BaseShareholderDef GetDefByCName(string szhname);

        [OperationContract]
        List<BaseShareholderDef> GetDefBy3FuzzyName(string name, bool isenglish, bool ispeople);

        [OperationContract]
        BaseShareholderDef GetDefByEName(string enname);

        [OperationContract]
        BaseShareholderDef GetDefByHid(string hid);

        [OperationContract]
        List<BaseShareholderDef> GetDefsByHid(List<string> hids);

        [OperationContract]
        bool GetIDByPeopleNameAndid(string name, string baseid);

        [OperationContract]
        List<BaseShareholderDef> GetDefByFuzzyName(string name);

        /// <summary>
        /// 获取所有股东标准表中未发布的集合
        /// 超过配置文件设定值时取配置文件中的条数
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [OperationContract]
        List<BaseShareholderDef> GetAllUnPublishDefItems();

        /// <summary>
        /// 查询并删除所有未被股东披露表引用的半标准列表
        /// 删除会删除内部及外部的数据
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        void GetAndDelAllUnRefDef();

        #endregion

        #region  BaseShareholder

        [OperationContract]
        List<BaseShareholder> GetShareIDByOrgid(string orgid);

        [OperationContract]
        BaseShareholder GetHolderByID(string id);

        [OperationContract]
        List<string> IsContainhid(List<string> hidlst);

        [OperationContract]
        BaseShareholder GetHolderByName(string name);

        [OperationContract]
        List<BaseShareholder> GetHolderlstByName(string name);

        [OperationContract]
        List<BaseShareholder> GetShareholderByOrgAndDate(string orgid, string date);

        [OperationContract]
        List<BaseShareholder> GetUnPublishShareholderByOrgAndDate(string orgid, string date, bool IsUnpublishStat);

        [OperationContract]
        List<BaseShareholder> GetShareholderByOrgAndChangeDate(string orgid, string Startdate, string Endate);

        [OperationContract]
        bool SaveAddHolders(List<BaseShareholder> savelist);

        [OperationContract]
        void SaveHolders(List<BaseShareholder> savelist);

        [OperationContract]
        BaseShareholder GetShareholderByOrgAndName(string orgid, string name);

        /// <summary>
        /// 获取所有引用过该hid的股东
        /// </summary>
        [OperationContract]
        List<BaseShareholder> GetHoldersByHid(string hid);

        [OperationContract]
        Dictionary<string, string> GetIDNameByTableName(string tablename, string querystr, string orgid);

        [OperationContract]
        string GetDescripByTableID(string tablename, string id);

        [OperationContract]
        string GetNameByTableID(string tablename, string id);

        [OperationContract]
        List<HidCnnOrgid> GetHoldersContainhid(List<BaseShareholderDef> defhids);

        [OperationContract]
        List<string> GetAllEdtByOrgid(string orgid);

        /// <summary>
        /// 获取所有股东披露表中未发布的集合
        /// 超过配置文件设定值时取配置文件中的条数
        /// </summary>
        [OperationContract]
        List<BaseShareholder> GetAllUnPublishHolderItems();

        /// <summary>
        /// 根据披露名称获取其所有对应关系（3中）【如：披露名称为工行】
        /// 半标准-披露名称1【如：工商银行-工行】
        /// 半标准（名称=披露名称1）-披露名称2【如：工行-工商银行】
        /// 包含披露名称的半标准-null关系【如：中国工行-null】
        /// 由于排除不可重复关系，故此处用Dictionary
        /// </summary>
        [OperationContract]
        List<HidAndHolder> GetRelationByName(string holdername);


        #region 披露管理相关
        
        [OperationContract]
        List<BaseShareholder> GetManageShareholderByCondition(string orgid, string plsname, string gdsname, string startvdtdate, string endvdtdate, string startcrtdate, string endcrtdate);

        #endregion


        #endregion

        #region Common

        /// <summary>
        /// 通过ticker获取多个secu及orgid的值
        /// </summary>
        /// <param name="ticker">ticker</param>
        /// <returns>列表</returns>
        [OperationContract]
        Dictionary<string,string> GetDifferSecuAndOrgidByTicker(string ticker);

        #endregion
    }
}
