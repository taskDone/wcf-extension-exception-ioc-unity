﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CED;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.UIDeriveEntity.CED;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface ICedIndicatorService
    {
        [OperationContract]
        List<CedIndicatorPart> GetOrgsByConditions(string argName, string argFreq, string argIndStat,
            string argPubStat, string argSrc, string argType, string argIndustry, string argProduct, string argRegion,
            string argLstPerStart, string argLstPerEnd, DateTime argUps, DateTime argUpe, string argProperty);

        [OperationContract]
        List<DictCedSource> GetAllDictCedSources();

        [OperationContract]
        List<DictCedIndicatorType> GetAllDictCedType();

        [OperationContract]
        List<DictFreq> GetAllDictFreq();

        [OperationContract]
        List<DictIndustryGb> GetAllIndustryGb();

        [OperationContract]
        List<DictIndustry> GetAllCSFIndustry();

        [OperationContract]
        List<DictRegion> GetAllDictRegion();

        [OperationContract]
        List<DictUnits> GetAllDictUnits();

        [OperationContract]
        void Insert(List<CedIndicatorPart> argList);

        [OperationContract]
        void Save(List<CedIndicatorPart> argList);

        [OperationContract]
        List<DictProductCed> GetAllProductCeds();

        [OperationContract]
        List<CedIndicatorPart> GetCedIndicatorPartBysid(IEnumerable<string> argSid);

        [OperationContract]
        List<DictMagnitude> GetAllMagnitudes();

        [OperationContract]
        List<DictCurrency> GetAllCurrencies();

        [OperationContract]
        List<CedIndicatorDataPart> GetDataPartsByCodes(List<long> sidlst);

        //根据当前时间刷新指标状态
        [OperationContract]
        int UpdateStsByDt(string filename);

        //创建一个文件来标识服务器是否异步刷新完成
        //有文件则说明改次调用已完成
        [OperationContract]
        bool IsUpdateStsCompleted(string filename);

        //创建一个文件来标识服务器是否异步刷新完成
        //有文件则说明改次调用已完成
        [OperationContract]
        void DeleteFileByName(string filename);


        #region 指标Data相关接口

        /// <summary>
        /// 获取指标数据
        /// </summary>
        [OperationContract]
        List<CedIndicatorDataPart> DataGetOrgsByConditions(string argName, string argSrc, string argFreq,
            string argStatus, string argStat, string argPStart, string argPEnd, string argUStart, string argUEnd, string argProperty);

        /// <summary>
        /// 通过sid找到对应的指标集合
        /// </summary>
        [OperationContract]
        List<Contract.UIDeriveEntity.CED.CedIndicatorDataPart> DataGetIndsBySids(List<string> sidlst);

        /// <summary>
        /// 根据code获取所有报告期
        /// </summary>
        [OperationContract]
        List<string> GetyBycds(List<long> codes);

        /// <summary>
        /// 根据code获取所有data集合
        /// </summary>
        [OperationContract]
        List<CedIndicatorData> GetdataBycds(List<long> codes);

        /// <summary>
        /// 根据code获取单条data集合
        /// </summary>
        [OperationContract]
        CedIndicatorData GetSingleDataBycd(long code);

        /// <summary>
        /// 保存Data对象集合
        /// IndexUrl由于数据表要刷指标里的最新值属性，为前台能即使刷新每次保存要刷索引
        /// operation仅当添加数据操作时，要对指标表的rdt字段进行刷新（此字段表示最新添加时间）
        /// </summary>
        [OperationContract]
        string DataSave(List<CedIndicatorData> savelst, string IndexUrl);

        /// <summary>
        /// 仅保存Data对象不涉及指标相关
        /// 如果有索引会去刷索引
        /// 发布专用
        /// </summary>
        [OperationContract]
        void SaveAndRefIndex(List<CedIndicatorData> savelst, string IndexUrl, string guidkey);

        /// <summary>
        /// 根据code集合获取指标对象
        /// </summary>
        [OperationContract]
        List<CedIndicatorPart> GetIndsByCodes(List<long> codeList);

        /// <summary>
        /// 按名称获取单条用户设置的文件路径与配置
        /// </summary>
        [OperationContract]
        CedIDataFileSetting GetCedataFileSetting(string filename);

        /// <summary>
        /// 保存用户设置的文件路径与配置
        /// </summary>
        [OperationContract]
        void SaveCedataFileSetting(CedIDataFileSetting setting);


        /// <summary>
        /// 获取所有未发布的指标数据
        /// </summary>
        [OperationContract]
        List<CedIndicatorData> GetAllUnPublst();

        #endregion
    }
}
