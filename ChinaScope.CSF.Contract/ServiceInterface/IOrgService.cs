﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using ChinaScope.CSF.Contract.Entity.ShareHolder;
using ChinaScope.CSF.Contract.UIDeriveEntity.Org;
using ChinaScope.CSF.Contract.UIDeriveEntity.Stock;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IOrgService
    {
        [OperationContract]
        List<DeriveOrg> GetOrgByConditions(string argCn, string argEn, string argSCn, string argSEn,string argMarket, string argStat,string uptDateStart,string uptDateEnd);

        [OperationContract]
        bool Save(List<DeriveOrg> argList);

        [OperationContract]
        string GetOrgIdByName(string argName);

        [OperationContract]
        string GetOrgIdByEnName(string argName);

        [OperationContract]
        List<DeriveOrg> GetAllOrg();



        [OperationContract]
        List<DictIndustry> GetAllDictIndustry();

        [OperationContract]
        List<DictRegion> GetAllDictRegions();

        /// <summary>
        /// 获取所有未发布数据的信息
        /// 默认最多取5000条
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveOrg> GetAllUnPublishItems();

        /// <summary>
        /// 获取指定orgid列表下所有的stock
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveStock> GetStocksByOrgid(List<string> orgid);

    }
}
