﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.Cornerstone;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.UIDeriveEntity.Conerstone;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.DataContractNamespace)]
    public interface IConerStoneService
    {
        /// <summary>
        /// 获取所有基石信息
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveBaseCsInvestment> GetAll();

        /// <summary>
        /// 根据类别cat获取其类别下的所有分类
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DictCommon> GetAllByCats(string cat);

        /// <summary>
        /// 根据基石名称获取数据结果集列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveBaseCsInvestment> GetConerStoneByName(string csname);

        /// <summary>
        /// 根据基石名称/证券代码/发布状态获取数据结果集列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveBaseCsInvestment> GetConerStoneByQuery(string csname, string secu, string stat, string invtyp, string paymethod,
            string datestart, string dateend, string flrstart, string flrend, string uptstart, string uptend);

        /// <summary>
        /// 按照名称/关联类型/更新日期/发布状态到基石表中查找信息
        /// 支持中英文的模糊查询
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveBaseCsInvestment> GetPartialConerStoneByCondition(string csname, string cat, string uptstart,
            string uptend, string stat, string relatedtyp);

        /// <summary>
        /// 根据中文名称获取对应的条目信息
        /// 主要用于检验重名
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        DeriveBaseCsInvestment GetInvBySzhName(string szhname);

        /// <summary>
        /// 根据英文名称获取对应的条目信息
        /// 主要用于检验重名
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        DeriveBaseCsInvestment GetInvByEnName(string enname);

        /// <summary>
        /// 保存新添加的数据
        /// crt/upt需要在此时写入
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        bool SaveAddItems(List<DeriveBaseCsInvestment> addList);

        #region 中间类（基石投资）相关接口
        
        /// <summary>
        /// 根据基石名称/证券代码/发布状态获取中间转换类BaseCsSplitInvs的数据结果集列表
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveBaseCsSplitInvs> GetCSPlitInvByQuery(string csname, string secu, string stat, string invtyp, string paymethod,
            string datestart, string dateend, string flrstart, string flrend, string uptstart, string uptend);

        /// <summary>
        /// 保存中间转换类
        /// 在入库前程序会先尽心一次转换再保存到数据库中
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        bool SaveSPlitInvAddItems(List<DeriveBaseCsSplitInvs> addList);

        /// <summary>
        /// 获取所有无效代码(base_stock表中去检查)
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveBaseCsSplitInvs> GetInvalidSecu();

        #endregion

        #region 访问基础表（org/fund/portfolio/people）的接口

        /// <summary>
        /// 根据参数字符串ID到指定的基础表中查询是否存在
        /// 基础表（org:"1", fund:"2", portfolio:"3", people:"4"）
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        bool IsExistInBaseTable(string typ, string idstr);

        /// <summary>
        /// 通过表名和关键字获取与之匹配的ID和名称
        /// </summary>
        /// <param name="tablename">基础表表名</param>
        /// <param name="querystr">查询关键字</param>
        /// <returns>所有符合条件的id与名称</returns>
        [OperationContract]
        Dictionary<string, string> GetIDNameByDiffTableName(string tablename, string querystr);

        /// <summary>
        /// 根据code到指定表中查询改code是否存在
        /// 本处主要去base_stock表查询secucode是否存在
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        bool IsCodeExistInDiffTable(string tablename, string code);


        #endregion

    }
}
