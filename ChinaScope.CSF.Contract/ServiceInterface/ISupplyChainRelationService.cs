﻿using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;

using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.Sam;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface ISupplyChainRelationService
    {
        [OperationContract]
        IEnumerable<SupplyChainRelation> FindAll();

        [OperationContract]
        bool CheckUpdate(DateTime leastUpdateTime);

        [OperationContract]
        IEnumerable<FinSamAnalysis> FindYearMonthMkt(int year,int month,string mkt);

        [OperationContract]
        IEnumerable<FinSamAnalysis> FindUptMkt(DateTime leastUpdateTime, string mkt);

        [OperationContract]
        IEnumerable<FinSamAnalysis> FindDateNkt(string date, string mkt);

        [OperationContract]
        DataTable FindEquityPriceBySecu(string secuCode, string date);

        [OperationContract]
        DataTable FindEquitySplit(string table, string date, string exhgCdFiled);
    }
}
