﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.PEVC;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using ChinaScope.CSF.Contract.Entity.ShareHolder;
using ChinaScope.CSF.Contract.UIDeriveEntity.Stock;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IStockService
    {
        [OperationContract]
        Stock GetStockByTicker(string ticker);

        [OperationContract]
        List<DeriveStock> GetStocksByConditions(string ticker, string secu, string cov, string market,
            string stockstatus, string adr,
            string stat,string uptStart,string uptEnd);

        [OperationContract]
        DeriveStock GetItemByCode(string code);

        [OperationContract]
        bool SaveItems(List<DeriveStock> saveList);

        [OperationContract]
        List<IDCNEN> GetOrglstByName(string name);

        [OperationContract]
        IDCNEN GetOrgByName(string name);

        [OperationContract]
        List<DictMarket> GetAllDictMarket();

        [OperationContract]
        List<DictListingStatus> GetAllDictMarketStatus();

        [OperationContract]
        List<DeriveStock> GetAllStocks();

        [OperationContract]
        List<OrgTick> GetOrgidAndTickByName(string name);

        /// <summary>
        /// 获取所有未发布数据的信息
        /// 默认最多取5000条
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveStock> GetAllUnPublishItems();
        
        /// <summary>
        /// 根据bid查询
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveStock> GetItemsBybid(string bid);
    }
}
