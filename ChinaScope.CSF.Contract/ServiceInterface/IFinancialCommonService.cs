﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.CorpNature;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.FinCommon;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    [ServiceKnownType(typeof (MonFinRptAsrepExt))]
    [ServiceKnownType(typeof(MonFinRptTpl))]
    [ServiceKnownType(typeof(MonFinRptBreakdown))]
    public interface IFinancialCommonService
    {
        #region DictDao相关服务

        [OperationContract]
        List<DictModel> GetDicts(string tableName);

        [OperationContract]
        List<DictModel> GetCurrencyDicts(string tableName);

        [OperationContract]
        List<DictModelType> GetTypeDicts(string tableName);

        [OperationContract]
        List<DictModel> GetSysDicts(string tableName);

        #endregion

        #region DictIndustryDao相关服务

        [OperationContract]
        string GetNameByCode(string argCode);

        #endregion

        #region FinCorpNatureDao相关服务

        [OperationContract]
        FinCorpNature GetCorpNatureByTicker(string ticker);

        #endregion

        #region FinFpTempDao相关服务

        [OperationContract]
        long GetFpNum(string fy, string y);

        #endregion

        #region FinLockDao相关服务

        [OperationContract]
        VstoDsLock IsLocked(string rpt, string ticker, int consolidation, int type, string eqenvironment);

        [OperationContract]
        void RemoveById(string argId);

        //[OperationContract]
        //bool IsUnLocked(string rpt, string ticker, int consolidation, int type, string eqenvironment,
        //    out string argUserName);

        [OperationContract]
        List<VstoDsLock> GetAllLockItems(string rpt, string ticker, int consolidation, int type);

        [OperationContract]
        string InsertLockedRecords(VstoDsLock insertitem);

        [OperationContract]
        string ReleaseLock(string rpt, string ticker, int consolidation, int type, string environment);

        #endregion

        #region FinLogDao相关服务

        [OperationContract]
        //SafeModeResult WirteToLog(FinLog log);
        bool WirteToLog(FinLog log);

        [OperationContract]
        FinLog FindLastPeriod(string rpt, string tik, int reportType, int type);

        #endregion

        #region NormRptTplDao相关服务

        [OperationContract]
        List<MonFinRptTpl> GetRptTpl(SelectPeriodDataArgs args, string qt, int limit);

        [OperationContract]
        List<MonFinRptTpl> GetRptTplByMongo(SelectPeriodDataArgs args);

        [OperationContract]
        MonFinRptTpl GetRptTplByMongoID(long id, string table);

        [OperationContract]
        MonFinRptTpl GetRptTplByMongoFpAndQ(MonFinRptTpl selectedtpl, string table);

        [OperationContract(Name = "RecoveryDataTpl")]
        bool RecoveryData(List<MonFinRptTpl> savedata, string Collection);

        #endregion

        #region NormFinRptStyleDao相关服务

        [OperationContract(Name = "SaveRptStyle")]
        bool Save(NormFinRptStyle entity);

        [OperationContract]
        [FaultContract(typeof(Exception))]
        NormFinRptStyle GetSytleByVsto(SelectSytleArgs args);

        [OperationContract]
        long GetSytleIDByVsto(SelectSytleArgs args);

        #endregion

        #region NormFinItemDefDao相关服务

        [OperationContract]
        List<NormFinItemDef> GetItemDefByName(string name, string rpt);

        [OperationContract]
        bool InsertItemDef(NormFinItemDef entity);

        [OperationContract]
        bool DelItemDef(NormFinItemDef entity);

        [OperationContract]
        List<NormFinItemDef> GetItemDefByRpt(string argRpt);

        #endregion

        #region MonVstoFilterItemDao相关服务

        [OperationContract(Name = "InsertFilterItem")]
        bool Insert(MonVstoFilterItem monFilter);

        [OperationContract(Name = "GetMonFinItems4param")]
        List<MonVstoFilterItem> GetMonFinItems(string rpt, string nat, string tik, int classify);

        [OperationContract(Name = "GetMonFinItems3param")]
        List<MonVstoFilterItem> GetMonFinItems(string rpt, string nat, int classify);

        [OperationContract(Name = "DeleteFilterItem")]
        bool Delete(MonVstoFilterItem monFilter);

        #endregion

        #region MongoFinItemDao相关服务

        [OperationContract]
        bool InsertToItem(MonFinItem monFinitem);

        [OperationContract]
        MonFinItem GetMonFinItemsByCodeAndRpt(string code, string argRpt);

        [OperationContract]
        MonFinItem IsExsit(string chiname, string ticker, string rpt, string nature);

        #endregion

        #region MonVstoDsFileInfoDao相关服务

        [OperationContract(Name = "InsertDsFile")]
        bool Insert(MonVstoDsFileInfo monFileInfo);

        [OperationContract]
        List<MonVstoDsFileInfo> GetFileInfo(string ticker, string rpt, int consolidation, int typ, string envir);

        [OperationContract]
        bool DeleteFileInfo(MonVstoDsFileInfo argFile);

        #endregion

        #region MonDictRptNatureDao相关服务

        [OperationContract]
        List<MonDictRptNature> GetAll();

        #endregion

        #region NormFinItemDao相关服务

        [OperationContract(Name = "GetFinItemByCode2param")]
        NormFinItem GetFinItemByCode(string code, string rpt);

        [OperationContract(Name = "GetFinItemByCode1param")]
        NormFinItem GetFinItemByCode(string code);

        [OperationContract]
        NormFinItem DeleteCompanyRsItem(string szh, string rpt, string ticker, string nature, string defCode);

        [OperationContract]
        NormFinItem GetFinItemByRs(string abbr, string rpt, string ticker, string nature);

        [OperationContract]
        List<NormFinItem> GetFinItemsByRpt(string rpt);

        [OperationContract]
        List<MonFinItem> GetMonFinItemsByRpt(string rpt);

        [OperationContract]
        List<MonFinItem> GetMonFinItemsByRptAndTiker(string rpt, string tik);

        [OperationContract]
        MonFinItem GetMonFinItemsByNameAndRpt(string argName, string argRpt);

        [OperationContract]
        List<MonFinItem> GetMonFinItemsTitlebyRpt();

        [OperationContract]
        void SaveMongo(MonFinItem entity);

        #endregion

        #region NormFinAsRptDao相关服务

        [OperationContract(Name = "GetFinAsReps1param")]
        List<MonFinRptAsrepExt> GetFinAsReps(SelectPeriodDataArgs args);

        [OperationContract(Name = "GetFinAsReps3count")]
        IEnumerable<MonFinRptAsrepExt> GetFinAsReps(SelectPeriodDataArgs args, string q, int count);

        [OperationContract(Name = "GetFinAsReps3regex")]
        IEnumerable<MonFinRptAsrepExt> GetFinAsReps(SelectPeriodDataArgs args, string argRegex, string q);

        [OperationContract(Name = "RecoveryDataExt")]
        bool RecoveryData(List<MonFinRptAsrepExt> savedata, string Collection);

        [OperationContract(Name = "DeleteIDByTable")]
        bool Delete(string argID, string Collection);

        #endregion

        #region MonFinRptBreakDownDao相关服务

        [OperationContract(Name = "GetFinAsRepsBreakDown1")]
        List<MonFinRptBreakdown> GetFinAsRepsBreakDown(SelectPeriodDataArgs args);

        [OperationContract(Name = "GetFinAsRepsBreakDown2")]
        IEnumerable<MonFinRptBreakdown> GetFinAsRepsBreakDown(SelectPeriodDataArgs args, string argRegex, string q);

        [OperationContract(Name = "GetFinAsRepsBreakDown3")]
        IEnumerable<MonFinRptBreakdown> GetFinAsRepsBreakDown(SelectPeriodDataArgs args, string q, int count);

        [OperationContract]
        MonFinRptBreakdown GetRepFromAsExtRep(string table, string rpt, string tike, string y, int p, string q, int ctyp,
            string fy, int stdtyp, string accp);

        [OperationContract(Name = "SaveByTable")]
        bool Save(List<dynamic> entity, string collection);

        [OperationContract(Name = "RecoveryDataBreakdown")]
        bool RecoveryData(List<MonFinRptBreakdown> savedata, string Collection);

        #endregion

        #region FinRptTplSaveDao相关服务

        [OperationContract]
        bool SaveTpl(List<dynamic> entity, string collection);

        [OperationContract]
        bool SaveTplByYtd(dynamic monFinRptAsrepExt, string collection, string argMappingRptCode);

        [OperationContract]
        List<MonFinRptTpl> FindTplById(List<long> argIds, string collection);

        [OperationContract]
        bool DeleteTpl(string argID, string Collection);

        [OperationContract]
        MonFinRptTpl GetRepFromTpl(string table, string rpt, string tike, string y, int p, string q, int ctyp, string fy,
            int stdtyp, string accp);

        [OperationContract]
        bool MongoSaveEntityItemsById(MonFinRptTpl entity, string Collection);

        [OperationContract]
        IEnumerable<MonFinRptTpl> GetFinAsRepsTpl(SelectPeriodDataArgs args, string argRegex, string q);

        [OperationContract]
        string[] FindPrevious(string argTable, string argSecu, int argCtyp, string argRpt, int argStdtyp);

        #endregion

        #region FinRptAsrepExtSaveDao相关服务

        //[ServiceKnownType(typeof(MonFinRptAsrepExt))]
        [OperationContract]
        bool SaveAsrepExt(List<dynamic> entity, string collection);

        [OperationContract]
        MonFinRptAsrepExt GetRepExtFromAsExtRep(string table, string rpt, string tike, string y, int p, string q,
            int ctyp, string fy, int stdtyp, string accp);

        [OperationContract]
        bool GetRep(string table, string rpt, string secu, string y, int p, string q, int ctyp, string fy, int stdtyp);

        [OperationContract]
        FinRptAudit GetRepAudit(string secu, string y, string fp, string fy);

        [OperationContract]
        bool SaveRepAudit(List<FinRptAudit> rptAudit);

        [OperationContract]
        bool DeleteRepAudit(string secu, string y, string fp, string fy);

        #endregion

        #region UnitDao相关服务

        [OperationContract]
        List<MonScale> GetUnits();

        #endregion

        #region CommonDao相关服务

        [OperationContract]
        long GetSequence(string table);

        #endregion
         

    }
}
