﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.PEVC;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using ChinaScope.CSF.Contract.Entity.Sam;
using ChinaScope.CSF.Contract.Entity.ShareHolder;
using ChinaScope.CSF.Contract.UIDeriveEntity.Stock;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface ISamProductService
    {
        [OperationContract]
        string GetSecuByTick(string argTick);

        /// <summary>
        /// 查询code是否被fin_sam_product引用到
        /// true:被引用；false：未引用
        /// </summary>
        [OperationContract]
        bool IsCodeRefrenced(string code);

        [OperationContract]
        List<DictIndustry> SelectCSFIndustry(string expression);

        [OperationContract]
        List<DictIndustry> SelectCSFIndustryByCodes(string industryCode);

        [OperationContract]
        List<DictSamProduct> GetFinItemAll();

        [OperationContract]
        DictSamProduct GetFinItemByCode(string argCode);

        [OperationContract]
        void SaveMongo(DictSamProduct entity);

        [OperationContract]
        List<DictSamProduct> GetAllLevelItems(string level);

        [OperationContract]
        List<DictSamProduct> GetsubItems(string parentcode);

        [OperationContract]
        bool InsertToItem(DictSamProduct monFinitem);

        [OperationContract]
        DictSamProduct GetSamItemByEnName(string argEnName);

        [OperationContract]
        DictSamProduct GetSamItemByName(string argName);

        [OperationContract]
        DictSamProduct GetSamItemByID(int id);

        #region ProductDef相关接口

        [OperationContract]
        List<DictSamProductDef> GetItemDef();

         [OperationContract]
        List<DictSamProductDef> GetItemDefByCondition(string argSecu, string argSName, string argOName);

        [OperationContract]
        void SaveDef(DictSamProductDef entity);

        #endregion
    }
}
