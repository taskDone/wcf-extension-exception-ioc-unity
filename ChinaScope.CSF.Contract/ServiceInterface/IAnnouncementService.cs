﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.Announce;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IAnnouncementService
    {
        [OperationContract]
        List<Announcement> SelectSourceFile(Dictionary<string, string> strSearch);

        [OperationContract]
        Announcement GetSourcesByFnAndNEId(string argFn, ObjectId argId, string argTable);

        [OperationContract]
        bool SaveSources(List<Announcement> lstPevcDicts, string argTable);

        [OperationContract]
        Announcement GetSourcesByFnOrMd5(string argFn, string argMd5, string argTable);

        [OperationContract]
        List<Announcement> InsertSources(List<Announcement> lstPevcDicts, string argTable);

        [OperationContract]
        bool DeleteSources(List<Announcement> lstPevcDicts, string argTable);

        [OperationContract]
        Entity.BaseEntity.BaseStock GetBaseStockByCode(string argCode);

        [OperationContract]
        Entity.BaseEntity.BaseStock GetBaseStockByTick(string argCode);

        [OperationContract]
        DictAnnounceCatalog GetCatlogByCode(string argCode);

        [OperationContract]
        List<DictAnnounceCatalog> GetAllAnnounceCatalogs();

        [OperationContract]
        List<DictMarket> GetAllDictMarkets();

        [OperationContract]
        List<BaseStock> GetAllBaseStocks();

        /// <summary>
        /// 获取所有自定义类型
        /// </summary>
        [OperationContract]
        List<DictAnnounceCustomTyp> GetAllCustomTyps();

        /// <summary>
        /// 获取所有规则列表
        /// </summary>
        [OperationContract]
        List<DictAnnounceRule> GetAllRules();

        /// <summary>
        /// 保存规则
        /// </summary>
        [OperationContract]
        bool SaveRule(DictAnnounceRule saveentity);

        /// <summary>
        /// 删除规则
        /// </summary>
        [OperationContract]
        bool DelRule(DictAnnounceRule delentity);

        /// <summary>
        /// 保存类型
        /// </summary>
        [OperationContract]
        bool SaveCustomTyp(DictAnnounceCustomTyp saveentity);

        /// <summary>
        /// 删除类型
        /// </summary>
        [OperationContract]
        bool DelCustomType(DictAnnounceCustomTyp delentity);

        /// <summary>
        /// 判断指定的code是否在rule表中存在
        /// </summary>
        [OperationContract]
        bool IsTypExistInRule(string typcode);


    }



}
