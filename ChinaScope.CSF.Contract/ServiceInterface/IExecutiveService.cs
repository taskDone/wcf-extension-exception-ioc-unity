﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.Executive;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IExecutiveService
    {
        [OperationContract]
        List<ExecutiveModelPart> GetAllExecutives();

        [OperationContract]
        List<ExecutiveModelPart> GetExecutivesByCondition(string argName, string argOrg, string argTicker, string argStats);

        /// <summary>
        /// 仅获取高管本身相关数据，并不需要Merge成多个对象
        /// </summary>
        /// <param name="argOrglst">orgid列表</param>
        /// <param name="argName">模糊查询的名称</param>
        /// <returns></returns>
        [OperationContract]
        List<ExecutiveModel> GetExecutivelstByOrgidAndName(List<string> argOrglst, string argName);

        [OperationContract]
        DataTable GetOrgNameAllDataTable();

        [OperationContract]
        void Save(List<ExecutiveModelPart> argList);

        [OperationContract]
        void Insert(List<ExecutiveModelPart> argList);

        [OperationContract]
        void Delete(List<string> argList);

        [OperationContract]
        List<BasePeople> GetBasePeoplesByName(string argName);

        [OperationContract]
        List<BasePeople> GetBasePeoplesByNameAndRole(string argName, string argNat);

        [OperationContract]
        List<BasePeople> GetBasePeoplesByNameOrg(string argName, string argOrg);


        [OperationContract]
        List<DictExecutivePosition> GetAllDictExecutivePositions();

        [OperationContract]
        DictExecutivePosition GetDictExecutivePositionByName(string argName);

        [OperationContract]
        BasePeople InsertPeople(BasePeople argPeople);

        [OperationContract]
        void SavePeople(BasePeople argPeople);
    }
}
