﻿using System.Collections.Generic;
using System.ServiceModel;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.KeyWords;
using ChinaScope.CSF.Contract.Entity.Sam;
using ChinaScope.CSF.Contract.UIDeriveEntity.KeyWords;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IKeyWordsService
    {
        #region HotSpot

        [OperationContract]
        List<DeriveHotSpot> GetHotSpotByConditions(string abstractstr, string tags, string uptstart, string uptend,
            string stat, string mtgs);

        [OperationContract]
        bool SaveHotSpotItems(List<DeriveHotSpot> saveList);

        [OperationContract]
        List<DictSamProduct> GetAllSamlst();

        [OperationContract]
        List<BaseStock> FindBaseStockByFuzzyNameOrAbbr(string fuzzyname);

        [OperationContract]
        BaseStock FindBaseStockByName(string name);

        #endregion

        #region KeyWords

        [OperationContract]
        List<DeriveKeyWords> GetKeyWordsByConditions(string tag, string cat, string uptstart, string uptend, bool isacurrate);

        [OperationContract]
        bool SaveKeyWordsItems(List<DeriveKeyWords> saveList);

        [OperationContract]
        Keywords GetKeywordByName(string keywordname);

        /// <summary>
        /// 名称+类型唯一确定一条关键词
        /// </summary>
        /// <param name="keywordname">关键词名称</param>
        /// <param name="cat">关键词类型（一个关键词可有多个类型）</param>
        /// <returns></returns>
        [OperationContract]
        Keywords GetKeywordByNameAndCat(string keywordname,string cat);

        [OperationContract]
        List<DeriveKeyWords> GetKeyWordsByNamelst(List<string> namelst);

        /// <summary>
        /// 指定类型是否在关键词表中引用
        /// </summary>
        /// <param name="cat">指定类型</param>
        /// <returns></returns>
        [OperationContract]
        bool IsCatUsedInKeyword(string catcode);


        #region 类型相关接口

        //获取所有类别
        [OperationContract]
        List<DictCommon> GetKeyWordsAllCats();

        // 删除新闻热词下的指定分类
        [OperationContract]
        void DelCatByCode(string catcode);

        //根据中文名称获取类别
        [OperationContract]
        DictCommon GetCatBySzhName(string szhname);

        //根据英文名称获取类别
        [OperationContract]
        DictCommon GetCatByEnName(string enname);

        //获取最大的编码值
        [OperationContract]
        int GetCatMaxCode();

        //获取最大的序号
        [OperationContract]
        int GetCatMaxOrder();

        //获取最大的序号
        [OperationContract]
        bool SaveDictCommon(DictCommon saveitem);

        #endregion

        #endregion
    }
}
