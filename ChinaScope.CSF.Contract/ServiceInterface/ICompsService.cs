﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.Comps;
using ChinaScope.CSF.Contract.UIDeriveEntity.Comps;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface ICompsService
    {
        [OperationContract]
        List<DeriveFinCompsMetrics> GetAllComps();

        [OperationContract]
        List<DeriveFinCompsMetrics> GetCompsByCondition(string compsname, string parentcode, string stat);

        [OperationContract]
        bool SaveItems(List<DeriveFinCompsMetrics> saveList);

        [OperationContract]
        List<DeriveFinCompsMetrics> GetCompsBySzhName(string szhname);

        [OperationContract]
        List<DeriveFinCompsMetrics> GetCompsByEnName(string enname);

        [OperationContract]
        List<DeriveFinCompsMetrics> GetCompsByNameCd(string name, string code);

        //获取中文的ancestors
        [OperationContract]
        List<DeriveFinCompsMetrics> GetSzhAncestorsCompsByNameCd(string name, string code);

        [OperationContract]
        DeriveFinCompsMetrics GetCompsByID(string id);

        [OperationContract]
        List<DeriveFinCompsMetrics> GetCompsByCode(string code);

        [OperationContract]
        List<DeriveFinCompsMetrics> GetCompsChildsByCode(string code);

        [OperationContract]
        DeriveFinCompsMetrics GetCompsByCdANParentCd(string code, string parentcd);

        [OperationContract]
        DeriveFinCompsMetrics GetCompsByNameANParentCd(string szhname, string parentcd);

        [OperationContract]
        long GetMaxOrder();

        [OperationContract]
        bool IsExistOrder(string order);

        /// <summary>
        /// 获取所有未发布数据的信息
        /// 默认最多取5000条
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveFinCompsMetrics> GetAllUnPublishItems();
    }
}
