﻿using System.Collections.Generic;
using System.ServiceModel;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.Executive;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IExecutiveRegulationService
    {
        [OperationContract]
        List<ExecutiveRegulationModel> GetDatas(string argTicker, string argExeName, string argChangeName,
            string argChangeDate, string argSubmitDate, string argPubStat);

        [OperationContract]
        bool Save(List<ExecutiveRegulationModel> argList);

        [OperationContract]
        bool Insert(List<ExecutiveRegulationModel> lstData);

        [OperationContract]
        BaseStock GetScueCName(string secu_code);

        [OperationContract]
        List<string> GetSeculstByTicker(string ticker);

        [OperationContract]
        List<ExecutiveModel> GetExcetiveNameOrg(string name, string org);

        [OperationContract]
        List<BasePeople> GetBasePeoplesByNameAndRole(string argName, string argNat);

        /// <summary>
        /// 获取所有未发布数据的信息
        /// 默认最多取5000条
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<ExecutiveRegulationModel> GetAllUnPublishItems();

        /// <summary>
        /// 获取指定类型的股本
        /// </summary>
        /// <param name="secu">公司代码</param>
        /// <param name="vdt">日期</param>
        /// <param name="typ">总股本/流通股本</param>
        /// <returns></returns>
        [OperationContract]
        List<long> GetVary(string secu, string vdt);

        /// <summary>
        /// 根据指定的公司ticker刷新所持该公司股票的高管持股比例
        /// 包括总股本比例与流通股本比例
        /// </summary>
        /// <param name="secu">公司代码</param>
        /// <returns>是否刷新成功</returns>
        [OperationContract]
        bool RefreshRatioBySecu(string secu);
    }
}
