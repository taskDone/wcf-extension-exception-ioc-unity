﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
namespace ChinaScope.CSF.Contract.ServiceInterface
{
    /// <summary>
    /// 异步接口类
    /// 所有模块会超时操作或要与客户端交互可用此类
    /// </summary>
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IAsyncService
    {
        [OperationContract(AsyncPattern = true)]
        IAsyncResult BeginGetUnRefItemFromShareholderDef(AsyncCallback ac, object obj);

        List<BaseShareholderDef> EndGetUnRefItemFromShareholderDef(IAsyncResult ar);


    }
}
