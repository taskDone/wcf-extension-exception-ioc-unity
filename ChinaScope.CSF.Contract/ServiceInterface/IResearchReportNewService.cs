﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.Cornerstone;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using ChinaScope.CSF.Contract.UIDeriveEntity.Conerstone;
using ChinaScope.CSF.Contract.UIDeriveEntity.ResearchReport;
using DateRangewfm;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.DataContractNamespace)]
    public interface IResearchReportNewService
    {
        /// <summary>
        /// 根据条件查询
        /// </summary>
        [OperationContract]
        List<DeriveResearchReport> GetResearchByCondition(string title, string secu, string org, string researcher, string typ,
            string nullstr, string state, string startDate, string endDate);

        /// <summary>
        /// 根据条件查询原始数据
        /// 数据的解析工作拿到客户端去做，这是i因为数据量太大，服务经常返回不了结果就中断了
        /// </summary>
        [OperationContract]
        List<ResearchReportNew> GetOriginalResearchByCondition(string title, string secu, string org, string researcher, string typ,
            string nullstr, string state, string startDate, string endDate);

        /// <summary>
        /// 获取研报类型表中所有的研报类型
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<MongoTyp> SelectResearchType();

        [OperationContract]
        bool SaveItems(List<DeriveResearchReport> saveList);

        /// <summary>
        /// 与操作
        /// </summary>
        [OperationContract]
        RrResearcher GetResearcherByOrgAndName(string orgcd, string researchname);

        /// <summary>
        /// 或操作
        /// </summary>
        [OperationContract]
        List<RrResearcher> GetResearcherByOrgOrName(string orgcd, string researchname);

        [OperationContract]
        List<ResearchOrg> GetOrgByFuzzyName(string argFuzzyName);

        [OperationContract]
        List<ResearchRating> GetRatingByOrgid(string orgid);

        [OperationContract]
        List<DictIndustry> GetCSFIndustry(string expression);

        [OperationContract]
        ResearchReportDef GetResearchReportDefById(string id);

        /// <summary>
        /// 根据姓名和机构查询研究员信息
        /// </summary>
        /// <param name="name">姓名</param>
        /// <param name="orgid">机构</param>
        [OperationContract]
        List<RrResearcher> GetReserlstByNameAndOrgid(string name, string orgid);

        /// <summary>
        /// 根据证书编码查询研究员信息
        /// </summary>
        /// <param name="name">编码</param>
        [OperationContract]
        RrResearcher GetReserByCode(string code);

        /// <summary>
        /// 保存研究员信息
        /// </summary>
        [OperationContract]
        bool SaveReser(RrResearcher saveitem);

        /// <summary>
        /// 检查表里导过来的数据是否已经存在于数据库中
        /// </summary>
        [OperationContract]
        bool IsResearchReportNewExist(string vn); 
        
        /// <summary>
        /// 自动匹配摘要
        /// 按照发布时间的开始与结束时间
        /// </summary>
        [OperationContract]
        void AutoMatchReflesh(string starttime, string endtime);


        /// <summary>
        /// 保存摘要
        /// </summary>
        [OperationContract]
        ResearchReportDef SaveDef(ResearchReportDef saventity);

        /// <summary>
        /// 获取未发布的研究员
        /// </summary>
        [OperationContract]
        List<RrResearcher> GetUnpubResearcherByCodes(List<string> codelst);

        /// <summary>
        /// 获取未发布的摘要
        /// </summary>
        [OperationContract]
        List<ResearchReportDef> GetUnpubDefByIDs(List<string> idlst);

        [OperationContract]
        List<ResearchRating> SelectResearchRating();

        [OperationContract]
        List<ResearchOrg> SelectResearchOrg();

        /// <summary>
        /// 根据code列表获取研究员信息
        /// </summary>
        /// <param name="codelst"></param>
        /// <returns></returns>
        [OperationContract]
        List<RrResearcher> GetReserlstByCodes(List<string> codelst);

        [OperationContract]
        string GetNameByCode(string argCode);
    }
}
