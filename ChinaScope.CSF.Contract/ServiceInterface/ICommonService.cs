﻿using System.Collections.Generic;
using System.ServiceModel;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface ICommonService
    {
        [OperationContract]
        byte[] GetDataSource(string table, Dictionary<int, string[]> condition);

        [OperationContract]
        void Save(string table, CommonCollection t);
    }
}
