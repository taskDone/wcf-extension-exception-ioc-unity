﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.Cornerstone;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using ChinaScope.CSF.Contract.UIDeriveEntity.Conerstone;
using DateRangewfm;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.DataContractNamespace)]
    public interface IResearchReportService
    {
        /// <summary>
        /// 根据日期查询
        /// </summary>
        [OperationContract(Name = "SelectResearchRptByDate")]
        List<ResearchRpt> SelectResearchRpt(DateTime startDate, DateTime endDate);

        /// <summary>
        /// 根据关键字查询
        /// </summary>   
        [OperationContract(Name = "SelectResearchRptByTitle")]
        List<ResearchRpt> SelectResearchRpt(string title);

        /// <summary>
        /// 根据日期、标题
        /// </summary>
        [OperationContract(Name = "SelectResearchRptByDateAndTitle")]
        List<ResearchRpt> SelectResearchRpt(DateTime startDate, DateTime endDate, string title);

        /// <summary>
        /// 根据日期查询
        /// </summary>
        [OperationContract]
        List<ResearchRpt> SelectRptsByWhop(SelectDateResult selectQuery);

        /// <summary>
        /// 根据ID删除
        /// </summary>
        /// <param name="argID"></param>
        /// <returns></returns>
        [OperationContract]
        bool DeleteByID(int argID);

        /// <summary>
        /// 根据ID获取研报记录
        /// </summary>
        /// <param name="argID"></param>
        /// <returns></returns>
        [OperationContract]
        ResearchRpt GetRptByID(int argID);

        /// <summary>
        /// 保存一条记录
        /// </summary>
        /// <param name="argID"></param>
        /// <returns></returns>
        [OperationContract]
        bool SaveSingle(ResearchRpt saverpt);

        /// <summary>
        /// 获取所有评级记录
        /// </summary>   
        [OperationContract]
        List<ResearchRating> SelectResearchRating();

        /// <summary>
        /// 按简称查询出全称
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [OperationContract]
        string GetCutName(string argAbbrName);

        [OperationContract]
        BaseStock SearchByCode(string code);

        [OperationContract]
        ResearchOrg SelectResearchOrgByCode(string code);

        [OperationContract]
        List<Researcher> SelectResearcherByCodes(string analystCode);

        [OperationContract]
        MongoTyp SelectResearchTypeByCode(string typeCode);

        [OperationContract]
        ResearchRating SelectResearchRatingByCode(string argCode, string argType, string csfrCode);

        [OperationContract]
        List<DictIndustry> SelectDictIndustryByCodes(string industryCode);

        [OperationContract]
        bool Save(List<ResearchRpt> savelstRpts, bool argIsPublish, string argCreatIndex, string argPublishServer);

        [OperationContract]
        BaseOrg SelectBaseOrgById(string id);

        /// <summary>
        /// 通过四次模糊查询 取并集
        /// </summary>
        /// <param name="argKeyWord"></param>
        /// <returns></returns>
        [OperationContract]
        List<BaseStock> SearchByKeyWord(string argKeyWord);

        [OperationContract]
        ResearchRating GetRatingInfo(string argCode, string argType, string argRatting);

        [OperationContract]
        BsonDocument GetOrgIdBySecu(string argSecu);

        [OperationContract]
        List<Researcher> SelectResearcher(string txtSearch);

        [OperationContract]
        List<MongoTyp> SelectResearchType();

        [OperationContract]
        List<ResearchOrg> SelectResearchOrg();

        [OperationContract]
        List<DictIndustry> SelectDictIndustry(string expression);

        [OperationContract]
        long GetSequence(string table);

        [OperationContract]
        string PublishWithService(string url);
    }
}
