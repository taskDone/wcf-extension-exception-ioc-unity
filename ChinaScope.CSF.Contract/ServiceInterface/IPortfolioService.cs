﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.ShareHolder;
using ChinaScope.CSF.Contract.UIDeriveEntity.Portfolio;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IPortfolioService
    {
        [OperationContract]
        List<DeriveBasePortfolio> GetAllPortfolios();

        [OperationContract]
        List<DeriveBasePortfolio> GetByContidion(string name, string typ, string stat);

        [OperationContract]
        bool SaveItems(List<DeriveBasePortfolio> saveList);

        [OperationContract]
        DeriveBasePortfolio GetItemBySzhName(string szhname);

        [OperationContract]
        DeriveBasePortfolio GetItemByEnName(string enname);

        [OperationContract]
        List<DictShareholderType> GetAllShareholerTyps();

        /// <summary>
        /// 获取所有未发布数据的信息
        /// 默认最多取5000条
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DeriveBasePortfolio> GetAllUnPublishItems();
    }
}
