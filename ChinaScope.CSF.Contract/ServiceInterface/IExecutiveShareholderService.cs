﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.Executive;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IExecutiveShareholderService
    {
        [OperationContract]
        List<BaseExecutiveShareholder> GetExecutivesByCondition(string argName, string argOrg, string argStats,
            string argDateFrom, string argDateTo);

        [OperationContract]
        void Save(List<BaseExecutiveShareholder> argList);

        [OperationContract]
        void Insert(List<BaseExecutiveShareholder> argList);

        [OperationContract]
        void Delete(List<string> argList);

        [OperationContract]
        List<StockPart> FindStockByCode(string argCode);

        [OperationContract]
        List<string> GetCodeByOrgId(string argOrgId);

        /// <summary>
        /// 获取所有货币
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<DictCurrency> GetAllCurrency();
    }
}
