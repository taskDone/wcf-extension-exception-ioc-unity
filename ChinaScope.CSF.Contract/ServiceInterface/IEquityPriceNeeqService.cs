﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.Comps;
using ChinaScope.CSF.Contract.Entity.EquityPrice;
using ChinaScope.CSF.Contract.UIDeriveEntity.Comps;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IEquityPriceNeeqService
    {
        [OperationContract]
        DataTable GetEquityPriceNeeqsTickDt(string ticker, DateTime dt,string table);

        [OperationContract]
        DataTable GetEquityPriceNeeqsTickPeriod(string ticker, DateTime from_dt, DateTime to_dt, string table);

        [OperationContract]
        DataTable GetEquityPriceNeeqsPeriod(DateTime from_dt, DateTime to_dt, string table);

        [OperationContract]
        bool Insert(List<string> arg);

        [OperationContract]
        DataTable GetEquityPrice(string secu, string to_dt, string table);
    }
}
