﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.Index;
using ChinaScope.CSF.Contract.UIDeriveEntity.Index;

namespace ChinaScope.CSF.Contract.ServiceInterface
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IIndexSpecimenStockService
    {
        [OperationContract]
        List<DeriveIndexSpecimenStock> GetByCode(string code);

        [OperationContract]
        List<DeriveIndexSpecimenStock> GetDataByConditions(Dictionary<int, string[]> args);

        [OperationContract]
        double GetMarketCap(string secu_code);

        [OperationContract]
        string GetScueCName(string secu_code);

        [OperationContract]
        List<CNEN> GetSpecimenIndexs();

        [OperationContract]
        bool Save(List<DeriveIndexSpecimenStock> lst);

        [OperationContract]
        bool SaveIndexExChange(List<IndexExChange> lst);

        [OperationContract]
        List<DeriveIndexSpecimenStock> GetByCodeStat(string indexName, string secu_code, string stat, string indexCode);

        [OperationContract]
        List<IndexExChange> GetIndexExChange(string secu, string stat);

        [OperationContract]
        List<DeriveIndexChange> GetIndexChange(string indexName, string tradeDate, string secuCode, string stat,
            string indexCode);

        [OperationContract]
        bool SaveIndexChange(List<DeriveIndexChange> lst);

        #region DictIndex相关接口

        [OperationContract]
        List<DictIndex> GetAllIndex();

        [OperationContract]
        bool InsertItem(DictIndex item);

        [OperationContract]
        bool RemoveItem(DictIndex item);

        [OperationContract]
        bool SaveItem(DictIndex item);

        /// <summary>
        /// 指数字典是否被引用
        /// </summary>
        /// <param name="code">代码</param>
        /// <returns></returns>
        [OperationContract]
        bool IsCodeRefBySpeciStock(string code);

        [OperationContract]
        bool SaveIndexs(List<DictIndex> indexlst);

        #endregion

    }
}
