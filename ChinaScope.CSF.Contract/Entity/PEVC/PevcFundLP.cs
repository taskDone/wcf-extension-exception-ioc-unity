﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public class PevcFundLP
    {
        public const string Collection = "pevc_fund_lp";

        #region 自段定义

        public string _id { get; set; }

        public string fid { get; set; }

        public CNEN name { get; set; }

        public CNEN abbr { get; set; }

        public IDCNEN lp { get; set; }

        public CDCNEN typ { get; set; }

        public Accpt accpt { get; set; }

        public Accpt inv { get; set; }

        public string org { get; set; }

        public string upu { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime upt { get; set; }

        public int src { get; set; }

        public string rem { get; set; }

        public string status { get; set; }

        public string crt { get; set; }

        #endregion

        public PevcFundLP()
        {
            this._id = "";
            this.abbr = new CNEN();
            this.accpt = new Accpt();
            this.crt = "";
            this.fid = "";
            this.inv = new Accpt();
            this.lp = new IDCNEN();
            this.name = new CNEN();
            this.org = "";
            this.rem = "";
            this.src = 0;
            this.status = "";
            this.typ = new CDCNEN();
            this.upt = new DateTime();
            this.upu = "";

        }
    }

    public class Accpt
    {

        public string amt { get; set; }

        public string c { get; set; }

        public Accpt()
        {
            amt = "";
            c = "";
        }
    }
}
