﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
   public class PevcProjectDetail
    {
        public const string Collection = "pevc_project_detail";

        public string _id { get; set; }

        public IDCNEN org { get; set; }

        public object lst { get; set; }

        public object lstd { get; set; }

        public CDCNEN secu { get; set; }

        public string invs { get; set; }

        public CDCNEN invm { get; set; }

        public object fd { get; set; }

        public string fn { get; set; }

        public List<CDCNEN> invfund { get; set; }

        public CDCNEN invfund1 { get; set; }

        public List<TypCDCNEN> invorg { get; set; }

        public CDCNEN invorg1 { get; set; }

        public List<CDCNEN> inv { get; set; }

        public CDCNEN inv1 { get; set; }

        public string sh { get; set; }

        public Price price { get; set; }

        public CDCNEN stage { get; set; }

        public long shares { get; set; }

        public double total { get; set; }

        public Amt amt { get; set; }

        public Eval eval { get; set; }

        public string bror { get; set; }

        public string src { get; set; }

        public CDCNEN reveal { get; set; }

        public CNEN desc { get; set; }

        public string upu { get; set; }

        public DateTime upt { get; set; }

        public string audu { get; set; }

        public string audt { get; set; }

        public string rem { get; set; }

        public string upd { get; set; }

        public string status { get; set; }

        public string crt { get; set; }

        public PevcProjectDetail()
        {
            this._id = "";
            this.amt=new Amt();
            this.audt = "";
            this.audu = "";
            this.bror = "";
            this.crt = "";
            this.desc=new CNEN();
            this.eval=new Eval();
            this.fn = "";
            this.inv=new List<CDCNEN>();
            this.inv1=new CDCNEN();
            this.invfund=new List<CDCNEN>();
            this.invfund1=new CDCNEN();
            this.invm=new CDCNEN();
            this.invorg=new List<TypCDCNEN>();
            this.invorg1=new CDCNEN();
            this.invs = "";
            this.org = new IDCNEN();
            this.price=new Price();
            this.rem = "";
            this.reveal=new CDCNEN();
            this.secu=new CDCNEN();
            this.sh = "";
            this.shares = 0;
            this.src = "";
            this.stage=new CDCNEN();
            this.status = "";
            this.total = 0;
            this.upd = "";
            this.upt=new DateTime();
            this.upu = "";
        }
    }

    public class UC
    {
        public string u { get; set; }
        public string c { get; set; }

        public UC()
        {
            u = "";
            c = "";
        }
    }

    public class Price : UC
    {
        public string amt { get; set; }

        public Price()
        {
            amt = "";
        }
    }

    public class Amt : UC
    {
        public string v { get; set; }
        public string cal { get; set; }
        public string usm { get; set; }

        public Amt()
        {
            v = "";
            cal = "";
            usm = "";
        }
    }

    public class Eval : UC
    {
        public string amt { get; set; }

        public string usm { get; set; }

        public Eval()
        {
            amt = "";
            usm = "";
        }

    }
}
