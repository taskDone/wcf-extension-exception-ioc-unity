﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public class PevcPeople
    {
        public const string Collection = "pevc_people";

        #region 属性

        public string _id { get; set; }

        private PYCNEN _name;

        public PYCNEN name
        {
            get
            {
                if (_name == null)
                    return new PYCNEN();
                else
                {
                    return _name;
                }
            }
            set { _name = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string cn { get { return name.szh; } }
        public string en { get { return name.en; } }

        /// <summary>
        /// 
        /// </summary>
        public string cov { get; set; }

        private CDCNEN _org;

        /// <summary>
        /// 说明
        /// </summary>
        public CDCNEN org
        {
            get
            {
                if (_org == null)
                    return new CDCNEN();
                else
                {
                    return _org;
                }
            }
            set { _org = value; }
        }

        public string zip { get; set; }

        public string email { get; set; }

        public string mobi { get; set; }

        public string tel { get; set; }

        private CNEN _post;
        public CNEN post
        {
            get
            {
                if (_post == null)
                    return new CNEN();
                else
                {
                    return _post;
                }
            }
            set { _post = value; }
        }

        public string pstart { get; set; }

        public string pend { get; set; }

        public string mul { get; set; }

        public string activ { get; set; }

        private CNEN _addr;
        public CNEN addr
        {
            get
            {
                if (_addr == null)
                    return new CNEN();
                else
                {
                    return _addr;
                }
            }
            set { _addr = value; }
        }

        public string fax { get; set; }

        public object bd { get; set; }

        private CNEN _profile;
        public CNEN profile
        {
            get
            {
                if (_profile == null)
                    return new CNEN();
                else
                {
                    return _profile;
                }
            }
            set { _profile = value; }
        }

        public string upu { get; set; }

        public string upt { get; set; }

        public string audu { get; set; }

        public string audt { get; set; }

        public string rem { get; set; }

        /// <summary>
        /// 教育背景
        /// </summary>
        public string edu { get; set; }

        /// <summary>
        /// 教育背景
        /// </summary>
        [BsonIgnore]
        public string edubg { get; set; }

        public string status { get; set; }

        public string crt { get; set; }

        #endregion

        public PevcPeople()
        {
            this._id = "";
            this.activ = "";
            this.audt = "";
            this.audu = "";
            this.cov = "";
            this.crt = "";
            this.edu = "";
            this.email = "";
            this.fax = "";
            this.mobi = "";
            this.mul = "";
            this.pend = "";
            this.pstart = "";
            this.rem = "";
            this.status = "";
            this.tel = "";
            this.upt = "";
            this.upu = "";
            this.zip = "";
        }

        #region 方法

        #endregion
    }
}
