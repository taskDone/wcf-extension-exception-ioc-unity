﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public enum Operation
    {
        Update = 0,
        Add = 1,
        Delete = 2,
        None = 3
    }
}
