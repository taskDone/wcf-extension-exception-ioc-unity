﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public class PevcDict
    {
        public const string Collection = "pevc_dict";

        #region 属性

        public ObjectId Id { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public CDCNEN type { get; set; }

        /// <summary>
        /// 代码
        /// </summary>
        public string code { get; set; }

        public CNEN value { get; set; }

        /// <summary>
        /// 属性
        /// </summary>
        public string prop { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        public string desc { get; set; }

        public string upu { get; set; }

        public DateTime upt { get; set; }

        public string adu { get; set; }

        public DateTime adut { get; set; }

        public string TypName { get { return type.szh; } }

        public string TypCode { get { return type.cd; } }

        public string ValueSzh { get { return value.szh; } }

        public string status { get; set; }

        public string crt { get; set; }

        public PevcDict()
        {
            this.Id = new ObjectId();
            this.adu = "";
            this.adut = new DateTime();
            this.code = "";
            this.crt = "";
            this.desc = "";
            this.prop = "";
            this.status = "";
            this.type = new CDCNEN();
            this.upt = new DateTime();
            this.upu = "";
            this.value = new CNEN();
        }

        #endregion

        #region 方法

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}-{3}-{4}",
                type.ToString(),
                code,
                value.ToString(),
                prop,
                desc);
        }

        #endregion
    }
}
