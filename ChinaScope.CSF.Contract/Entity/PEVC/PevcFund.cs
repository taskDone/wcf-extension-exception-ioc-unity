﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public class PevcFund
    {
        public const string Collection = "pevc_fund";

        #region 属性

        public string _id { get; set; }

        public CNEN name { get; set; }

        public CNEN abbr { get; set; }

        public CDCNEN typ { get; set; }

        public List<IDCNEN> org { get; set; }

        public CDCNEN typo { get; set; }

        public CDCNEN invs { get; set; }

        public CDCNEN invarea { get; set; }

        public AppealState collect { get; set; }

        public string invy { get; set; }

        public CDCNEN area { get; set; }

        public string ratio { get; set; }

        public Limit limit { get; set; }

        public CDCNEN invst { get; set; }

        public CDCNEN invind { get; set; }

        public CNEN livp { get; set; }

        public CNEN invcyc { get; set; }

        public CNEN cbank { get; set; }

        public CNEN lawf { get; set; }

        public CNEN audf { get; set; }

        public CNEN qc { get; set; }

        public CDCNEN mte { get; set; }

        public CNEN mtb { get; set; }

        public string mgrc { get; set; }

        public CNEN intro { get; set; }

        public Accu accu { get; set; }

        public long src { get; set; }

        public string cru { get; set; }

        public string cut { get; set; }

        public DateTime crt { get; set; }

        public DateTime ts { get; set; }

        public string u { get; set; }

        public string t { get; set; }

        public string rem { get; set; }

        public string cov { get; set; }

        public string status { get; set; }

        public string upu { get; set; }

        public string upt { get; set; }

        [BsonIgnore]
        public string CN
        {
            get
            {
                return name.szh;
            }
        }
        [BsonIgnore]
        public string EN
        {
            get
            {
                return name.en;
            }
        }




        #endregion

        public PevcFund()
        {
            this._id = "";
            this.abbr = new CNEN();
            this.accu = new Accu();
            this.area = new CDCNEN();
            this.audf = new CNEN();
            this.cbank = new CNEN();
            this.collect = new AppealState();
            this.cov = "";
            this.crt = new DateTime();
            this.cru = "";
            this.cut = "";
            this.intro = new CNEN();
            this.invarea = new CDCNEN();
            this.invcyc = new CNEN();
            this.invind = new CDCNEN();
            this.invs = new CDCNEN();
            this.invst = new CDCNEN();
            this.invy = "";
            this.lawf = new CNEN();
            this.limit = new Limit();
            this.livp = new CNEN();
            this.mgrc = "";
            this.mtb = new CNEN();
            this.mte = new CDCNEN();
            this.name = new CNEN();
            this.org = new List<IDCNEN>();
            this.qc = new CNEN();
            this.ratio = "";
            this.rem = "";
            this.src = 0;
            this.status = "";
            this.t = "";
            this.ts = new DateTime();
            this.typ = new CDCNEN();
            this.typo = new CDCNEN();
            this.u = "";
            this.upt = "";
            this.upu = "";
        }
    }

    public class Accu
    {
        public Inv inv { get; set; }
        public Wd wd { get; set; }
        public string t { get; set; }
        public string u { get; set; }

        public Accu()
        {
            this.inv = new Inv();
            this.wd = new Wd();
            this.t = "";
            this.u = "";
        }
    }

    public class Wd
    {
        public string amt { get; set; }
        public long n { get; set; }

        public Wd()
        {
            this.amt = "";
            this.n = 0;
        }
    }

    public class Inv
    {
        public string amt { get; set; }
        public long evs { get; set; }

        public Inv()
        {
            this.amt = "";
            this.evs = 0;
        }
    }

    public class Limit
    {
        public string lower { get; set; }
        public string upper { get; set; }
        public string c { get; set; }

        public Limit()
        {
            lower = "";
            upper = "";
            c = "";
        }
    }

    public class AppealState
    {
        public CDCNEN st { get; set; }
        public string obj { get; set; }
        public object beg { get; set; }
        public object endt { get; set; }
        public Aoi aoi { get; set; }
        public string szh { get; set; }
        public CDCNEN cs { get; set; }
        public CDCNEN c { get; set; }
        public Sca tsca { get; set; }
        public Sca fsca { get; set; }

        public AppealState()
        {
            st = new CDCNEN();
            obj = "";
            beg = "";
            endt = "";
            aoi = new Aoi();
            szh = "";
            cs = new CDCNEN();
            c = new CDCNEN();
            tsca = new Sca();
            fsca = new Sca();
        }
    }

    public class Aoi
    {
        public string amt { get; set; }
        public string c { get; set; }
        public string szh { get; set; }

        public Aoi()
        {
            amt = "";
            c = "";
            szh = "";
        }
    }

    public class Sca : Aoi
    {
        public string us { get; set; }

        public Sca()
        {
            us = "";
        }
    }
}
