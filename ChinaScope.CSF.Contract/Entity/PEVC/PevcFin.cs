﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public class PevcFin
    {
        public const string Collection = "pevc_fin";

        public int _id { get; set; }

        public string typ { get; set; }

        public object rp { get; set; }

        public string pid { get; set; }

        public CNEN name { get; set; }

        public string cov { get; set; }

        public string cas { get; set; }

        public string fixas { get; set; }

        public string tas { get; set; }

        public string clia { get; set; }

        public string tlia { get; set; }

        public string pequ { get; set; }

        public string oequ { get; set; }

        public string income { get; set; }

        public string othinc { get; set; }

        public string opcost { get; set; }

        public string fcost { get; set; }

        public string ioi { get; set; }

        public string othpf { get; set; }

        public string oppf { get; set; }

        public string nopexp { get; set; }

        public string pbt { get; set; }

        public string taxexp { get; set; }

        public string netpf { get; set; }

        public string netcfo { get; set; }

        public string netcfi { get; set; }

        public string netcff { get; set; }

        public string eps { get; set; }

        public CNEN u { get; set; }

        public CNEN c { get; set; }

        public string rtyp { get; set; }

        //1:是 2：否
        public string audit { get; set; }

        public CNENTIK src { get; set; }

        public CNEN ab { get; set; }

        public string upt { get; set; }

        public string upu { get; set; }

        public string rem { get; set; }

        public string status { get; set; }

        public string crt { get; set; }

        public PevcFin()
        {
            this._id = 0;
            this.ab = new CNEN();
            this.audit = "";
            this.c = new CNEN();
            this.cas = "";
            this.clia = "";
            this.cov = "";
            this.crt = "";
            this.eps = "";
            this.fcost = "";
            this.fixas = "";
            this.income = "";
            this.ioi = "";
            this.name = new CNEN();
            this.netcff = "";
            this.netcfi = "";
            this.netcfo = "";
            this.netpf = "";
            this.nopexp = "";
            this.oequ = "";
            this.opcost = "";
            this.oppf = "";
            this.othinc = "";
            this.othpf = "";
            this.pbt = "";
            this.pequ = "";
            this.pid = "";
            this.rem = "";
            this.rp = new object();
            this.rtyp = "";
            this.src = new CNENTIK();
            this.status = "";
            this.tas = "";
            this.taxexp = "";
            this.tlia = "";
            this.typ = "";
            this.u = new CNEN();
            this.upt = "";
            this.upu = "";
        }

    }
}
