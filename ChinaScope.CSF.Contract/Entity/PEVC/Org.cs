﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public class Org
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Ename { get; set; }

        public Org()
        {
            Code = "";
            Name = "";
            Ename = "";
        }
    }
}
