﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public class PevcOrg
    {
        public const string Collection = "pevc_org";

        #region 属性

        public string _id { get; set; }

        public CNEN name { get; set; }

        public CNEN abbr { get; set; }

        public string fn { get; set; }

        public string cov { get; set; }

        public string prop { get; set; }

        public CDCNEN typ { get; set; }

        public string url { get; set; }

        public CDCNEN hq { get; set; }

        public object ft { get; set; }

        public string dt { get; set; }

        public Fund fund { get; set; }

        public CDCNEN stc { get; set; }

        public CNEN offi { get; set; }

        public CNEN profi { get; set; }

        public string upu { get; set; }

        public object upt { get; set; }

        public string audt { get; set; }

        public int src { get; set; }

        public string rem { get; set; }

        public CNEN addr { get; set; }

        public string tel { get; set; }

        public string fax { get; set; }

        public string email { get; set; }

        public string rect { get; set; }

        public CDCNEN invst { get; set; }

        public string limit_lower { get; set; }

        public string limit_upper { get; set; }

        public string limit_c { get; set; }

        public CNEN invs { get; set; }

        public string status { get; set; }

        public object crt { get; set; }





        #endregion

        public PevcOrg()
        {
            this._id = "";
            this.abbr = new CNEN();
            this.addr = new CNEN();
            this.audt = "";
            this.cov = "";
            this.crt = "";
            this.dt = "";
            this.email = "";
            this.fax = "";
            this.fn = "";
            this.ft = new object();
            this.fund = new Fund();
            this.hq = new CDCNEN();
            this.invs = new CNEN();
            this.invst = new CDCNEN();
            this.limit_c = "";
            this.limit_lower = "";
            this.limit_upper = "";
            this.name = new CNEN();
            this.offi = new CNEN();
            this.profi = new CNEN();
            this.prop = "";
            this.rect = "";
            this.rem = "";
            this.src = 0;
            this.status = "";
            this.stc = new CDCNEN();
            this.tel = "";
            this.typ = new CDCNEN();
            this.upu = "";
            this.url = "";
        }

        #region 方法

        //public override string ToString()
        //{
        //    return string.Format("{0}-{1}-{2}-{3}-{4}",
        //        type.ToString(),
        //        code,
        //        value.ToString(),
        //        prop,
        //        desc);
        //}

        #endregion
    }

    /// <summary>
    /// 构造机构表中的一个局部类
    /// </summary>
    public class Fund
    {
        #region 属性

        public string v { get; set; }

        public CDCNEN u { get; set; }

        public CDCNEN c { get; set; }

        public string usm { get; set; }

        #endregion

        public Fund()
        {
            v = "";
            u = new CDCNEN();
            c = new CDCNEN();
            usm = "";
        }

    }
}
