﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public class PevcManagement
    {
        public const string Collection = "pevc_management";

        #region 属性

        public ObjectId Id { get; set; }

        /// <summary>
        /// 人物ID
        /// </summary>
        public string pid { get; set; }

        private PYCNEN _name;
        /// <summary>
        /// 
        /// </summary>
        public PYCNEN name
        {
            get
            {
                if (_name == null)
                    _name = new PYCNEN();
                return _name;
            }
            set { _name = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string cov { get; set; }

        private TypCDCNEN _org;
        /// <summary>
        /// 说明
        /// </summary>
        public TypCDCNEN org
        {
            get
            {
                if (_org == null)
                    _org = new TypCDCNEN();
                return _org;
            }
            set { _org = value; }
        }

        private CNEN _post;
        public CNEN post
        {
            get
            {
                if (_post == null)
                    _post = new CNEN();
                return _post;
            }
            set { _post = value; }
        }

        public string dept { get; set; }

        public string activ { get; set; }

        private CNEN _addr;
        public CNEN addr
        {
            get
            {
                if (_addr == null)
                    _addr = new CNEN();
                return _addr;
            }
            set { _addr = value; }
        }

        public string fax { get; set; }

        public string email { get; set; }

        public string mobi { get; set; }

        public string tel { get; set; }

        public string serv { get; set; }

        public string upu { get; set; }

        public string upt { get; set; }

        public string audu { get; set; }

        public string audt { get; set; }

        public string rem { get; set; }

        public string status { get; set; }

        public string crt { get; set; }

        [BsonIgnore]
        public string CN
        {
            get
            {
                return name.szh;
            }
        }
        [BsonIgnore]
        public string EN
        {
            get
            {
                return name.en;
            }
        }

        [BsonIgnore]
        public string ORG_CN
        {
            get
            {
                return org.szh;
            }
        }

        #endregion

        public PevcManagement()
        {
            this.Id = new ObjectId();
            this.activ = "";
            this.audt = "";
            this.audu = "";
            this.cov = "";
            this.crt = "";
            this.dept = "";
            this.email = "";
            this.fax = "";
            this.mobi = "";
            this.pid = "";
            this.rem = "";
            this.serv = "";
            this.status = "";
            this.tel = "";
            this.upt = "";
            this.upu = "";
        }

        #region 方法

        #endregion
    }
}
