﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public class PevcProjectShareholder
    {
        public const string Collection = "pevc_project_shareholder";

        public int _id { get; set; }

        public string pid { get; set; }

        public object rp { get; set; }

        public CNEN name { get; set; }

        public TypCDCNEN holder { get; set; }

        public string ratio { get; set; }

        public object dd { get; set; }

        public CNENTIK src { get; set; }

        public string rem { get; set; }

        public string status { get; set; }

        public string upt { get; set; }

        public string crt { get; set; }

        public PevcProjectShareholder()
        {
            this._id = 0;
            this.crt = "";
            this.holder = new TypCDCNEN();
            this.name = new CNEN();
            this.pid = "";
            this.ratio = "";
            this.rem = "";
            this.src = new CNENTIK();
            this.status = "";
            this.upt = "";
        }
    }
}
