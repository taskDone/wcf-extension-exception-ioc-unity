﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.PEVC
{
    public class PevcProject
    {
        public const string Collection = "pevc_project";

        #region 属性

        public string _id { get; set; }

        public CNEN name { get; set; }

        public CNEN abbr { get; set; }

        public string cov { get; set; }

        private CDCNEN _st = null;
        public CDCNEN st
        {
            get
            {
                if (_st == null)
                {
                    _st = new CDCNEN();
                }
                return _st;
            }

            set
            {
                _st = value;
            }


        }

        public CDCNEN mkt { get; set; }

        public string tik { get; set; }


        public object lt { get; set; }   //该类型应该是DateTime,但是在数据库中 有时间,null,和"" 3类型,不用OBJECT无法处理

        public string sepr { get; set; }

        public string Type { get; set; }

        public Contact contact { get; set; }

        public Regi regi { get; set; }

        public Ind ind { get; set; }

        public CNEN intro { get; set; }

        public Acc acc { get; set; }

        public string rem { get; set; }

        public string upt { get; set; }

        public string upu { get; set; }

        public DateTime crt { get; set; }

        public string status { get; set; }

        [BsonIgnore]
        public string CN
        {
            get
            {
                return name.szh;
            }
        }
        [BsonIgnore]
        public string EN
        {
            get
            {
                return name.en;
            }
        }

        #endregion

        public PevcProject()
        {
            this._id = "";
            this.Type = "";
            this.abbr = new CNEN();
            this.acc = new Acc();
            this.contact = new Contact();
            this.cov = "";
            this.crt = new DateTime();
            this.ind = new Ind();
            this.intro = new CNEN();
            this.mkt = new CDCNEN();
            this.name = new CNEN();
            this.regi = new Regi();
            this.rem = "";
            this.sepr = "";
            this.st = new CDCNEN();
            this.status = "";
            this.tik = "";
            this.upt = "";
            this.upu = "";

        }
    }

    public class Ind
    {
        //public CDCNEN sic { get; set; }
        //public CDCNEN sywg { get; set; }
        //public CDCNEN csrc { get; set; }
        public CDCNEN csf { get; set; }
        public CDCNEN gb { get; set; }

        public Ind()
        {
            //sic = new CDCNEN();
            //sywg = new CDCNEN();
            //csrc = new CDCNEN();
            csf = new CDCNEN();
            gb = new CDCNEN();
        }
    }

    public class Regi
    {
        public string brc { get; set; }
        public string oc { get; set; }
        public string lrep { get; set; }
        public object ft { get; set; }
        public string emp { get; set; }
        public CDCNEN addr { get; set; }
        public CNEN prd { get; set; }

        public Regi()
        {
            brc = "";
            oc = "";
            lrep = "";
            emp = "";
            addr = new CDCNEN();
            prd = new CNEN();
        }
    }

    public class Contact
    {
        public CNEN addr { get; set; }

        public string zip { get; set; }

        public string cont { get; set; }
        public string tel { get; set; }
        public string fax { get; set; }
        public string mail { get; set; }
        public string url { get; set; }

        public Contact()
        {
            addr = new CNEN();
            zip = "";
            tel = "";
            fax = "";
            mail = "";
            url = "";
        }
    }

    public class Acc
    {
        public long amt { get; set; }
        public string c { get; set; }
        public string u { get; set; }

        public Acc()
        {
            amt = 0;
            c = "";
            u = "";
        }
    }
}
