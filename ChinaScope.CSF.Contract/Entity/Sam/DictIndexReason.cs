﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Sam
{
    public class DictIndexReason
    {
        public const string Collection = "dict_index_reason";

        public long _id { get; set; }

        public string rsen { get; set; }

        public string rsszh { get; set; }

        /// <summary>
        /// 原因分类 sam:0,产品树1，容错转移2
        /// </summary>
        public int typ { get; set; }

        /// <summary>
        /// upu
        /// </summary>
        public string upu { get; set; }

        /// <summary>
        /// upt
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? upt { get; set; }

        public int? stat { get; set; }
    }
}
