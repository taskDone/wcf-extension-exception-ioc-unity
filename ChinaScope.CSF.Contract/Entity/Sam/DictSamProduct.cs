﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Sam
{
    public class DictSamProduct
    {
        public const string Collection = "dict_product_rs";

        public int _id { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// 中英缩写名
        /// </summary>
        public CNEN name { get; set; }

        [BsonIgnore]
        public string CN
        {
            get
            {
                return name.szh;
            }
        }

        /// <summary>
        /// 备注中英文
        /// </summary>
        public CNEN rem { get; set; }


        /// <summary>
        /// 明细科目使用，属于哪个主要科目
        /// </summary>
        public string parent { get; set; }

        /// <summary>
        /// ancestors
        /// </summary>
        public List<string> ancestors { get; set; }

        /// <summary>
        /// level
        /// </summary>
        public string level { get; set; }

        /// <summary>
        /// comb
        /// </summary>
        //public List<string> comb { get; set; }  要改回来
        public string comb { get; set; }

        /// <summary>
        /// active
        /// </summary>
        //public int active { get; set; }

        /// <summary>
        /// upu
        /// </summary>
        public string upu { get; set; }

        /// <summary>
        /// upt
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? upt { get; set; }

        /// <summary>
        /// crt
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? crt { get; set; }


        /// <summary>
        /// valid
        /// </summary>
        public string valid { get; set; }

        /// <summary>
        /// 对港美股容错
        /// </summary>
        public List<string> rs { get; set; }

        /// <summary>
        /// ind
        /// </summary>
        public List<TitleCode> ind { get; set; }

        public int? stat { get; set; }

        public string note { get; set; }  //内部使用的备注 updateby204140324


    }
}
