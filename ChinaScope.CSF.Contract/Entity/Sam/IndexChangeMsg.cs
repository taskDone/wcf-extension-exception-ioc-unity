﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Sam
{
    public class IndexChangeMsg
    {
        public const string Collection = "index_change_message";

        public ObjectId _id { get; set; }

        public string code { get; set; }

        //public string rcode { get; set; }

        /// <summary>
        /// 消息来源分类0：sam，1:sam容错，2：产品树调整，3:新公司
        /// </summary>
        public int cls { get; set; }


        /// <summary>
        /// 更新方式：true:常规，false:非常规
        /// </summary>
        public bool umde { get; set; }

        /// <summary>
        /// 原因code
        /// </summary>
        public string rzcd { get; set; }

        /// <summary>
        /// 变动因素
        /// </summary>
        public string ele { get; set; }

        /// <summary>
        /// 变动影响
        /// </summary>
        public List<ChangeAffect> rt { get; set; }

        /// <summary>
        /// upu
        /// </summary>
        public string upu { get; set; }

        /// <summary>
        /// upt
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? upt { get; set; }

        /// <summary>
        /// 0:未计算 1：计算中 2：已计算
        /// </summary>
        public int iscalc { get; set; }
    }

    public class ChangeAffect
    {
        public string secu { get; set; }
        public string v { get; set; }
    }
}
