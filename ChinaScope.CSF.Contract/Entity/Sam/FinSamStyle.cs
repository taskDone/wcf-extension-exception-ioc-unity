﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Sam
{
    [Serializable]
    public class FinSamStyle
    {
        public const string Collection = "fin_sam_style";

        public int _id { get; set; }

        /// <summary>
        /// 报表类型（is/bs/cf）
        /// </summary>    
        public string rpt { get; set; }

        /// <summary>
        /// ticker股票代码
        /// </summary>  
        public string tik { get; set; }

        /// <summary>
        /// 安全代码(格式如：601398_SH_EQ)
        /// </summary>   
        public string secu { get; set; }

        //public string org { get; set; }

        /// <summary>
        /// 合并准则
        /// </summary>  
        public int ctyp { get; set; }

        /// <summary>
        /// 准则类型
        /// </summary>   
        public int stdtyp { get; set; }

        /// <summary>
        /// 默认为通用"11"，否则为金融"2*"
        /// </summary> 
        public string typ { get; set; }

        public string accp { get; set; }

        /// <summary>
        /// remark
        /// </summary>   
        public string rem { get; set; }

        /// <summary>
        /// 录入人
        /// </summary>  
        public string upu { get; set; }

        /// <summary>
        /// 录入时间
        /// </summary>   
        public DateTime? crt { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>    
        public DateTime? upt { get; set; }

        public int? stat { get; set; }

        /// <summary>
        /// 明细
        /// </summary>
        public List<SamStyleItem> items { get; set; }

    }

    public class SamStyleItem : ICloneable
    {

        public SamStyleItem()
        {
            FiType = "00";
        }
        /// <summary>
        /// 科目编码
        /// </summary>   
        private string _cd;
        public string cd
        {
            get
            {
                if (_cd == null)
                {
                    return "";
                }
                return _cd;
            }
            set
            {
                _cd = value;
            }
        }

        /// <summary>
        /// 中英文名
        /// </summary>  
        private CNEN _name;
        public CNEN name
        {
            get
            {
                if (_name == null)
                    return new CNEN();
                else
                {
                    return _name;
                }
            }
            set { _name = value; }
        }


        /// <summary>
        /// 顺序
        /// </summary>    
        public int pos { get; set; }

        /// <summary>
        /// 是否有下一层
        /// </summary>     
        public string child { get; set; }

        /// <summary>
        /// 层级
        /// </summary>   
        public int level { get; set; }

        /// <summary>
        /// 校验公式
        /// </summary> 
        public string check { get; set; }

        /// <summary>
        /// 关闭跟踪
        /// </summary>    
        public string bt { get; set; }

        /// <summary>
        /// 显示情况（A股需要, 港股默认强制显示, 1：强制显示  2：非强制显示 （当选择的几个数据期该科目数据都为空，不显示)
        /// </summary>      
        public string disp { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>   
        /// 
        private CNEN _m;
        public CNEN m
        {
            get
            {
                if (_m == null)
                    return new CNEN();
                else
                {
                    return _m;
                }
            }
            set { _m = value; }
        }

        [BsonIgnore]
        public int Row { get; set; }

        [BsonIgnore]
        public string CombCode
        {
            get
            {
                return cd + ":" + Row + ":" + cat;
            }
        }

        [BsonIgnore]
        public int CheckRow { get; set; }

        [BsonIgnore]
        public string EN
        {
            get { return name.en; }
        }

        [BsonIgnore]
        public string nat { get; set; }

        [BsonIgnore]
        public string cat { get; set; }


        [BsonIgnore]
        public string CN
        {
            get
            {
                if (!string.IsNullOrEmpty(nat))
                {
                    string strNat = "-";
                    switch (nat)
                    {
                        case "1":
                            strNat += "总值";
                            break;
                        case "3":
                            strNat += "减值";
                            break;
                        case "5":
                            strNat += "净值";
                            break;
                        default:
                            strNat = "";
                            break;
                    }
                    return name.szh + strNat;
                }
                else
                {
                    return name.szh;
                }

            }
        }


        /// <summary>
        /// fi and type
        /// </summary>
        [BsonElement("fi")]
        public string FiType { get; set; }

        [BsonIgnore]
        public string fi
        {
            get
            {
                if (FiType.Length == 2)
                {
                    return FiType.ToCharArray()[0].ToString();
                }
                return "";
            }
            set
            {
                if (FiType.Length == 2)
                {
                    char[] charArray = FiType.ToCharArray();
                    charArray[0] = char.Parse(value);
                    FiType = new string(charArray);
                }
            }
        }

        /// <summary>
        /// type
        /// </summary>
        [BsonIgnore]
        public string typ
        {
            get
            {
                if (FiType.Length == 2)
                {
                    return FiType.ToCharArray()[1].ToString();
                }
                else if (FiType.StartsWith("m") && FiType.Length == 3)
                {
                    return FiType.Substring(2);
                }
                return "";
            }
            set
            {
                if (FiType.Length == 2)
                {
                    char[] charArray = FiType.ToCharArray();
                    charArray[1] = char.Parse(value);
                    FiType = new string(charArray);
                }
                else if (FiType.StartsWith("m") && FiType.Length == 3)
                {
                    string strFi = FiType.Substring(0, 2);
                    FiType = strFi + value;
                }
            }
        }

        /// <summary>
        /// 拼接起来的CODE 用于数据填充
        /// </summary>
        [BsonIgnore]
        public string CombinationCode
        {
            get
            {
                return fi + "|" + typ + "|" + cd;
            }

        }


        public Object Clone()
        {
            return (Object)this.MemberwiseClone();
        }
    }

    public class PosComparer : IComparer<SamStyleItem>
    {
        public int Compare(SamStyleItem x, SamStyleItem y)
        {
            if (x.pos > y.pos)
            {
                return 1;
            }
            else if (x.pos < y.pos)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }
    }


}
