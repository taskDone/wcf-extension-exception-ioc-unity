﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Sam
{
    public class IndexValuation
    {
        public const string Collection = "index_valuation";

        public ObjectId _id { get; set; }

        public string secu { get; set; }

        public string code { get; set; }

        public string y { get; set; }

        public string q { get; set; }


        /// <summary>
        /// upu
        /// </summary>
        public string upu { get; set; }

        /// <summary>
        /// upt
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? upt { get; set; }

        public int stat { get; set; }
    }
}
