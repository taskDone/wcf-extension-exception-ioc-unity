﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Sam
{
  public  class FinSamProduct
    {
        [BsonElement("_id")]
        public long Id { get; set; }

        /// <summary>
        /// 报表类型（is/bs/cf）
        /// </summary>
        [BsonElement("rpt")]
        public string ReportType { get; set; }

        /// <summary>
        /// 是否可用
        /// by alan 2012-9-12
        /// </summary>
        [BsonElement("active")]
        public int active { get; set; }

        /// <summary>
        /// ticker
        /// </summary>
        [BsonElement("tik")]
        public string Ticker { get; set; }

        /// <summary>
        /// 证券代码
        /// </summary>
        [BsonElement("secu")]
        public string SecurityCode { get; set; }

        /// <summary>
        /// 机构ID
        /// </summary>
        [BsonElement("org")]
        public string OrgnizationID { get; set; }

        /// <summary>
        /// 报告日期
        /// </summary>
        [BsonElement("y")]
        public string ReportYear { get; set; }

        /// <summary>
        /// 报告期间3,6,9,12 (1-12的整数) [取报告年度的月度值作为报告期间的值]
        /// </summary>
        [BsonElement("p")]
        public string Period { get; set; }

        /// <summary>
        /// 属于哪季度: Q1 -- Q4
        /// </summary>
        [BsonElement("q")]
        public string Quarter { get; set; }

        /// <summary>
        /// 合并类型（中英文）1：合并本期；2：合并调整，3：母公司本期，4：母公司调整
        /// </summary>
        [BsonElement("ctyp")]
        public int CombineType { get; set; }

        /// <summary>
        /// 报告来源
        /// </summary>
        [BsonElement("src")]
        public List<string> ReportResource { get; set; }

        /// <summary>
        /// accounting standard (中英文) : 1: IFRS 2: HKFRS 3: PRC GAAP 4: US GAAP 5: OLD PRC GAAP
        /// </summary>
        [BsonElement("std"),BsonIgnoreIfNull]
        public CDCNEN Standard { get; set; }

        /// <summary>
        /// 准则类型
        /// </summary>
        [BsonElement("stdtyp")]
        public int StandType { get; set; }

        /// <summary>
        /// 年度会计
        /// </summary>
        [BsonElement("fy")]
        public string FiscalYear { get; set; }

        /// <summary>
        /// 财年相关字段
        /// </summary>
        [BsonElement("fp")]
        public string FP { get; set; }

        /// <summary>
        /// report type (中英文) : 11: 通用， 21: 银行， 22: 证券， 23: 保险
        /// </summary>
        [BsonElement("typ")]
        public string Type { get; set; }

        /// <summary>
        /// 量级字典表
        /// </summary>
        [BsonElement("u")]
        public string DicTable { get; set; }

        /// <summary>
        /// (中英文 id) units="CNY" for A shares by default
        /// </summary>
        [BsonElement("c")]
        public CDCNEN Cny { get; set; }

        /// <summary>
        /// 录入人
        /// </summary>
        [BsonElement("upu")]
        public string UpdateUser { get; set; }

        /// <summary>
        /// 录入时间
        /// </summary>
        [BsonElement("crt")]
        public DateTime? RecordTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [BsonElement("upt"), BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? UpdateTime { get; set; }


        /// <summary>
        /// 状态
        /// </summary>
        [BsonIgnore]
        public String State { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [BsonIgnore]
        public String YearQuarter
        {
            get
            {
                return ReportYear + " " + Quarter;
            }
        }


        public const string Collection = "fin_sam_product";

        /// <summary>
        /// 备注
        /// </summary>
        [BsonElement("rem")]
        public CNEN Remark { get; set; }

        /// <summary>
        /// 会计期间
        /// </summary>
        [BsonElement("accp")]
        public string AccountPeriod { get; set; }

        [BsonElement("items")]
        public List<SamProductItem> Items { get; set; }

        [BsonElement("stat")]
        public int? Status { get; set; }
    }

  [Serializable]
  public class SamProductItem : ICloneable
  {
      public SamProductItem()
      {
          Code = string.Empty;
          FiType = "00";
          LstTrace = new List<TraceItem>();
          ResizeLstTrace = new List<TraceItem>();
          remark = new CNEN();
          Value = string.Empty;
          ValueU = string.Empty;
          ResizeValue = string.Empty;

      }

      /// <summary>
      /// 科目编码
      /// </summary>
      [BsonElement("cd")]
      public string Code { get; set; }

      /// <summary>
      /// 最新值
      /// </summary>
      [BsonElement("v")]
      public string Value { get; set; }

      /// <summary>
      /// 调整后最新值
      /// </summary>
      [BsonElement("rv")]
      public string ResizeValue { get; set; }

      /// <summary>
      /// 最新值美元汇率
      /// </summary>
      [BsonElement("vu")]
      public string ValueU { get; set; }

      /// <summary>
      /// 跟踪最新值
      /// </summary>
      [BsonElement("o")]
      public List<TraceItem> LstTrace { get; set; }

      /// <summary>
      /// 调整后跟踪最新值
      /// </summary>
      [BsonElement("ro")]
      public List<TraceItem> ResizeLstTrace { get; set; }

      /// <summary>
      /// 中英文备注
      /// </summary>
      [BsonElement("m")]
      public CNEN remark { get; set; }

      [BsonElement("fi")]
      public string FiType { get; set; }

      /// <summary>
      /// 大类
      /// </summary>
      [BsonIgnore]
      public string Fi
      {
          get
          {
              if (FiType.Length == 2)
              {
                  return FiType.ToCharArray()[0].ToString();
              }
              else if (FiType.StartsWith("m") && FiType.Length == 3)
              {
                  return FiType.Substring(0, 2);
              }
              return "";
          }
          set
          {
              if (FiType.Length == 2)
              {
                  char[] charArray = FiType.ToCharArray();
                  charArray[0] = char.Parse(value);
                  FiType = new string(charArray);
              }
              else if (FiType.StartsWith("m") && FiType.Length == 3)
              {
                  string strType = FiType.Substring(2);
                  FiType = value + strType;
              }
          }
      }

      /// <summary>
      /// 小类
      /// </summary>
      [BsonIgnore]
      public string Type
      {
          get
          {
              if (FiType.Length == 2)
              {
                  return FiType.ToCharArray()[1].ToString();
              }
              else if (FiType.StartsWith("m") && FiType.Length == 3)
              {
                  return FiType.Substring(2);
              }
              return "";
          }
          set
          {
              if (FiType.Length == 2)
              {
                  char[] charArray = FiType.ToCharArray();
                  charArray[1] = char.Parse(value);
                  FiType = new string(charArray);
              }
              else if (FiType.StartsWith("m") && FiType.Length == 3)
              {
                  string strFi = FiType.Substring(0, 2);
                  FiType = strFi + value;
              }
          }
      }





      /// <summary>
      /// 组合CODE
      /// </summary>
      [BsonIgnore]
      public string CombinationCode
      {
          get
          {
              return Fi + "|" + Type + "|" + Code;
          }
      }



      public object Clone()
      {
          return (object)this.MemberwiseClone();
      }
  }
}
