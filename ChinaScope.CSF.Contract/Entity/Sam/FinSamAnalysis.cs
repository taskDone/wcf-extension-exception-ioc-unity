﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Sam
{
    [Serializable]
    public class FinSamAnalysis
    {
        public static string Collection = "fin_sam_analysis";

        [BsonElement("_id")]
        public ObjectId Id { get; set; }

        public string code { get; set; }

        public string date { get; set; }

        public string mkt { get; set; }

        public int count { get; set; }

        public Dictionary<string, ChangeContent> chg { get; set; }
    }

    [Serializable]
    public class ChangeContent
    {
        public double v { get; set; }

        public Dictionary<string, double> secus { get; set; }
    }
}
