﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    /// <summary>
    /// 股东表
    /// </summary>
    public class BaseShareholder
    {
        public const string Collection = "base_shareholder";

        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        public ObjectId _id { get; set; }

        /// <summary>
        /// org
        /// </summary>
        [BsonElement("org")]
        public string org { get; set; }

        /// <summary>
        /// edt
        /// </summary>
        [BsonElement("edt")]
        public object edt { get; set; }

        /// <summary>
        /// 股东类型
        /// 1——十大股东
        /// 2——十大流通股东
        /// 3——其他股东
        /// </summary>
        [BsonElement("ttyp")]
        public List<string> ttyp { get; set; }

        /// <summary>
        /// hid
        /// </summary>
        [BsonElement("hid")]
        public string hid { get; set; }

        /// <summary>
        /// name
        /// </summary>
        [BsonElement("name")]
        public string name { get; set; }

        /// <summary>
        /// hldg
        /// </summary>
        [BsonElement("hldg")]
        //[BsonIgnoreIfNull]
        //long
        public long hldg { get; set; }

        /// <summary>
        /// chldg
        /// <summary>
        [BsonElement("chldg")]
        public long chldg
        {
            get;
            set;
        }

        /// <summary>
        /// ratio
        /// </summary>
        [BsonElement("ratio")]
        public string ratio { get; set; }

        /// <summary>
        /// cratio
        /// </summary>
        [BsonElement("cratio")]
        public string cratio { get; set; }

        /// <summary>
        /// src
        /// <summary>
        [BsonElement("src")]
        public string src
        {
            get;
            set;
        }

        /// <summary>
        /// sid
        /// <summary>
        [BsonElement("sid")]
        public string sid { get; set; }

        /// <summary>
        /// upu
        /// <summary>
        [BsonElement("upu")]
        public string upu { get; set; }

        /// <summary>
        /// upt
        /// <summary>
        [BsonElement("upt")]
        public object upt { get; set; }

        /// <summary>
        /// cru
        /// <summary>
        [BsonElement("cru")]
        public string cru { get; set; }

        /// <summary>
        /// crt
        /// <summary>
        [BsonElement("crt")]
        public object crt { get; set; }

        /// <summary>
        /// stat
        /// <summary>
        [BsonElement("stat")]
        public int? stat { get; set; }

        /// <summary>
        /// bid用于生成发布的唯一键guid
        /// <summary>
        [BsonElement("bid")]
        public string bid { get; set; }

        [BsonIgnore]
        public string HalfCName
        {
            get;
            set;
        }

        [BsonIgnore]
        public string HalfEName
        {
            get;
            set;
        }

        [BsonIgnore]
        public string IsPeople
        {
            get;
            set;
        }

        [BsonIgnore]
        public int tempid
        {
            get;
            set;
        }

        /// <summary>
        /// isdel(是否删除临时状态)
        /// </summary>
        [BsonIgnore]
        public int isdel { get; set; }
    }

    /// <summary>
    /// 股东表局部中间对象
    /// </summary>
    public class HidCnnOrgid
    {
        //股东表中的Hid
        public string Hid { get; set; }

        //股东表中的Orgid
        public string Orgid { get; set; }

        //股东表中的Hid
        public List<string> Ticker { get; set; }
    }
}
