﻿using System;
using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class UptTableTrack 
    {
        public static string Collection = "upt_table_track";

        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public string table { get; set; }

        [DataMember]
        public DateTime upt { get; set; }
    }



}
