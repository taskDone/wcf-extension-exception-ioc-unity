﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    public class BaseFund
    {
        public const string Collection = "base_fund";

        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        public string _id { get; set; }

        /// <summary>
        /// code
        /// </summary>
        [BsonElement("code")]
        public string code { get; set; }

        /// <summary>
        /// tick
        /// </summary>
        [BsonElement("tick")]
        public string tick { get; set; }

        /// <summary>
        /// name
        /// </summary>
        [BsonElement("name")]
        public CNEN name { get; set; }

        /// <summary>
        /// abbr
        /// </summary>
        [BsonElement("abbr")]
        public ABBR abbr { get; set; }

        /// <summary>
        /// org
        /// </summary>
        [BsonElement("org")]
        public IDCNEN org { get; set; }

        /// <summary>
        /// typ
        /// </summary>
        [BsonElement("typ")]
        public CDCNEN typ { get; set; }

        /// <summary>
        /// nature
        /// <summary>
        [BsonElement("nature")]
        public CDCNEN nature { get; set; }

        /// <summary>
        /// mkt
        /// </summary>
        [BsonElement("mkt")]
        public MKT mkt { get; set; }

        /// <summary>
        /// mgr
        /// <summary>
        [BsonElement("mgr")]
        public List<IDCNEN> mgr { get; set; }

        /// <summary>
        /// trustee
        /// <summary>
        [BsonElement("trustee")]
        public IDCNEN trustee { get; set; }

        /// <summary>
        /// found
        /// </summary>
        [BsonElement("found")]
        public object found { get; set; }

        /// <summary>
        /// contp
        /// </summary>
        [BsonElement("contp")]
        public long contp { get; set; }

        /// <summary>
        /// invtyp
        /// </summary>
        [BsonElement("invtyp")]
        public IDCNEN invtyp { get; set; }

        /// <summary>
        /// invobj
        /// </summary>
        [BsonElement("invobj")]
        public CNEN invobj { get; set; }

        /// <summary>
        /// invscop
        /// </summary>
        [BsonElement("invscop")]
        public CNEN invscop { get; set; }

        /// <summary>
        /// tf
        /// <summary>
        [BsonElement("tf")]
        public decimal tf { get; set; }

        /// <summary>
        /// mfr
        /// <summary>
        [BsonElement("mfr")]
        public decimal mfr { get; set; }

        /// <summary>
        /// feestd
        /// <summary>
        [BsonElement("feestd")]
        public CNEN feestd { get; set; }

        /// <summary>
        /// rem
        /// <summary>
        [BsonElement("rem")]
        public CNEN rem { get; set; }

        /// <summary>
        /// upu
        /// <summary>
        [BsonElement("upu")]
        public string upu { get; set; }

        /// <summary>
        /// tf
        /// <summary>
        [BsonElement("upt")]
        public object upt { get; set; }

        /// <summary>
        /// cru
        /// <summary>
        [BsonElement("cru")]
        public string cru { get; set; }

        /// <summary>
        /// crt
        /// <summary>
        [BsonElement("crt")]
        public object crt { get; set; }

        /// <summary>
        /// src
        /// <summary>
        [BsonElement("src")]
        public string src { get; set; }

        /// <summary>
        /// sid
        /// <summary>
        [BsonElement("sid")]
        public string sid { get; set; }
    }

    /// <summary>
    /// ABBR
    /// </summary>
    [Serializable]
    public class ABBR
    {
        /// <summary>
        /// py
        /// </summary>
        [BsonElement("py")]
        public string py { get; set; }

        /// <summary>
        /// en
        /// </summary>
        [BsonElement("en")]
        public string en { get; set; }

        /// <summary>
        /// szh
        /// </summary>
        [BsonElement("szh")]
        public string szh { get; set; }
    }

    /// <summary>
    /// MKT
    /// </summary>
    [Serializable]
    public class MKT
    {
        /// <summary>
        /// abbr
        /// </summary>
        [BsonElement("abbr")]
        public string abbr { get; set; }

        /// <summary>
        /// code
        /// </summary>
        [BsonElement("code")]
        public string code { get; set; }

        /// <summary>
        /// en
        /// </summary>
        [BsonElement("en")]
        public string en { get; set; }

        /// <summary>
        /// szh
        /// </summary>
        [BsonElement("szh")]
        public string szh { get; set; }
    }
}
