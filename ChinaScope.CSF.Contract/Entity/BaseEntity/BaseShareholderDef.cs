﻿using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    /// <summary>
    /// 股东定义表
    /// </summary>
    public class BaseShareholderDef
    {
        public const string Collection = "base_shareholder_def";

        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        public ObjectId _id { get; set; }

        /// <summary>
        /// hid
        /// </summary>
        [BsonElement("hid")]
        public string hid { get; set; }

        /// <summary>
        /// name
        /// </summary>
        [BsonElement("name")]
        public CNEN name { get; set; }

        /// <summary>
        /// typ
        /// </summary>
        [BsonElement("typ")]
        public CDCNEN typ { get; set; }

        /// <summary>
        /// baseid
        /// </summary>
        [BsonElement("baseid")]
        public string baseid { get; set; }

        /// <summary>
        /// cru
        /// </summary>
        [BsonElement("cru")]
        public string cru { get; set; }

        /// <summary>
        /// crt
        /// </summary>
        [BsonElement("crt")]
        public object crt { get; set; }

        /// <summary>
        /// upu
        /// </summary>
        [BsonElement("upu")]
        public string upu { get; set; }

        /// <summary>
        /// upt
        /// </summary>
        [BsonElement("upt")]
        public object upt { get; set; }

        /// <summary>
        /// stat
        /// </summary>
        [BsonElement("stat")]
        public int? stat { get; set; }

        /// <summary>
        /// stat
        /// </summary>
        [BsonElement("bid")]
        [BsonIgnoreIfNull]
        public string bid { get; set; }

        /// <summary>
        /// tempid
        /// </summary>
        [BsonIgnore]
        public int tempid { get; set; }

        public string sid { get; set; }

        public string src { get; set; }

        /// <summary>
        /// 半标准中文名
        /// </summary>
        [BsonIgnore]
        public string chname
        {
            //get; set;
            get
            {
                if (name != null)
                    return name.szh;
                return "";
            }
            set
            {
                if (name == null)
                    name = new CNEN();
                name.szh = value;
            }
        }

        /// <summary>
        /// 半标准英文名
        /// </summary>
        [BsonIgnore]
        public string ehname
        {
            //get; set;
            get
            {
                if (name != null)
                    return name.en;
                return "";
            }
            set
            {
                if (name == null)
                    name = new CNEN();
                name.en = value;
            }
        }

        /// <summary>
        /// 标准中文名
        /// </summary>
        [BsonIgnore]
        public string schname
        {
            get;
            set;
        }

        /// <summary>
        /// ticker(股东对应的股票代码，可多个)
        /// </summary>
        [BsonIgnore]
        public List<string> tickers { get; set; }
    }
}
