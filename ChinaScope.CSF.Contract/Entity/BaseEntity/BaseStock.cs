﻿using System;
using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class BaseStock 
    {
        public static string Collection = "base_stock";

        [DataMember]
        public string _id { get; set; }
         [DataMember]
        public string code { get; set; }
         [DataMember]
        public string tick { get; set; }
         [DataMember]
        public CNEN name { get; set; }
         [DataMember]
        public MongoAbbr abbr { get; set; }
         [DataMember]
        public MongoOrg org { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public Mongols typ { get; set; }
         [DataMember]
        public MongoMkt mkt { get; set; }

        [DataMember]
         public MongolsStr ls { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public double? ivol { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public long? share { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public string fv { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public Mongols fvcur { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public string dss { get; set; }
         [DataMember]
        public string rkd { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public string cik { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public CNEN rem { get; set; }
         [DataMember]
        public string upu { get; set; }
         [DataMember]
        public DateTime? upt { get; set; }
         [DataMember]
        public string cru { get; set; }

        private DateTime? ct;
         [DataMember]
        public DateTime? crt {
            get
            {
                if (ct == null)
                {
                    return new DateTime();
                }
                else
                {
                    return ct;
                }
            }
            set
            {
                ct = value;
            }
        }
         [DataMember]
        [BsonIgnoreIfNull]
        public string src { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public string sid { get; set; }
         [DataMember]
        public long? cov { get; set; }
         [DataMember]
        public bool adr { get; set; }
         [DataMember]
        public double? rat { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public string isin { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public string sedol { get; set; }
         [DataMember]
        [BsonIgnoreIfNull]
        public string rdk { get; set; }
         [DataMember]
        [BsonIgnore]
        public string CN
        {
            get
            {
                if (name != null)
                    return name.szh;
                return "";
            }
            set
            {
                if (name != null)
                    name.szh = value;
                else
                {
                    name = new CNEN() {szh = value};
                }
            }
        }
         [DataMember]
        [BsonIgnore]
        public string abbrCN
        {
            get
            {
                if (abbr != null)
                    return abbr.szh;
                return "";
            }
            set
            {
                if (abbr != null)
                    abbr.szh = value;
                else
                {
                    abbr = new MongoAbbr() {szh = value};
                }
            }
        }
         [DataMember]
        [BsonIgnore]
        public string orgCN
        {
            get
            {
                if (org != null)
                    return org.szh;
                return "";
            }
            set
            {
                //TODO:此处要根据中文名称获取机构信息（机构信息的编辑只有名称，故此处要得到一个org对象）
                if (org != null)
                    org.szh = value;
                else
                {
                    org = new MongoOrg() {szh = value};
                }
            }
        }
         [DataMember]
        [BsonIgnore]
        public string orgCD
        {
            get
            {
                if (org != null)
                    return org.Code;
                return "";
            }
            set
            {
                if (org != null)
                    org.Code = value;
                else
                {
                    org = new MongoOrg() {Code = value};
                }
            }
        }
         [DataMember]
        [BsonIgnore]
        public string orgEN
        {
            get
            {
                if (org != null)
                    return org.en;
                return "";
            }
            set
            {
                if (org != null)
                    org.en = value;
                else
                {
                    org = new MongoOrg() {en = value};
                }
            }
        }
         [DataMember]
        public int? stat { get; set; }

        //update 20131113-为了添加一个查询条件下的发布功能，必须需要此字段
        [BsonIgnoreIfNull]
        [DataMember]
        public string bid { get; set; }

        //是否两地上市update 20141110
        [DataMember]
        public bool dual { get; set; }

        //是否是沪港通update 20141110
        //10表示沪股通
        //20表示港股通
        [DataMember]
        public string shhk { get; set; }
    }


}
