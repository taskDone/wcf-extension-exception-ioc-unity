﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    public class SupplyChainDriver

    {
        public const string Collection = "supply_chain_driver";

        public ObjectId _id { get; set; }

        public string code { get; set; }

        public List<types> types { get; set; }

        public int? stat { get; set; }

        public string bid { get; set; }

    }

    public class types
    {
        public types()
        {
            ced = new List<long>();
        }

        public List<long> ced { get; set; }

        public int type { get; set; }

    }

}
