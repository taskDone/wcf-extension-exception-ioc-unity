﻿using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    public class BasePeople
    {
        public BasePeople()
        {
            bio = new CNEN();
            ce = new CNEN();
            profq = new List<CDCNEN>();
            rem = new CNEN();
            sex = new CNEN();
            til = new CNEN();
            role = new List<Role>();
            name = new CNEN();



            

        }


        public const string Collection = "base_people";

        public string _id { get; set; }

        public CNEN bio { get; set; }

        public string birth { get; set; }

        public CNEN ce { get; set; }

        public object crt { get; set; }

        public string cecd { get; set; }

        public long check { get; set; }

        public string email { get; set; }

        public string mobi { get; set; }

        public object movd { get; set; }

        public string nacd { get; set; }

        public List<CDCNEN> profq { get; set; }

        public CNEN rem { get; set; }

        public CNEN sex { get; set; }

        public string styr { get; set; }

        public string sid { get; set; }

        public string src { get; set; }

        public string tilcd { get; set; }

        public CNEN til { get; set; }

        public string upu { get; set; }

        public object upt { get; set; }

        public string cru { get; set; }

        public string valid { get; set; }

        public string ad { get; set; }

        public string tel { get; set; }

        public List<Role>  role { get; set; }

        public CNEN name { get; set; }

        public string orgid { get; set; }

        public  object stat { get; set; }

        [BsonIgnore]
        public string orgcn { get; set; }

        [BsonIgnore]
        public string CN
        {
            get
            {
                return name.szh;
            }
        }
    }

    public class Role
    {
        public Role()
        {
            nat = string.Empty;
            org = new List<string>();
        }
        public string nat { get; set; }

        public List<string>  org { get; set; }
    }
}
