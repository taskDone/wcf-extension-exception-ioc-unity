﻿using System;
using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class BasePortfolio
    {
        public const string Collection = "base_portfolio";

        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        [DataMember]
        public string _id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [BsonElement("name")]
        [DataMember]
        public CNEN name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [BsonElement("sid")]
        [DataMember]
        public string sid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [BsonElement("sup")]
        [DataMember]
        public CDCNEN sup { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [BsonElement("typ")]
        [DataMember]
        public string typ { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [BsonElement("src")]
        [DataMember]
        public string src { get; set; }

        /// <summary>
        /// crt
        /// </summary>
        [BsonElement("crt")]
        [DataMember]
        public DateTime? crt { get; set; }

        /// <summary>
        /// upt
        /// </summary>
        [BsonElement("upt")]
        [DataMember]
        public DateTime? upt { get; set; }

        /// <summary>
        /// upu
        /// </summary>
        [BsonElement("upu")]
        [DataMember]
        public string upu { get; set; }

        /// <summary>
        /// stat
        /// </summary>
        [BsonElement("stat")]
        [DataMember]
        public int? stat { get; set; }


        /// <summary>
        /// stat
        /// </summary>
        [BsonElement("bid")]
        [BsonIgnoreIfNull]
        [DataMember]
        public string bid { get; set; }
    }
}
