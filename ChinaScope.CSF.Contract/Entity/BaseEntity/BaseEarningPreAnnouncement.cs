﻿using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    public class BaseEarningsPreannouncement
    {
        public const string Collection = "base_earnings_preannouncement";

        public ObjectId _id { get; set; }

        public string rll { get; set; }

        /// <summary>
        /// 公告日期
        /// </summary>
        public string rpt { get; set; }

        public string rul { get; set; }

        public string cru { get; set; }

        public string vul { get; set; }

        public string content { get; set; }

        public string reason { get; set; }

        public string secu { get; set; }

        public string sid { get; set; }

        /// <summary>
        /// 报告日期
        /// </summary>
        public string y { get; set; }

        public string isnew { get; set; }

        public string upt { get; set; }

        public string upu { get; set; }

        public string typ { get; set; }

        public string crt { get; set; }

        public string vll { get; set; }

        public int? stat { get; set; }

        public string bid { get; set; }

        //最新更新时间
        public string lmt { get; set; }
    }
}
