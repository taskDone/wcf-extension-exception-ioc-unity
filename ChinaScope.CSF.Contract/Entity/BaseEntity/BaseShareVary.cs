﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    public class BaseShareVary
    {
       public const string Collection = "base_share_vary";

        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        public ObjectId _id { get; set; }

        /// <summary>
        /// org
        /// </summary>
        [BsonElement("org")]
        public IDCNEN org { get; set; }

        /// <summary>
        /// vdt
        /// </summary>
        [BsonElement("vdt")]
        public object vdt { get; set; }

        /// <summary>
        /// dcdt
        /// </summary>
        [BsonElement("dcdt")]
        public object dcdt { get; set; }

        /// <summary>
        /// rsn
        /// </summary>
        [BsonElement("rsn")]
        public CDCNEN rsn { get; set; }

        /// <summary>
        /// share
        /// </summary>
        [BsonElement("share")]
        public List<TypAmt> share { get; set; }

        /// <summary>
        /// total
        /// </summary>
        [BsonElement("total")]
        public string total { get; set; }

        /// <summary>
        /// lf
        /// </summary>
        [BsonElement("lf")]
        public string lf { get; set; }

        /// <summary>
        /// rem
        /// </summary>
        [BsonElement("rem")]
        public CNEN rem { get; set; }

        /// <summary>
        /// upu
        /// </summary>
        [BsonElement("upu")]
        public string upu { get; set; }

        /// <summary>
        /// upt
        /// </summary>
        [BsonElement("upt")]
        public object upt { get; set; }

        /// <summary>
        /// cru
        /// </summary>
        [BsonElement("cru")]
        public string cru { get; set; }

        /// <summary>
        /// crt
        /// </summary>
        [BsonElement("crt")]
        public object crt { get; set; }

        /// <summary>
        /// valid
        /// </summary>
        [BsonElement("valid")]
        public string valid { get; set; }

        /// <summary>
        /// src
        /// </summary>
        [BsonElement("src")]
        public string src { get; set; }

        /// <summary>
        /// sid
        /// </summary>
        [BsonElement("sid")]
        public string sid { get; set; }

        /// <summary>
        /// stat（是否删除，逻辑删除）
        /// </summary>
        [BsonElement("stat")]
        public int? stat { get; set; }

        /// <summary>
        /// tempid
        /// </summary>
        [BsonIgnore]
        public string tempid { get; set; }

        /// <summary>
        /// isdel
        /// </summary>
        [BsonIgnore]
        public int isdel { get; set; }

        /// <summary>
        /// bid
        /// </summary>
        public string bid { get; set; }

    }

   public class ShareVaryNameAndTotal
   {
       public string name { get; set; }

       public string total { get; set; }

       public DateTime date { get; set; }
   }

}
