﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    public class BaseInterMediary
    {
        public static string Collection = "base_intermediary";

        public string _id { get; set; }

        public IDCNEN org { get; set; }

        public TypIDCNEN interOrg { get; set; }

        //[BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? vadt { get; set; }

        [BsonIgnoreIfNull]
        public CNEN iorgs { get; set; }

        [BsonIgnoreIfNull]
        public CNEN note { get; set; }

        public string valid { get; set; }

        [BsonIgnoreIfNull]
        public string upu { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        [BsonIgnoreIfNull]
        public DateTime? upt { get; set; }

        [BsonIgnoreIfNull]
        public string cru { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        [BsonIgnoreIfNull]
        public DateTime? crt { get; set; }

        [BsonIgnoreIfNull]
        public int? stat { get; set; }

        [BsonIgnoreIfNull]
        public string bid { get; set; }
    }
}
