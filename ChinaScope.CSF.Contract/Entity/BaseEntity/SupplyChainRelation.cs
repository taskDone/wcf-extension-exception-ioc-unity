﻿using System;
using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class SupplyChainRelation 
    {
        public static string Collection = "supply_chain_relation";

        [DataMember]
        public ObjectId _id { get; set; }

        [DataMember]
        public CDCNEN up { get; set; }

        [DataMember]
        public CDCNEN down { get; set; }

        //值为1、2
        //表示产品类、行业类
        [DataMember]
        public int? type { get; set; }

        //值为0、1、2、3、4
        //表示行业类、原料对产品彼此都很重要、原料是产品的主要原料、产品时原料的主要应用、原料和产品无强烈的相互影响
        [DataMember]
        public int? degree { get; set; }

        //新增于2014-11-12
        //值为1、2、3
        //表示正向单向显示即A->B、反向单向显示即B->A、双向显示A<->B
        [DataMember]
        public int? display { get; set; }

        /// <summary>
        /// 添加一个是否有效的标识
        /// </summary>
        [DataMember]
        public int? valid { get; set; }

        /// <summary>
        ///发布状态
        /// </summary>
        [DataMember]
        [BsonIgnoreIfNull]
        public int? stat { get; set; }

        /// <summary>
        /// bid查询发布标识
        /// </summary>
        [DataMember]
        [BsonIgnoreIfNull]
        public string bid { get; set; }
    }



}
