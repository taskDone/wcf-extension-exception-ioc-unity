﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    /// <summary>
    /// 中间转换类
    /// SupplyChainDriver列表转换为单个对象
    /// </summary>
    public class SupplyChainDriverConvert : types
    {

        public ObjectId _id { get; set; }

        public string code { get; set; }

        public int? stat { get; set; }

        public string bid { get; set; } //发布标识

    }
}
