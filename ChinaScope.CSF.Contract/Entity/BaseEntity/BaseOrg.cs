﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.BaseEntity
{
    public class BaseOrg 
    {

        public BaseOrg()
        {
            name = new CNEN();
            abbr = new MongoAbbr();
            typ = string.Empty;
            corprep  = new MongoOrg();
            regaddr = new CDCNEN();
            lbln = string.Empty;
            oc = string.Empty;
            idp = string.Empty;
            capi = new Capi();
            fy = string.Empty;
            contact = new Contact();
            biz = new CNEN();
            scop = new CNEN();
            nature = new Nature();
            ind = new Ind();
            desc = new CNEN();
            note = new CNEN();
            upu = string.Empty;
            cru = string.Empty;
            valid = string.Empty;
            src = string.Empty;
            sid = string.Empty;
            reg = new CDCNEN();
            logo = string.Empty;
        }

        public static string Collection = "base_org";

        public string _id { get; set; }

        [BsonIgnoreIfNull]
        public string bid { get; set; }

        public CNEN name { get; set; }

        public MongoAbbr abbr { get; set; }

        public long? listed { get; set; }

        public string typ { get; set; }

        public MongoOrg corprep { get; set; }

        public CDCNEN regaddr { get; set; }

        public string lbln { get; set; }

        public string oc { get; set; }

        public string idp { get; set; }

        public Capi capi { get; set; }

        public long? noe { get; set; }

        public int? stat { get; set; }

        public string logo { get; set; }




   
        public string dt
        {
            get;
            set;
        }

        public string fy { get; set; }

        public Contact contact { get; set; }

        public CNEN biz { get; set; }

        public CNEN scop { get; set; }

        public Nature nature { get; set; }

        public Ind ind { get; set; }

        public CNEN desc { get; set; }

        public CNEN note { get; set; }

        public string upu { get; set; }

        public DateTime upt { get; set; }

        public string cru { get; set; }

        public DateTime crt { get; set; }

        public string valid { get; set; }

        public string src { get; set; }

        public string sid { get; set; }

        public long? active { get; set; }

        public long? cov { get; set; }

        public long? movd { get; set; }

        public CDCNEN reg { get; set; }
    }

    public class Nature
    {
        public string code { get; set; }
        public CNEN nm { get; set; }
    }

    public class Contact
    {
        public Contact()
        {
            addr = new CNEN();
            zip = string.Empty;
            url = string.Empty;
            mail = string.Empty;
            tel = string.Empty;
            fax = string.Empty;
        }
        public CNEN addr { get; set; }
        public string zip { get; set; }
        public string url { get; set; }
        public string mail { get; set; }
        public string tel { get; set; }
        public string fax { get; set; }
    }

    public class Capi
    {
        public Capi()
        {
            curtyp = string.Empty;
            cur = string.Empty;
        }
        public object amnt { get; set; }
        public string curtyp { get; set; }
        public string cur { get; set; }
    }

    public class Ind
    {
        public Ind()
        {
            csf = new List<Inds>();
            csrc = new List<Inds>();
            gics = new List<Inds>();
            icb = new List<Inds>();
            sywg = new List<Inds>();
            hs = new List<Inds>();
            gb = new List<Inds>();
        }
        public List<Inds> csf { get; set; }
        public List<Inds> csrc { get; set; }
        public List<Inds> gics { get; set; }
        public List<Inds> icb { get; set; }
        public List<Inds> sywg { get; set; }
        public List<Inds> hs { get; set; }
        public List<Inds> gb { get; set; }
    }

    public class Inds
    {
        public Inds()
        {
            code = new List<string>();
            en = new List<string>();
            szh = new List<string>();
        }
        public List<string> code { get; set; }
        public List<string> en { get; set; }
        public List<string> szh { get; set; }

        public bool pri { get; set; }

    }
}
