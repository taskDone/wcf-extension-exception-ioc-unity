﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChinaScope.CSF.Common;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.CorpNature
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class FinCorpNature
    {
        public const string Collection = "fin_corp_nature";

        /// <summary>
        /// id
        /// </summary>
        [DataMember]
        public long _id { get; set; }

        /// <summary>
        /// 标识是港股还是A股
        /// </summary>
        [DataMember]
        public string exchange { get; set; }

        /// <summary>
        /// 一级类型
        /// </summary>
        [DataMember]
        public string catlog { get; set; }

        /// <summary>
        /// 二级类型
        /// </summary>
        [DataMember]
        public string subcatlog { get; set; }

        /// <summary>
        /// 巨潮sid
        /// </summary>
        [DataMember]
        public string sid { get; set; }

        /// <summary>
        /// ticker
        /// </summary>
        [DataMember]
        public string tick { get; set; }

        /// <summary>
        /// 证券编码
        /// </summary>
        [DataMember]
        public string secu { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        [DataMember]
        public string orgname { get; set; }

        /// <summary>
        /// 机构id
        /// </summary>
        [DataMember]
        public string orgid { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        [DataMember]
        public string upu { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [DataMember]
        public DateTime? upt { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        [BsonIgnoreIfNull]
        [DataMember]
        public string src { get; set; }

        /// <summary>
        /// 发布状态
        /// </summary>
        [DataMember]
        public int? stat { get; set; }

        /// <summary>
        /// 发布标识字段
        /// </summary>
        [BsonIgnoreIfNull]
        [DataMember]
        public string bid { get; set; }
    }
}
