﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Announce
{
    public class Announcement
    {
        public const string Collection = "announcement";

        public ObjectId _id { get; set; }

        public string title { get; set; }

        public MongoFile file { get; set; }

        public string valid { get; set; }

        public object pdt { get; set; }

        public List<string> cat { get; set; }

        //自定义类型，updateby 20140819
        public string typ { get; set; }

        public List<MongoSecu> secu { get; set; }

        public string pid { get; set; }

        public string pubid { get; set; }

        public string upu { get; set; }

        public object upt { get; set; }

        public object cru { get; set; }

        public object crt { get; set; }

        public string src { get; set; }

        public string sid { get; set; }


        /// <summary>
        /// 正负面评价  0 负面 1 中性 2 正面
        /// </summary>
        public int? effect { get; set; }

        // public string status { get; set; }

        public int? stat { get; set; }  //用于标记删除   0 为删除 1为未发布 2 未审核 3 已发布 

        [BsonIgnore]
        public string filePath { get; set; }

        [BsonIgnore]
        public int row { get; set; }

        /// <summary>
        /// 是否人工对类型进行过编辑
        /// update by 2014-09-25
        /// </summary>
        [BsonIgnoreIfNull]
        public bool check { get; set; }

        public Announcement()
        {
            file = new MongoFile();
            pdt = new object();
            cat = new List<string>();
            secu = new List<MongoSecu>();
            upt = new object();
            pid = string.Empty;
            pubid = string.Empty;
            upu = string.Empty;
            src = string.Empty;
            sid = string.Empty;
            stat = 1;

        }
    }

    public class MongoFile
    {
        public string fn { get; set; }

        [BsonIgnore]
        public string oldfn { get; set; }

        public string ext { get; set; }

        public long? bytes { get; set; }

        public int? pn { get; set; }

        public string md5 { get; set; }

        public string url { get; set; }

        public string src { get; set; }

        [BsonIgnore]
        public string oldurl { get; set; }

        public MongoFile()
        {
            fn = string.Empty;
            ext = string.Empty;
            md5 = string.Empty;
            url = string.Empty;
            src = string.Empty;
        }
    }

    public class MongoCat
    {
        public string cd { get; set; }
        public string pcd { get; set; }
        public string szh { get; set; }
        public string en { get; set; }
        public string typ { get; set; }
        public object sdt { get; set; }
        public object edt { get; set; }
        public MongoCat()
        {
            sdt = new object();
        }
    }

    public class MongoSecu
    {
        public string cd { get; set; }
        public string mkt { get; set; }
        public string org { get; set; }

        [BsonIgnoreIfNull]
        public string typ { get; set; }
        [BsonIgnoreIfNull]
        public string abbr { get; set; }
        [BsonIgnoreIfNull]
        public string szh { get; set; }
        [BsonIgnoreIfNull]
        public string en { get; set; }
    }

    public enum EffectType
    {
        负面 = 0,
        中性 = 1,
        正面 = 2
    }



}
