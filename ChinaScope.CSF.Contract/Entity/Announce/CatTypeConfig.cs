﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;

namespace ChinaScope.CSF.Contract.Entity.Announce
{
    public static class CatTypeConfig
    {
        public static List<CDCNEN> lstCatType = new List<CDCNEN>();

        public static DataTable dtCatTypeA = null;

        public static DataTable dtCatTypeH = null;

        public static DataTable dtCatTypeUS = null;

        public static string CatTypeListA = string.Empty;
        public static string CatTypeListH = string.Empty;
        public static string CatTypeListUS = string.Empty;

        public const string AnnouncementA = "announcement";
        public const string AnnouncementH = "announcement_hk";
        public const string AnnouncementUS = "announcement_us";


        static CatTypeConfig()
        {
            lstCatType.Add(new CDCNEN() { en = "A股", szh = "年度报告", cd = "1030101" });
            lstCatType.Add(new CDCNEN() { en = "A股", szh = "半年度报告", cd = "1030301" });
            lstCatType.Add(new CDCNEN() { en = "A股", szh = "一季度报告", cd = "1030501" });
            lstCatType.Add(new CDCNEN() { en = "A股", szh = "三季度报告", cd = "1030701" });
            lstCatType.Add(new CDCNEN() { en = "A股", szh = "招股说明书", cd = "1020301" });

            lstCatType.Add(new CDCNEN() { en = "H股", szh = "年度报告", cd = "H10601" });
            lstCatType.Add(new CDCNEN() { en = "H股", szh = "半年度报告", cd = "H10602" });
            lstCatType.Add(new CDCNEN() { en = "H股", szh = "季度报告", cd = "H10603" });
            lstCatType.Add(new CDCNEN() { en = "H股", szh = "招股说明书", cd = "H10708" });

            lstCatType.Add(new CDCNEN() { en = "US股", szh = "年度报告", cd = "USANNUAL" });
            lstCatType.Add(new CDCNEN() { en = "US股", szh = "半年度报告", cd = "USINTERIM" });
            lstCatType.Add(new CDCNEN() { en = "US股", szh = "一季度报告", cd = "US1QUARTER" });
            lstCatType.Add(new CDCNEN() { en = "US股", szh = "三季度报告", cd = "US3QUARTER" });
            lstCatType.Add(new CDCNEN() { en = "US股", szh = "招股说明书", cd = "USPROSPECTUS" });

            dtCatTypeA = ToDataTable(lstCatType.FindAll(x => x.en == "A股"));
            dtCatTypeH = ToDataTable(lstCatType.FindAll(x => x.en == "H股"));
            dtCatTypeUS = ToDataTable(lstCatType.FindAll(x => x.en == "US股"));

            foreach (DataRow item in dtCatTypeA.Rows)
            {
                CatTypeListA += item["szh"].ToString() + ",";
            }

            foreach (DataRow item in dtCatTypeH.Rows)
            {
                CatTypeListH += item["szh"].ToString() + ",";
            }

            foreach (DataRow item in dtCatTypeUS.Rows)
            {
                CatTypeListUS += item["szh"].ToString() + ",";
            }

            CatTypeListA = CatTypeListA.Substring(0, CatTypeListA.Length - 1);
            CatTypeListH = CatTypeListH.Substring(0, CatTypeListH.Length - 1);
            CatTypeListUS = CatTypeListUS.Substring(0, CatTypeListUS.Length - 1);

        }

        /// <summary>
        /// 根据股票类型 和公告类型 返回Code  如果公告类型为空 则返回该股票类型下 所有Code 用,分割
        /// </summary>
        /// <param name="argStockType"></param>
        /// <param name="argCatType"></param>
        /// <returns></returns>
        public static string GetCode(string argStockType, string argCatType)
        {
            if (string.IsNullOrEmpty(argCatType))
            {
                List<CDCNEN> lstTemp = lstCatType.FindAll(x => x.en == argStockType);
                if (lstTemp.Count > 0)
                {
                    string strCode = string.Empty;
                    foreach (var item in lstTemp)
                    {
                        strCode += item.cd + ",";
                    }
                    return strCode.Substring(0, strCode.Length - 1);
                }
                else
                {
                    throw new Exception("该股票类型不存在,请重新输入!");
                }

            }
            else
            {
                CDCNEN temp = lstCatType.Find(x => x.szh == argCatType && x.en == argStockType);
                if (temp != null)
                {
                    return temp.cd;
                }
                else
                {
                    throw new Exception("该公告类型不存在,请重新输入!");
                }
            }

        }


        public static DataTable ToDataTable(IList list)
        {
            DataTable result = new DataTable();
            if (list.Count > 0)
            {
                PropertyInfo[] propertys = list[0].GetType().GetProperties();
                foreach (PropertyInfo pi in propertys)
                {
                    result.Columns.Add(pi.Name, pi.PropertyType);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ArrayList tempList = new ArrayList();
                    foreach (PropertyInfo pi in propertys)
                    {
                        object obj = pi.GetValue(list[i], null);
                        tempList.Add(obj);
                    }
                    object[] array = tempList.ToArray();
                    result.LoadDataRow(array, true);
                }
            }
            return result;
        }



    }
}
