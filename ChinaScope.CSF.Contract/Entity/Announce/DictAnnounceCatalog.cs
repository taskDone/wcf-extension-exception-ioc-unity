﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Announce
{
    public class DictAnnounceCatalog
    {
        public const string Collection = "dict_announce_catalog";

        public ObjectId _id { get; set; }

        public List<string> ancestors { get; set; }

        public string code { get; set; }

        public CNEN name { get; set; }

        public string parent { get; set; }

        public string cov { get; set; }

        [BsonElement("event")]
        public string eventproperty { get; set; }

        public string security { get; set; }

        public string market { get; set; }

        public object start { get; set; }

        public object stop { get; set; }

        public string valid { get; set; }

        public object crt { get; set; }

        public object upt { get; set; }

        public string rem { get; set; }

        public long order { get; set; }

        public string cattyp { get; set; }

        public string mkttyp { get; set; }
    }
}
