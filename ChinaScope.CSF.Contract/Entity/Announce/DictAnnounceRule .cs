﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Announce
{
    public class DictAnnounceRule
    {
        public const string Collection = "dict_announce_rule";

        public ObjectId _id { get; set; }

        public string rule { get; set; }

        public string code { get; set; }

        //发布
        public int? stat { get; set; }

        //updateby2014-09-25
        //规则本身也有不同的匹配方式，此时确定有两套验证规则的方式，暂时定为1,2（其中1为默认方式，即匹配最长的那个）
        public int type { get; set; }

        [BsonIgnore]
        public object upt { get; set; }

        [BsonIgnore]
        public string upu { get; set; }
    }
}
