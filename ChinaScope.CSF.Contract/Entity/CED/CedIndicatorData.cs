﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.CED
{
    public class CedIndicatorData
    {
        public const string Collection = "ced_indicator_data";

        public ObjectId _id { get; set; }

        [BsonElement("code")]
        public long code { get; set; }

        public Dictionary<string, Dictionary<string,string>> v { get; set; }

        [BsonIgnoreIfNull]
        public  Dictionary<string,Dictionary<string, CNEN>> rem { get; set; }

        [BsonIgnoreIfNull]
        public string bid { get; set; }

        [BsonIgnoreIfNull]
        public string upu { get; set; }

        [BsonIgnoreIfNull]
        public DateTime? upt { get; set; }

        [BsonIgnoreIfNull]
        public int? stat { get; set; }
    }
}
