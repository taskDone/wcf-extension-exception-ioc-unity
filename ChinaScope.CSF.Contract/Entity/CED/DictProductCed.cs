﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.CED
{
    public class DictProductCed
    {
        public const string Collection = "dict_product_ced";

        public long _id { get; set; }

        public bool active { get; set; }

        public CNEN name { get; set; }

        public Dictionary<string,long?> parent { get; set; }

        public List<string> rs { get; set; }

        public List<string> sam { get; set; }

        public string upu { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? upt { get; set; }

        public int? stat { get; set; }

        [BsonIgnoreIfNull]
        public string bid { get; set; }

        //新增两个行业,国标和数库 updateby2014-12-1
        //存储方式"csf":"csf_code";"gb":"gb_code"
        //数库行业来源于行业字典表dict_industry，国标行业来源于国标行业表dict_industry_gb
        //没有写成 "cd": ""en": "szh":的格式是因为用户只希望用code去同步显示，而非自己缓存的值，此值可能与code不同步
        public Dictionary<string,string> ind { get; set; }

    }

  
}
