﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.CED
{
   public  class DictFreq
   {
       public const string Collection = "dict_freq";

       public ObjectId _id { get; set; }

       public string code { get; set; }

       public string ename { get; set; }

       public string zhsname { get; set; }

       public string abbr { get; set; }
    }
}
