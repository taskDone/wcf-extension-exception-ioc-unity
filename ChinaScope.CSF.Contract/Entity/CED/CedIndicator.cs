﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.ServiceInterface;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.CED
{
    public class CedIndicator
    {
        public const string Collection = "ced_indicator";

        public CedIndicator()
        {
            rdt = string.Empty;
            c = string.Empty;
            name = new CNEN();
            prd = new List<string>();
            lstpd = new Lstpd();
            regs = new List<string>();
            vn = string.Empty;
            s = string.Empty;
            u = string.Empty;
            sid = string.Empty;
            ind = new Ind();
            frq = string.Empty;
            src = new List<string>();
            typ = string.Empty;
            reg = new List<string>();
            fstpd = string.Empty;
            bid = string.Empty;
            upu = string.Empty;
        }

        /// <summary>
        ///     Id
        /// </summary>
        public long _id { get; set; }

        public string rdt { get; set; }

        /// <summary>
        /// 指标名称
        /// </summary>
        public CNEN name { get; set; }

        public List<string> prd { get; set; }

        public Lstpd lstpd { get; set; }

        public string ytd { get; set; }

        public int? sts { get; set; }

        public List<string> regs { get; set; }

        public string vn { get; set; }

        public string rec { get; set; }

        public string s { get; set; }

        public string u { get; set; }

        public string c { get; set; }

        public string sid { get; set; }

        public Ind ind { get; set; }

        public string frq { get; set; }

        public List<string> src { get; set; }

        public string typ { get; set; }

        public List<string> reg { get; set; }

        public string fstpd { get; set; }

        public int? stat { get; set; }

        public string bid { get; set; }

        public string upu { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? upt { get; set; }

        //新增的一个排序字段
        public string order { get; set; }

        //新增一个标识，后台不需要维护(updateby20140825)默认为“0”和“1”两种状态
        public string dr { get; set; }

    }

    public class Ind
    {

        public Ind()
        {
            gb = new List<string>();
            csf = new List<string>();
        }
        public List<string> gb { get; set; }

        public List<string> csf { get; set; }
    }

    public class Lstpd
    {
        public Lstpd()
        {
            dt = string.Empty;
            v = string.Empty;
            chg = new ChgRate();
        }
        public string dt { get; set; }

        public ChgRate chg { get; set; }

        public string v { get; set; }
    }

    public class ChgRate
    {
        public ChgRate()
        {
            yoy = null;
            mom = null;
        }
        public double? yoy { get; set; }
        public double? mom { get; set; }
    }
}
