﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.ServiceInterface;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.CED
{
    /// <summary>
    /// 指标日历
    /// </summary>
    public class CedIndicatorCalendar
    {
        public const string Collection = "ced_indicator_calendar";
        
        /// <summary>
        ///     Id
        /// </summary>
        public ObjectId _id { get; set; }

        /// <summary>
        /// 指标代码
        /// </summary>
        public long code { get; set; }

        /// <summary>
        /// 指标名称(指中文名称)
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 频率（A/S/Q/M）
        /// </summary>
        public string frq { get; set; }

        public List<string> rdt { get; set; }

        public List<string> kw { get; set; }

      
    }
}
