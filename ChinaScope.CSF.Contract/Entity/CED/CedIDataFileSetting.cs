﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.CED
{
    public class CedIDataFileSetting
    {
        public const string Collection = "ced_indata_filesetting";

        public ObjectId _id { get; set; }

        //文件名称
        public string name { get; set; }

        //行标位置
        public int srow { get; set; }

        //关联代码列
        public string cdcol { get; set; }

        //首个数值位置列
        public string dcol { get; set; }

        [BsonIgnoreIfNull]
        public string upu { get; set; }

        [BsonIgnoreIfNull]
        public DateTime? upt { get; set; }
    }
}
