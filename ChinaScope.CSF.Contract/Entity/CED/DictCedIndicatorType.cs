﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.CED
{
    public class DictCedIndicatorType
    {
        public const string Collection = "dict_ced_indicator_type";

        public ObjectId _id { get; set; }

        public bool active { get; set; }

        public string code { get; set; }

        public CNEN name { get; set; }

        public CDCNEN cat { get; set; }

        public string upu { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? upt { get; set; }

        public int? stat { get; set; }

        [BsonIgnoreIfNull]
        public string bid { get; set; }

        [BsonIgnoreIfNull]
        public long order { get; set; }//需求变更于20140311,用于排序

    }
}
