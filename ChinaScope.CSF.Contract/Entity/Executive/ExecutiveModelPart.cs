﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.ServiceInterface;

namespace ChinaScope.CSF.Contract.Entity.Executive
{
    public class ExecutiveModelPart : IMapping
    {

        public ExecutiveModelPart()
        {
            szh = string.Empty;
            en = string.Empty;
            sex = string.Empty;
            gender = string.Empty;
            birth = string.Empty;
            pid = string.Empty;
            orgid = string.Empty;
            orgcn = string.Empty;
            serv = string.Empty;
            dept = string.Empty;
            jccode = string.Empty;
            jccn = string.Empty;
            jcen = string.Empty;
            jcserv = string.Empty;
            jcdept = string.Empty;




        }
        [ModelAlias(EnumType = typeof(PublishType))]
        [ExcelBinding(ColumnNo = 2),]
        public int? stat { get; set; }

        [ExcelBinding(ColumnNo = 3)]
        public string _id { get; set; }

        public string bid { get; set; }

        [ExcelBinding(ColumnNo = 4)]
        public string szh { get; set; }

        [ExcelBinding(ColumnNo = 5)]
        public string en { get; set; }

        [ExcelBinding(ColumnNo = 6)]
        public string sex { get; set; }

        [ExcelBinding(ColumnNo = 7)]
        public string gender { get; set; }

        [ExcelBinding(ColumnNo = 8)]
        public string birth { get; set; }

        [ExcelBinding(ColumnNo = 9)]
        public string pid { get; set; }

        [ExcelBinding(ColumnNo = 10)]
        public string orgid { get; set; }

        [ExcelBinding(ColumnNo = 11)]
        public string orgcn { get; set; }

        [ExcelBinding(ColumnNo = 12)]
        public string serv { get; set; }

        [ExcelBinding(ColumnNo = 13)]
        public string dept { get; set; }

        [ModelAlias(EnumType = typeof(TermType))]
        [ExcelBinding(ColumnNo = 14)]
        public int? act { get; set; }

        [ExcelBinding(ColumnNo = 15)]
        public string jccode { get; set; }

        [ExcelBinding(ColumnNo = 16)]
        public string jccn { get; set; }

        public string jcen { get; set; }

        [ExcelBinding(ColumnNo = 17)]
        public string jcserv { get; set; }

        [ExcelBinding(ColumnNo = 18)]
        public string jcdept { get; set; }

        [ModelAlias(EnumType = typeof(TermType))]
        [ExcelBinding(ColumnNo = 19)]
        public int? jcact { get; set; }

        public object upt { get; set; }

        public string upu { get; set; }
    }
}
