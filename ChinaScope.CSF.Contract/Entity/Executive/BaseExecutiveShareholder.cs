﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.ServiceInterface;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Executive
{
    public class BaseExecutiveShareholder : IMapping
    {
        public BaseExecutiveShareholder()
        {
            name = new CNEN();
            paid = new CurrAmt();
            alw = new CurrAmt();
            rem = new CNEN();
            htyp = string.Empty;
            aftax = string.Empty;
            recpay = string.Empty;
            secu = string.Empty;
            pid = string.Empty;
            orgid = string.Empty;
            cru = string.Empty;
            src = string.Empty;
            sid = string.Empty;




        }
        public const string Collection = "base_executive_shareholder";

        //[ModelAlias(EnumType = typeof(PublishType))]
        [ExcelBinding(ColumnNo = 3)]
        public string _id { get; set; }

        [ExcelBinding(ColumnNo = 7)]
        public string secu { get; set; }

        [ExcelBinding(ColumnNo = 10)]
        public object rptdt { get; set; }

        public CNEN name { get; set; }

        public string bid { get; set; }

        [ModelAlias(EnumType = typeof(PublishType))]
        [ExcelBinding(ColumnNo = 2),]
        public int? stat { get; set; }

        [BsonIgnore]
        [ExcelBinding(ColumnNo = 5)]
        public string CN
        {
            get
            {
                return name.szh;
            }

            set
            {
                name.szh = value;
            }
        }


        [BsonIgnore]
        [ExcelBinding(ColumnNo = 6)]
        public string EN
        {
            get
            {
                return name.en;
            }

            set
            {
                name.en = value;
            }
        }

        [ModelAlias(EnumType = typeof(TermType))]
        [ExcelBinding(ColumnNo = 15)]
        public string aftax { get; set; }

        [ModelAlias(EnumType = typeof(HType))]
        [ExcelBinding(ColumnNo = 12)]
        public string htyp { get; set; }

        [ModelAlias(EnumType = typeof(TermType))]
        [ExcelBinding(ColumnNo = 16)]
        public string recpay { get; set; }

        public CurrAmt paid { get; set; }

        [BsonIgnore]
        [ExcelBinding(ColumnNo = 13)]
        public string PaidAmt
        {
            get
            {
                return paid.amt;
            }
            set
            {
                paid.amt = value;
            }
        }

        [BsonIgnore]
        [ExcelBinding(ColumnNo = 14)]
        public string PaidCurr
        {
            get
            {
                return paid.curr;
            }
            set
            {
                paid.curr = value;
            }
        }



        public CurrAmt alw { get; set; }

        [ExcelBinding(ColumnNo = 11)]
        public string shares { get; set; }

        public CNEN rem { get; set; }
        [BsonIgnore]
        [ExcelBinding(ColumnNo = 8)]
        public string orgcn { get; set; }
        [ExcelBinding(ColumnNo = 9)]
        public string orgid { get; set; }



        [ExcelBinding(ColumnNo = 4)]
        public string pid { get; set; }

        public string upu { get; set; }

        public object upt { get; set; }

        public string cru { get; set; }

        public object crt { get; set; }

        public string src { get; set; }

        public string sid { get; set; }
    }
}
