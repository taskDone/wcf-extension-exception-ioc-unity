﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;

namespace ChinaScope.CSF.Contract.Entity.Executive
{
    public class ExecutiveModel
    {
        public ExecutiveModel()
        {
            jc = new List<Job>();
            name = new CNEN();
            rem = new CNEN();
            sex = new CNEN();

        }
        public const string Collection = "base_executive";

        public string _id { get; set; }

        public string bid { get; set; }

        public int? act { get; set; }

        public string birth { get; set; }

        public object crt { get; set; }

        public string cru { get; set; }

        public string dept { get; set; }

        public List<Job> jc { get; set; }

        public CNEN name { get; set; }

        public string orgid { get; set; }

        public string pid { get; set; }

        public CNEN rem { get; set; }

        public string serv { get; set; }

        public CNEN sex { get; set; }

        public string sid { get; set; }

        public string src { get; set; }

        public int? stat { get; set; }

        public object upt { get; set; }

        public string upu { get; set; }
    }


    public class Job : CNEN
    {
        public Job()
        {
            code = "";
            serv = "";
            dept = "";
        }
        public string code { get; set; }

        public string serv { get; set; }

        public string dept { get; set; }

        public long? level { get; set; }

        public int? act { get; set; }
    }
}
