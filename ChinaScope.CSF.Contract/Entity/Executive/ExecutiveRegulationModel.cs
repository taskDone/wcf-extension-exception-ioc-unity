﻿using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Executive
{
    public class ExecutiveRegulationModel
    {
        public ExecutiveRegulationModel()
        {
            name = new CNEN();
            scp = new CNEN();
            isdelete = "否";
        }

        public const string Collection = "base_executive_regulation";

        public ObjectId _id { get; set; }

        public string secu { get; set; }

        public string bid { get; set; }

        [BsonIgnore]
        public string secuname { get; set; }

        public string org { get; set; }

        public CNEN scp { get; set; }

        [BsonIgnore]
        public string scpen
        {
            get { return scp.en; }
        }

        [BsonIgnore]
        public string scpcn
        {
            get { return scp.szh; }
        }

        public string relation { get; set; }

        public CNEN name { get; set; }

        [BsonIgnore]
        public string en
        {
            get { return name.en; }
        }

        [BsonIgnore]
        public string cn
        {
            get { return name.szh; }
        }

        public string pid { get; set; }

        public string change { get; set; }

        public string after { get; set; }

        public string price { get; set; }

        public string cur { get; set; }

        public string cause { get; set; }

        public string cd { get; set; }

        public string rd { get; set; }

        public int stat { get; set; }

        public object upt { get; set; }

        public string upu { get; set; }

        [BsonIgnore]
        public string isdelete { get; set; }

        private string _tempid = "";
        [BsonIgnore]
        public string tempid
        {
            get
            {
                int i_id = 0;

                int.TryParse(_tempid.ToString(), out i_id);

                if (i_id < 0)
                {
                    return _tempid;
                }
                else
                {
                    _tempid = string.Format("{0}", _id.ToString());
                    return _tempid;
                }

            }
            set { _tempid = value; }
        }

        [BsonIgnore]
        public string statstr
        {
            get
            {
                if (stat == 3)
                {
                    return "已发布";
                }
                else
                {
                    return "未发布";
                }
            }
        }

        //占总股本比例 updateby2014-11-13
        public string torat { get; set; }

        //占流通股本比例 updateby2014-11-13
        public string cirrat { get; set; }
    }

}
