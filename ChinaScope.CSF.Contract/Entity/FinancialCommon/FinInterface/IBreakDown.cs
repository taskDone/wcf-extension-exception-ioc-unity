﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.FinInterface
{
    public interface IBreakDown
    {
        List<EnterBlock> Blocks { get; set; }
    }

    [Serializable]
    public class EnterBlock
    {
        public string Name { get; set; }
        public int DefaultLevel { get; set; }
        public List<Area> Areas { get; set; }
        public int Classify { get; set; }//定义按**分类

        public int DefaultArea { get; set; }

        public List<ListPosNature> List { get; set; }

        public string ContributionCode { get; set; }

        public string StartCode { get; set; }

        public string EndCode { get; set; }

        public string EndName
        {
            get
            {
                if (Areas.Count > 0)
                {
                    return Areas[Areas.Count - 1].EndSign.Name;
                }
                return "";
            }
        }

        public EnterBlock Clone()
        {
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, this);
            ms.Seek(0, SeekOrigin.Begin);

            return (EnterBlock)bf.Deserialize(ms);
        }
    }

    [Serializable]
    public class Area
    {
        public int Level { get; set; }
        public string Name { get; set; }
        public Coordinate StartSign { get; set; }
        public Coordinate EndSign { get; set; }
        public string StartCol { get; set; }
        public string EndCol { get; set; }
        public int Nature { get; set; }

        public string[] cols = new[]{"J","K"};
        /// <summary>
        /// 读取和填充那几列值
        /// </summary>
        public string[] Cols
        {
            get { return cols; }
            set { cols = value; }
        }

    }

    [Serializable]
    public class Coordinate
    {
        public string Name { get; set; }
        public int Row { get; set; }
    }

    //public class ValueNature
    //{
    //    public string Value { get; set; }
    //    public int Nature { get; set; }
    //    public string Disp { get; set; }
    //}

    [Serializable]
    public class ListPosNature
    {
        public int Pos { get; set; }
        public int Nature { get; set; }
    }

    /// <summary>
    /// 用于占比分析
    /// </summary>
    public class RatioAnalysisBlockCnnItems : EnterBlock
    {
        public string blockcode { get; set; }

        public List<NormStyleItem> items { get; set; }

        public int RatioStartRow { set; get; }

        public int pos { get; set; }//用于block的排序，主要是在删除添加行值的时候重新计算下一个block的值，但是block本身是无序的，这里添加一个pos使之有序
    }
}
