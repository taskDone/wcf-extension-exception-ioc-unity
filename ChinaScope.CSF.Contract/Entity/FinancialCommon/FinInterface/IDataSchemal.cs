﻿using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.FinInterface
{
    public interface IDataSchemal<T>
    {
        long Id { get; set; }

        string ReportYear { get; set; }

        List<string> ReportResource { get; set; }

        bool active { get; set; }

        int Period { get; set; }

        string Quarter { get; set; }

        string FiscalYear { get; set; }

        MonStandard Cny { get; set; }

        MonScale DicTable { get; set; }

        MonStandard Standard { get; set; }

        List<T> Items { get; set; }
    }
}
