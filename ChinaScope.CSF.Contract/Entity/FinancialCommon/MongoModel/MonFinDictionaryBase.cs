﻿using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 字典类基类（Financial Dictionary）
    /// </summary>
    public class MonFinDictionaryBase
    {
        /// <summary>
        /// ID
        /// </summary>
        [BsonElement("_id")]
        public int Id { get; set; }

        /// <summary>
        /// 编码类型
        /// </summary>
        [BsonElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 英文名称
        /// </summary>
        [BsonElement("ename")]
        public string Ename { get; set; }

        /// <summary>
        /// 中文名称
        /// </summary>
        [BsonElement("zhsname")]
        public string Zhsname { get; set; }
    }
}
