﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 账号标准（accounting standard）
    /// </summary>
    [Serializable]
    public class MonStandard
    {
        [BsonElement("cd")]
        public string Cd { get; set; }

        /// <summary>
        /// 英文的账号标准
        /// </summary>
        [BsonElement("en")]
        public string EngStandard { get; set; }

        /// <summary>
        /// 中文的账号标准
        /// </summary>
        [BsonElement("szh")]
        public string ChinStandard { get; set; }
    }
}
