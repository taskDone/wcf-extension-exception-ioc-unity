﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.FinInterface;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    [Serializable]
    //[DataContract(Namespace = Constants.DataContractNamespace)]
    public class MonFinRptAsrepExt : MonFinRptBase, IDataSchemal<AsrepExtItem>
    {
        public const string Collection = "fin_rpt_asrep_ext_ytd";

        /// <summary>
        /// 备注
        /// </summary>
        [BsonElement("rem")]
        public MonRemark Remark { get; set; }

        /// <summary>
        /// 会计期间
        /// </summary>
        [BsonElement("accp")]
        public string AccountPeriod { get; set; }

        [BsonElement("items")]
        public List<AsrepExtItem> Items { get; set; }
    }

    [Serializable]
    //[DataContract(Namespace = Constants.DataContractNamespace)]
    public class AsrepExtItem
    {
        /// <summary>
        /// 临时增加
        /// </summary>
        [BsonElement("pos"), BsonIgnoreIfNull]
        public object Pos { get; set; }

        /// <summary>
        /// 临时增加
        /// </summary>
        [BsonElement("calc"), BsonIgnoreIfNull]
        public object Calc { get; set; }

        /// <summary>
        /// 临时增加
        /// </summary>
        [BsonElement("level"), BsonIgnoreIfNull]
        public object Level { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        [BsonElement("cd")]
        public string Code { get; set; }

        /// <summary>
        /// 原始值
        /// </summary>
        [BsonElement("ov")]
        public string OriginalValue { get; set; }

        /// <summary>
        /// 原始值美元汇率
        /// </summary>
        [BsonElement("ovu")]
        public double? OriginalValueU { get; set; }

        
        /// <summary>
        /// 最新值
        /// </summary>
        [BsonElement("lv")]
        public string LastestValue { get; set; }
        /// <summary>
        /// 最新值美元汇率
        /// </summary>
        [BsonElement("lvu")]
        public double? LastestValueU { get; set; }

        /// <summary>
        /// 跟踪的原始值
        /// </summary>
        [BsonElement("o"), BsonIgnoreIfNull]
        public List<TraceItem> OriTrace { get; set; }

        /// <summary>
        /// 跟踪最新值
        /// </summary>
        [BsonElement("l"), BsonIgnoreIfNull]
        public List<TraceItem> LstTrace { get; set; }

        /// <summary>
        /// 跟踪src的页码
        /// </summary>
        [BsonElement("t"), BsonIgnoreIfNull]
        public List<string> Trace { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>
        [BsonElement("m"), BsonIgnoreIfNull]
        public MonRemark remark { get; set; }


        [BsonElement("ori"),BsonIgnoreIfNull]
        public string ori
        {
            get; set;
        }

    }

}
