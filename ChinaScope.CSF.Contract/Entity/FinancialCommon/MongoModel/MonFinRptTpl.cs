﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.FinInterface;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// templated
    /// </summary>
    [Serializable]
    public class MonFinRptTpl : MonFinRptBase, IDataSchemal<TplItem>
    {
        public const string Collection = "fin_rpt_tpl_ytd";//TODO:

        public MonFinRptTpl()
        {
            Remark = new MonRemark();
            Items = new List<TplItem>();
        }
        /// <summary>
        /// 备注
        /// </summary>
        [BsonElement("rem")]
        public MonRemark Remark { get; set; }

        [BsonElement("accp")]
        public string AccountPeriod { get; set; }

        [BsonElement("items")]
        public List<TplItem> Items { get; set; }
    }

    [Serializable]
    public class TplItem
    {
        //todo:其中pos和level是导数据的时候用到的，和这里没关系，为了兼容数据

        /// <summary>
        /// 科目编码
        /// </summary>
        [BsonIgnoreIfNull]
        public object pos { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        [BsonIgnoreIfNull]
        public object level { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        [BsonElement("cd")]
        public string Code { get; set; }

        /// <summary>
        /// 本期值
        /// </summary>
        [BsonElement("ov")]
        public string OriginalValue { get; set; }

        /// <summary>
        /// 原始值说明
        /// </summary>
        [BsonElement("ovu")]
        public double? OriginalValueU { get; set; }

        /// <summary>
        /// 最新值
        /// </summary>
        [BsonElement("lv")]
        public string LastestValue { get; set; }

        /// <summary>
        /// 最新值美元汇率转换
        /// </summary>
        [BsonElement("lvu")]
        public double? LastestValueU { get; set; }

        /// <summary>
        /// 运算关系
        /// </summary>
        [BsonElement("calc"), BsonIgnoreIfNull]
        public string CalOperator { get; set; }

        /// <summary>
        /// trace
        /// </summary>
        [BsonElement("t"), BsonIgnoreIfNull]
        public List<string> Trace { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>
        [BsonElement("m"), BsonIgnoreIfNull]
        public MonRemark remark { get; set; }

        /// <summary>
        /// 跟踪的原始值
        /// </summary>
        [BsonElement("o"), BsonIgnoreIfNull]
        public List<TraceItem> OriTrace { get; set; }

        /// <summary>
        /// 跟踪最新值
        /// </summary>
        [BsonElement("l"), BsonIgnoreIfNull]
        public List<TraceItem> LstTrace { get; set; }

        [BsonIgnoreIfNull]
        public string ori { get; set; }

    }

}
