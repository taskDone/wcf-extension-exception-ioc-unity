﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 衍生表
    /// </summary>
    public class MonFinRptDerived:MonFinRptBase
    {
        /// <summary>
        /// 备注
        /// </summary>
        [BsonElement("rem")]
        public string Remark { get; set; }

        [BsonElement("items")]
        public List<DerivedItem> Items { get; set; }
    }

    public class DerivedItem
    {
        /// <summary>
        /// 项目编码
        /// </summary>
        [BsonElement("cd")]
        public string Code { get; set; }

        /// <summary>
        /// 值（数字/NA/NM）
        /// </summary>
        [BsonElement("v")]
        public string Value { get; set; }

        /// <summary>
        /// 值的美元汇率转换
        /// </summary>
        [BsonElement("vu")]
        public string ValueU { get; set; }

        /// <summary>
        /// 运算关系
        /// </summary>
        [BsonElement("calc")]
        public MonRemark CalOperator { get; set; }      

        /// <summary>
        /// 跟踪
        /// </summary>
        [BsonElement("t")]
        public List<string> Trace { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>
        [BsonElement("m")]
        public MonRemark remark { get; set; }

    }
}
