﻿using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    public class MonTrans
    {
        [BsonElement("asrep")]
        public Monasrep asrep { get; set; }

        [BsonElement("tpl")]
        public Montpl tpl { get; set; }

        [BsonElement("analysis1")]
        public Montpl analysis1 { get; set; }

        [BsonElement("analysis2")]
        public Montpl analysis2 { get; set; }
    }

    public class Monasrep
    {
        [BsonElement("gen")]
        public string gen { get; set; }

        [BsonElement("fin")]
        public string fin { get; set; } 
    }

    public class Montpl 
    {
        [BsonElement("gen")]
        public string gen { get; set; }

        [BsonElement("bank")]
        public string fin { get; set; } 

        [BsonElement("ins")]
        public string ins { get; set; }

        [BsonElement("sec")]
        public string sec { get; set; } 
    }

}