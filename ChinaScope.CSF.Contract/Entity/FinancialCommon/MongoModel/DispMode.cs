﻿using System.Collections.Generic;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
   public class DispMode
    {
      public List<NameValue> Disp { get; set; }
    }
}
