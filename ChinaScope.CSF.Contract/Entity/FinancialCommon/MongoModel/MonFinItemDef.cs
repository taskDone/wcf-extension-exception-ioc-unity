﻿using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    ///  一张报表里名字唯一
    /// </summary>
    public class MonFinItemDef
    {
        [BsonElement("_id")]
        public int Id { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        [BsonElement("code")]
        public string ItemCode { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [BsonElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// 报表代码
        /// </summary>
        [BsonElement("rpt")]
        public string Rpt { get; set; }
    }
}
