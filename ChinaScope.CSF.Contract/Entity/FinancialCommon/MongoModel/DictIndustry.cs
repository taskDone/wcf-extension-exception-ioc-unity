﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    public class DictIndustry
    {
        public const string Collection = "dict_industry";

        public ObjectId _id { get; set; }

        public string code { get; set; }

        public string ename { get; set; }

        public string zhsname { get; set; }

        public List<string> ancestors { get; set; }

        public string parent { get; set; }

        public string publisher { get; set; }

        public int level { get; set; }

        public string cn_def { get; set; }

        public string en_def { get; set; }

        public string index { get; set; }

        public string comment { get; set; }

        public string start_dt { get; set; }

        public string end_dt { get; set; }

        public string gics_code { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? upd_stmp { get; set; }
    }
}
