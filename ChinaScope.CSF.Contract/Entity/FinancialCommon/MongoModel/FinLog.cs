﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    public class FinLog
    {
        public const string Collection = "ops_fin_rpt_log";

        [BsonElement("_id")]
        public ObjectId _id { get; set; }

        public string secu { get; set; }
        public string rpt { get; set; }
        public string y { get; set; }
        public string fpq { get; set; }
        public int type { get; set; }
        public string upu { get; set; }
        public DateTime? upt { get; set; }
    }
}
