﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 中英文备注表
    /// </summary>
    [Serializable]
    public class MonRemark
    {
        public MonRemark()
        {
            en = "";
            szh = "";
        }
        /// <summary>
        /// 英文备注
        /// </summary>
        [BsonElement("en")]
        public string en { get; set; }

        /// <summary>
        /// 中文备注
        /// </summary>
        [BsonElement("szh")]
        public string szh { get; set; }
    }

    /// <summary>
    /// 录入科目跟踪的原始值和最新值
    /// </summary>
    [Serializable]
    public class TraceItem
    {

        public TraceItem()
        {
            Code = "";
            Value = "";
        }
        /// <summary>
        /// 关联code
        /// </summary>
        [BsonElement("cd")]
        public string Code { get; set; }

        /// <summary>
        /// 关联值
        /// </summary>
        [BsonElement("v")]
        public string Value { get; set; }
    }
}
