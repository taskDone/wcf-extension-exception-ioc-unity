using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    public class FinRptAudit
    {
        public static string Collection = "fin_rpt_audit";

        public ObjectId _id { get; set; }

        public string secu { get; set; }
        public string tick { get; set; }
        public string org { get; set; }
        public string fp { get; set; }
        public string y { get; set; }
        public string fy { get; set; }
        public CDCNEN audit { get; set; }

        /// <summary>
        /// ����״̬
        /// </summary>
        public int stat { get; set; }
    }
}