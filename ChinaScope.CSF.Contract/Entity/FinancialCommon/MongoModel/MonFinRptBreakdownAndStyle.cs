﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    [Serializable]
    public class MonFinRptBreakdownAndStyle
    {
        public NormFinRptStyle FinRptStyle { get; set; }

        public List<MonFinRptBreakdown> lstItems { get; set; }
    }
}
