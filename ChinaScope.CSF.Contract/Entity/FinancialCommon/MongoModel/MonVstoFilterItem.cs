﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    public class MonVstoFilterItem
    {
        public const string Collection = "vsto_filter_item";

        [BsonElement("_id")]
        public ObjectId _id { get; set; }

        [BsonElement("code")]
        public string Code { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("nat")]
        public string Nature { get; set; }

        [BsonElement("classify")]
        public int Classify { get; set; }

        [BsonElement("rpt")]
        public string Rpt { get; set; }

        [BsonElement("tik")]
        public string Tiker { get; set; }

        [BsonIgnore]
        public bool IsNew { get; set; }
    }
}
