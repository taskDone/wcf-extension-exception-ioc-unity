﻿using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 财务科目转换表
    /// </summary>
    public class MonFinItemCalc
    {
        public const string Collection = "fin_item_calc";

        [BsonElement("_id")]
        public long id { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        [BsonElement("code")]
        public string code { get; set; }

        /// <summary>
        /// cat
        /// </summary>
        [BsonElement("cat")]
        public string cat { get; set; }

        /// <summary>
        /// rpt
        /// </summary>
        [BsonElement("rpt")]
        public string rpt { get; set; }


        /// <summary>
        /// 转换关系
        /// </summary>
        [BsonElement("trans")]
        public MonTrans Trans { get; set; }
    }
}
