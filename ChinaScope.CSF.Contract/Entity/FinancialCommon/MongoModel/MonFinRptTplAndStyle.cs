﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    [Serializable]
    public class MonFinRptTplAndStyle
    {
        public NormFinRptStyle FinRptStyle { get; set; }

        public List<MonFinRptTpl> lstItems { get; set; }
    }
}
