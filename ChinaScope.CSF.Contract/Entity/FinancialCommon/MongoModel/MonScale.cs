﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    [Serializable]
    public class MonScale
    {        
        [BsonElement("cd")]
        public string Code { get; set; }

        /// <summary>
        /// 英文的账号标准
        /// </summary>   
        [BsonElement("en")]
        public string EngName { get; set; }

        /// <summary>
        /// 中文的账号标准
        /// </summary>  
        [BsonElement("szh")]
        public string ChinName { get; set; }

        /// <summary>
        /// scale
        /// </summary> 
        [BsonElement("scal")]
        public double Scale { get; set; }


    }
}
