﻿using System;
using System.Runtime.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    public class BaseStock
    {
        public static string Collection = "base_stock";

        public string _id { get; set; }

        public string code { get; set; }

        public string tick { get; set; }

        public CNEN name { get; set; }

        public MongoAbbr abbr { get; set; }

        public MongoOrg org { get; set; }

        [BsonIgnoreIfNull]
        public Mongols typ { get; set; }

        public MongoMkt mkt { get; set; }

        public Mongols ls { get; set; }

        [BsonIgnoreIfNull]
        public double? ivol { get; set; }

        [BsonIgnoreIfNull]
        public long? share { get; set; }

        [BsonIgnoreIfNull]
        public string fv { get; set; }

        [BsonIgnoreIfNull]
        public Mongols fvcur { get; set; }

        [BsonIgnoreIfNull]
        public string dss { get; set; }

        public string rkd { get; set; }

        [BsonIgnoreIfNull]
        public string cik { get; set; }

        [BsonIgnoreIfNull]
        public CNEN rem { get; set; }

        public string upu { get; set; }

        public DateTime? upt { get; set; }

        public string cru { get; set; }

        private DateTime? ct;
        public DateTime? crt
        {
            get
            {
                if (ct == null)
                {
                    return new DateTime();
                }
                else
                {
                    return ct;
                }
            }
            set
            {
                ct = value;
            }
        }

        [BsonIgnoreIfNull]
        public string src { get; set; }

        [BsonIgnoreIfNull]
        public string sid { get; set; }

        public long? cov { get; set; }

        public bool adr { get; set; }

        public object rat { get; set; }

        [BsonIgnoreIfNull]
        public string isin { get; set; }

        [BsonIgnoreIfNull]
        public string sedol { get; set; }

        [BsonIgnoreIfNull]
        public string rdk { get; set; }

        [BsonIgnore]
        public string CN
        {
            get
            {
                if (name != null)
                    return name.szh;
                return "";
            }
            set
            {
                if (name != null)
                    name.szh = value;
                else
                {
                    name = new CNEN() { szh = value };
                }
            }
        }

        [BsonIgnore]
        public string abbrCN
        {
            get
            {
                if (abbr != null)
                    return abbr.szh;
                return "";
            }
            set
            {
                if (abbr != null)
                    abbr.szh = value;
                else
                {
                    abbr = new MongoAbbr() { szh = value };
                }
            }
        }

        [BsonIgnore]
        public string orgCN
        {
            get
            {
                if (org != null)
                    return org.szh;
                return "";
            }
            set
            {
                //TODO:此处要根据中文名称获取机构信息（机构信息的编辑只有名称，故此处要得到一个org对象）
                if (org != null)
                    org.szh = value;
                else
                {
                    org = new MongoOrg() { szh = value };
                }
            }
        }

        [BsonIgnore]
        public string orgCD
        {
            get
            {
                if (org != null)
                    return org.Code;
                return "";
            }
            set
            {
                if (org != null)
                    org.Code = value;
                else
                {
                    org = new MongoOrg() { Code = value };
                }
            }
        }

        [BsonIgnore]
        public string orgEN
        {
            get
            {
                if (org != null)
                    return org.en;
                return "";
            }
            set
            {
                if (org != null)
                    org.en = value;
                else
                {
                    org = new MongoOrg() { en = value };
                }
            }
        }

        public int? stat { get; set; }

        //update 20131113-为了添加一个查询条件下的发布功能，必须需要此字段
        [BsonIgnoreIfNull]
        public string bid { get; set; }
    }

    public class CNEN
    {
        public CNEN()
        {
            en = "";
            szh = "";
        }
        /// <summary>
        /// 英文备注
        /// </summary>
        [BsonElement("en")]
        [DataMember]
        public string en { get; set; }

        /// <summary>
        /// 中文备注
        /// </summary>
        [BsonElement("szh")]
        [DataMember]
        public string szh { get; set; }

        #region 方法

        public override string ToString()
        {
            return string.Format("{0}-{1}", en, szh);
        }

        #endregion
    }

    public class MongoMkt : Mongols
    {
        public string abbr { get; set; }
    }

    public class MongoAbbr : CNEN
    {
        /// <summary>
        /// py
        /// </summary>
        [BsonElement("py"), BsonIgnoreIfNull]
        [DataMember]
        public string py { get; set; }
    }

    public class Mongols : CNEN
    {
        public string code { get; set; }

        [BsonIgnoreIfNull]
        public DateTime? dt { get; set; }

        [BsonIgnoreIfNull]
        public DateTime? edt { get; set; }
    }

    public class MongoOrg
    {
        public MongoOrg()
        {
            Code = string.Empty;
            en = string.Empty;
            szh = string.Empty;
        }
        [BsonElement("id")]
        public string Code { get; set; }
        public string en { get; set; }
        public string szh { get; set; }

    }
}
