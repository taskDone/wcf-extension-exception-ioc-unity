﻿using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 中英文名及缩写
    /// </summary>
    public class MonName
    {
        /// <summary>
        /// 英文名
        /// </summary>
        [BsonElement("en")]
        public string EngName { get; set; }

        /// <summary>
        /// 中文名
        /// </summary>
        [BsonElement("szh")]
        public string ChinName { get; set; }

        /// <summary>
        /// 缩写
        /// </summary>
        [BsonElement("abbr")]
        public string Abrev { get; set; }
    }
}
