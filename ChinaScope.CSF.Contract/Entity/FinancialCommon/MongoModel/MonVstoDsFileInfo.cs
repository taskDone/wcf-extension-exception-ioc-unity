﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    public class MonVstoDsFileInfo
    {
        public const string Collection = "vsto_ds_fileinfo";
        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        public ObjectId  _id{ get; set; }

        /// <summary>
        /// tiker
        /// </summary>
        [BsonElement("tik")]
        public string tiker { get; set; }

        /// <summary>
        /// 合并报表
        /// </summary>
        [BsonElement("consolidation")]
        public int consolidation { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        [BsonElement("typ")]
        public int type { get; set; }

        /// <summary>
        /// RptCode
        /// </summary>
        [BsonElement("rpt")]
        public string RptCode { get; set; }


        /// <summary>
        /// 更新时间
        /// </summary>
        [BsonElement("upt"),BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? upt { get; set; }

        /// <summary>
        /// 文件URL
        /// </summary>
        [BsonElement("url")]
        public string url { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        [BsonElement("user")]
        public string user { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        [BsonElement("table")]
        public string table { get; set; }

        /// <summary>
        /// 环境
        /// </summary>
        [BsonElement("envir")]
        public string environment { get; set; }
    }
}
