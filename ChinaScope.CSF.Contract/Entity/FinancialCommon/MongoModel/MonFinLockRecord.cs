﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 报表是否被锁定的记录
    /// </summary>
    public class MonFinLockRecord
    {
        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        public int Id { get; set; }
        /// <summary>
        /// 报表名
        /// </summary>
        [BsonElement("rpt")]
        public string Report { get; set; }
        /// <summary>
        /// ticker
        /// </summary>
        [BsonElement("tik")]
        public string Ticker { get; set; }
        /// <summary>
        /// 合并类型
        /// </summary>
        [BsonElement("consolidation")]
        public int Consodidation { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        [BsonElement("typ")]
        public int Type { get; set; }
        /// <summary>
        /// 操作用户名
        /// </summary>
        [BsonElement("username")]
        public string UserName { get; set; }
        /// <summary>
        /// 操作用户Code,用户唯一区别        
        /// </summary>
        [BsonElement("usercode")]
        public string UserCode { get; set; }
        /// <summary>
        /// 锁定时间
        /// </summary>
        [BsonElement("upt")]
        public DateTime? UpdateTime { get; set; }
    }
}
