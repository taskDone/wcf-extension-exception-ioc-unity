﻿
namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    public class DictSamProductDef
    {
        public const string Collection = "dict_product_def";

        public int _id { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>     
        public string code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>      
        public string name { get; set; }

        /// <summary>
        /// secu
        /// </summary>
        public string secu { get; set; }

        /// <summary>
        /// itemcd
        /// </summary>
        public string itemcd { get; set; }

        public string status { get; set; }

    }

   
       
}
