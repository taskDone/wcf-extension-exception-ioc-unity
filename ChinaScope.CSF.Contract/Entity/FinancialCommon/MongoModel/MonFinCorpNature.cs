﻿using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 公司表
    /// </summary>
    public class MonFinCorpNature
    {
        [BsonElement("_id")]
        public int Id { get; set; }

        /// <summary>
        /// 机构ID
        /// </summary>
        [BsonElement("orgid")]
        public string Orgid { get; set; }

        /// <summary>
        /// 数据源类型（1：juchao）
        /// </summary>
        [BsonElement("src")]
        public string Src { get; set; }

        /// <summary>
        /// 数据源对应ID
        /// </summary>
        [BsonElement("sid")]
        public string Sid { get; set; }

        /// <summary>
        /// 组织名称（如：福建省三奥信息科技股份有限公司）
        /// </summary>
        [BsonElement("orgname")]
        public string Orgname { get; set; }

        [BsonElement("tick")]
        public string Tick { get; set; }

        /// <summary>
        /// security code
        /// </summary>
        [BsonElement("secu")]
        public string SecurityCode { get; set; }

        /// <summary>
        /// 类别（1:通用，2：金融）
        /// </summary>
        [BsonElement("catlog")]
        public string Catlog { get; set; }

        /// <summary>
        /// 子类别（11:通用，21：银行，22：证券，23：保险）
        /// </summary>
        [BsonElement("subcatlog")]
        public string SubcatLog { get; set; }

    }
}
