﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.Sam;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    [Serializable]
    public class MonFinSamProductAndStyle
    {
        public FinSamStyle FinRptStyle { get; set; }
        public List<FinSamProduct> lstItems { get; set; }
    }
}
