﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
   public class BreakDownCode
    {
       public string Code { get; set; }

       public int Row { get; set; }

       public string Classify { get; set; }

       public static BreakDownCode Parse(string argCode)
       {
           string[] strArray = argCode.Split(':');
           BreakDownCode code = new BreakDownCode();
           if (strArray.Length == 3)
           {
               code = new BreakDownCode();
               code.Code = strArray[0];
               code.Row = int.Parse(strArray[1]);
               if (string.IsNullOrEmpty(strArray[2]))
               {
                   code.Classify = "0";
               }
               else
               {
                   code.Classify = strArray[2];
               }
               
           }
           return code;
           
       }

       public override string ToString()
       {
           string strTemp = string.Format("{0}:{1}:{2}",Code,Row,Classify);
           return strTemp;
                
       }
    }
}
