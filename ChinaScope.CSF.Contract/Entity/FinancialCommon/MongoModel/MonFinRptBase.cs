﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    [Serializable]
    //[DataContract(Namespace = Constants.DataContractNamespace)]
    public class MonFinRptBase
    {
        [BsonElement("_id")]
        public long Id { get; set; }

        /// <summary>
        /// 报表类型（is/bs/cf）
        /// </summary>
        [BsonElement("rpt")]
        public string ReportType { get; set; }

        /// <summary>
        /// 是否可用
        /// by alan 2012-9-12
        /// </summary>
        [BsonElement("active")]
        public bool active { get; set; }

        /// <summary>
        /// ticker
        /// </summary>
        [BsonElement("tik")]
        public string Ticker { get; set; }

        /// <summary>
        /// 证券代码
        /// </summary>
        [BsonElement("secu")]
        public string SecurityCode { get; set; }

        /// <summary>
        /// 机构ID
        /// </summary>
        [BsonElement("org")]
        public string OrgnizationID { get; set; }

        /// <summary>
        /// 报告日期
        /// </summary>
        [BsonElement("y")]
        public string ReportYear { get; set; }

        /// <summary>
        /// 报告期间3,6,9,12 (1-12的整数) [取报告年度的月度值作为报告期间的值]
        /// </summary>
        [BsonElement("p")]
        public int Period { get; set; }

        /// <summary>
        /// 属于哪季度: Q1 -- Q4
        /// </summary>
        [BsonElement("q")]
        public string Quarter { get; set; }

        /// <summary>
        /// 合并类型（中英文）1：合并本期；2：合并调整，3：母公司本期，4：母公司调整
        /// </summary>
        [BsonElement("ctyp")]
        public int CombineType { get; set; }

        /// <summary>
        /// 报告来源
        /// </summary>
        [BsonElement("src")]
        public List<string> ReportResource { get; set; }

        /// <summary>
        /// accounting standard (中英文) : 1: IFRS 2: HKFRS 3: PRC GAAP 4: US GAAP 5: OLD PRC GAAP
        /// </summary>
        [BsonElement("std")]
        public MonStandard Standard { get; set; }

        /// <summary>
        /// 准则类型
        /// </summary>
        [BsonElement("stdtyp")]
        public int StandType { get; set; }

        /// <summary>
        /// 年度会计
        /// </summary>
        [BsonElement("fy")]
        public string FiscalYear { get; set; }

        /// <summary>
        /// 财年相关字段
        /// </summary>
        [BsonElement("fp")]
        public string FP { get; set; }

        /// <summary>
        /// report type (中英文) : 11: 通用， 21: 银行， 22: 证券， 23: 保险
        /// </summary>
        [BsonElement("typ")]
        public string Type { get; set; }

        /// <summary>
        /// 量级字典表
        /// </summary>
        [BsonElement("u")]
        public MonScale DicTable { get; set; }

        /// <summary>
        /// (中英文 id) units="CNY" for A shares by default
        /// </summary>
        [BsonElement("c")]
        public MonStandard Cny { get; set; }

        /// <summary>
        /// 录入人
        /// </summary>
        [BsonElement("upu")]
        public string UpdateUser { get; set; }

        /// <summary>
        /// 录入时间
        /// </summary>
        [BsonElement("crt")]
        public DateTime? RecordTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [BsonElement("upt")]
        public DateTime? UpdateTime { get; set; }


        /// <summary>
        /// 状态
        /// </summary>
        [BsonIgnore]
        public String State { get; set; }

        [BsonIgnoreIfNull]
        public int? stat { get; set; }

    }
}
