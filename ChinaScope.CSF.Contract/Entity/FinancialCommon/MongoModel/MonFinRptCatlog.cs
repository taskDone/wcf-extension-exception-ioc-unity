﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 分类表（前台页面使用：4套标准分类+每个公司自定义分类）
    /// </summary>
    public class MonFinRptCatlog
    {
        [BsonElement("_id")]
        public int Id { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [BsonElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// Ticker(标准分类该项取值为空)
        /// </summary>
        [BsonElement("tik")]
        public MonName Ticker { get; set; }

        /// <summary>
        /// 证券代码
        /// </summary>
        [BsonElement("secu")]
        public string SecurityCode { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        [BsonElement("name")]
        public MonRemark CategoryType { get; set; }

        /// <summary>
        /// 分类下报表
        /// </summary>
        [BsonElement("rpt")]
        public List<string> ReportTable { get; set; }
    }
}
