﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 此类中类型有部分与数据库数据类型不匹配
    /// </summary>
    public class MonFinRptStyle
    {
        [BsonElement("_id")]
        public int Id { get; set; }       

        /// <summary>
        /// 报表类型（is/bs/cf）
        /// </summary>
        [BsonElement("rpt")]
        public string ReportType { get; set; }

        /// <summary>
        /// ticker股票代码
        /// </summary> 
        [BsonElement("tik")]
        public string Ticker { get; set; }

        /// <summary>
        /// 安全代码(格式如：601398_SH_EQ)
        /// </summary> 
        [BsonElement("secu")]
        public string SecurityCode { get; set; }

        /// <summary>
        /// 合并准则
        /// </summary> 
        [BsonElement("ctyp")]
        public string CTyp { get; set; }

        /// <summary>
        /// 准则类型
        /// </summary> 
        [BsonElement("stdtyp")]
        public string StarddType { get; set; }

        /// <summary>
        /// 默认为通用1，否则为金融2
        /// </summary>  
        [BsonElement("typ")]
        public string Type { get; set; }  

        /// <summary>
        /// 明细
        /// </summary>
        [BsonElement("items")]
        public List<StyleItem> Items { get; set; }

        /// <summary>
        /// remark
        /// </summary>
        [BsonElement("rem")]
        public string Remark { get; set; }

        /// <summary>
        /// 录入人
        /// </summary>
        [BsonElement("upu")]
        public string UpdateUsers { get; set; }

        /// <summary>
        /// 录入时间
        /// </summary>
        [BsonElement("crt")]
        public DateTime? RecordTime { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [BsonElement("upt")]
        public DateTime? UpdateTime { get; set; }
    }

    public class StyleItem
    {
        /// <summary>
        /// 科目编码
        /// </summary>
        [BsonElement("cd")]
        public string Code { get; set; }

        /// <summary>
        /// 中英文名
        /// </summary>
        [BsonElement("name")]
        public MonRemark Value { get; set; }

        /// <summary>
        /// 顺序
        /// </summary>
        [BsonElement("pos")]
        public int Pos { get; set; }

        /// <summary>
        /// 是否有下一层
        /// </summary>
        [BsonElement("child")]
        public bool IsChild { get; set; }

        /// <summary>
        /// 层级
        /// </summary>
        [BsonElement("level")]
        public int Level { get; set; }

        /// <summary>
        /// 校验公式
        /// </summary>
        [BsonElement("check")]
        public string Check { get; set; }

        /// <summary>
        /// 关闭跟踪
        /// </summary>
        [BsonElement("bt")]
        public string BlockTrace { get; set; }

        /// <summary>
        /// 显示情况（A股需要, 港股默认强制显示, 1：强制显示  2：非强制显示 （当选择的几个数据期该科目数据都为空，不显示）
        /// </summary>
        [BsonElement("disp")]
        public int Disp { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>
        [BsonElement("m")]
        public MonRemark remark { get; set; }

    }
}
