﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{

   public class MonDictRptNature
    {
       public MonDictRptNature()
       {
           Nature = new List<NameValue>();
           Classify = new List<NameValue>();

       }
       public const string Collection = "dict_rptnature";

       [BsonElement("_id")]
       public ObjectId _id { get; set; }

       [BsonElement("rpt")]
       public string Rpt { get; set; }

       [BsonElement("nature")]
       public List<NameValue> Nature { get; set; }

       [BsonElement("classify")]
       public List<NameValue> Classify { get; set; }

    }

    public class NameValue
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
