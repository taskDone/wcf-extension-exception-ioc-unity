﻿using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// 财务报表字典表
    /// </summary>
    public class MonFinRpt
    {
        [BsonElement("_id")]
        public int Id { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [BsonElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// 报名名称
        /// </summary>
        [BsonElement("name")]
        public MonName Name { get; set; }

        /// <summary>
        /// 对应的表明（此字段不一定用）
        /// </summary>
        [BsonElement("tb")]
        public string TableName { get; set; }

        /// <summary>
        /// 相对域名的URl
        /// </summary>
        [BsonElement("url")]
        public string Url { get; set; }

        /// <summary>
        /// 模版样式
        /// </summary>
        [BsonElement("style")]
        public int TemplateStyle { get; set; }
    }
}
