﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    public class DictSamProduct
    {
        public const string Collection = "dict_product_rs";

        public int _id { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// 中英缩写名
        /// </summary>
        public MonRemark name { get; set; }

        [MongoDB.Bson.Serialization.Attributes.BsonIgnore]
        public string CN
        {
            get
            {
                return name.szh;
            }
        }

        /// <summary>
        /// 备注中英文
        /// </summary>
        public MonRemark rem { get; set; }

        /// <summary>
        /// 86张表的哪张表
        /// </summary>
        //public string rpt { get; set; }

        /// <summary>
        /// 明细科目使用，属于哪个主要科目
        /// </summary>
        public string parent { get; set; }

        /// <summary>
        /// ancestors
        /// </summary>
        public List<string> ancestors { get; set; }

        /// <summary>
        /// level
        /// </summary>
        public string level { get; set; }

        /// <summary>
        /// comb
        /// </summary>
        //public List<string> comb { get; set; }  要改回来
        public string comb { get; set; }

        /// <summary>
        /// active
        /// </summary>
        //public int active { get; set; }

        /// <summary>
        /// upu
        /// </summary>
        public string upu { get; set; }

        /// <summary>
        /// upt
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? upt { get; set; }

        /// <summary>
        /// crt
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? crt { get; set; }

        /// <summary>
        /// industry
        /// </summary>
        //public string industry { get; set; }


        /// <summary>
        /// valid
        /// </summary>
        public string valid { get; set; }

        /// <summary>
        /// u
        /// </summary>
        //public string u { get; set; }

        /// <summary>
        /// t
        /// </summary>
        //public string t { get; set; }

        /// <summary>
        /// 对港美股容错
        /// </summary>
        public List<string> rs { get; set; }

        /// <summary>
        /// ind
        /// </summary>
        public List<TitleCode> ind { get; set; }

        public int stat { get; set; }




    }
}
