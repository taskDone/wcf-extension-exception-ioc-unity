﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    /// <summary>
    /// As Reported——income statement/balance sheet/cash flow
    /// </summary>
    public class MonFinRptAsrep:MonFinRptBase
    {
        public const string Collection = "fin_rpt_asrep";
        /// <summary>
        /// 备注
        /// </summary>
        [BsonElement("rem")]
        public MonRemark Remark { get; set; }

        /// <summary>
        /// accounting period 会计期间  (中英文) : 1: YTD  2: CUR  3: LTM
        /// </summary>
        [BsonElement("accp")]
        public string AccountPeriod { get; set; }
    
        [BsonElement("items")]
        public List<AsrepItem> Items { get; set; }
    }

    public class AsrepItem
    {
        /// <summary>
        /// 科目编码
        /// </summary>
        [BsonElement("cd")]
        public string Code { get; set; }

        /// <summary>
        /// 上期值
        /// </summary>
        [BsonElement("v")]
        public string Value { get; set; }

        ///// <summary>
        ///// 本期值
        ///// </summary>
        //[BsonElement("cv")]
        //public string CurrentValue { get; set; }

        ///// <summary>
        ///// 备注
        ///// </summary>
        //[BsonElement("m")]
        //public MonRemark remark { get; set; }
    }
}
