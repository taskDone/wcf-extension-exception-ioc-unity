﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.FinInterface;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.MongoModel
{
    [Serializable]
    public class MonFinRptBreakdown : MonFinRptBase, IDataSchemal<BreakdownItem>
    {
        public const string Collection = "fin_rpt_breakdown";
        /// <summary>
        /// 年度会计
        /// </summary>
        [BsonElement("accp")]
        public string AccountPeriod { get; set; }

        /// <summary>
        /// Cat
        /// </summary>
        [BsonElement("cat")]
        public string Cat { get; set; }//MonStandard

        /// <summary>
        /// 集合
        /// </summary>
        [BsonElement("items")]
        public List<BreakdownItem> Items { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [BsonElement("rem")]
        public MonRemark Remark { get; set; }
    }

    [Serializable]
    public class BreakdownItem
    {

       public BreakdownItem()
        {
            Nat = string.Empty;
            Code = string.Empty;
            LastestValue = string.Empty;
            OriginalValue = string.Empty;
            ValueUS = string.Empty;
            OriTrace = new List<TraceItem>();
            LstTrace = new List<TraceItem>();
           
        }
        /// <summary>
        /// 项目性质：原值、净值
        /// </summary>
        [BsonElement("nat")]
        public string Nat { get; set; }

        /// <summary>
        /// 项目编码
        /// </summary>
        [BsonElement("cd")]
        public string Code { get; set; }

        /// <summary>
        /// 最新值
        /// </summary>
        [BsonElement("lv")]
        public string LastestValue { get; set; }

        /// <summary>
        /// 最新值(美元)
        /// </summary>
        [BsonElement("lvu")]
        public double? LValueU { get; set; }


        /// <summary>
        /// alan add 可能需要修改
        /// </summary>
        [BsonElement("ov")]
        public string OriginalValue { get; set; }

        /// <summary>
        /// alan add 可能需要修改(美元)
        /// </summary>
        [BsonElement("ovu")]
        public double? OValueU { get; set; }

        /// <summary>
        /// 值的美元汇率
        /// </summary>
        [BsonElement("vu")]
        public string ValueUS { get; set; }

        /// <summary>
        /// 运算关系
        /// </summary>
        [BsonElement("calc"),BsonIgnoreIfNull]
        public string CalOperator { get; set; }

        /// <summary>
        /// 跟踪
        /// </summary>
        [BsonElement("t"), BsonIgnoreIfNull]
        public List<string> Trace { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>
        [BsonElement("m"), BsonIgnoreIfNull]
        public MonRemark remark { get; set; }

        /// <summary>
        /// 跟踪的原始值
        /// </summary>
        [BsonElement("o")]
        public List<TraceItem> OriTrace { get; set; }

        /// <summary>
        /// 跟踪最新值
        /// </summary>
        [BsonElement("l")]
        public List<TraceItem> LstTrace { get; set; }

        [BsonIgnoreIfNull]
        public string ori { get; set; }

        /// <summary>
        /// 临时字段
        /// </summary>
        private object _cat = null;
        [BsonElement("cat")]
        public object Cat {
            get
            {
                if (_cat == null)
                {
                    return "0";
                }
                else
                {
                    return _cat;
                }
            }
            set
            {
                if (value!=null&&value.ToString() == "0")
                {
                    _cat = null;
                }
                else
                {
                    _cat = value;
                }
            }
        }

    }
}
