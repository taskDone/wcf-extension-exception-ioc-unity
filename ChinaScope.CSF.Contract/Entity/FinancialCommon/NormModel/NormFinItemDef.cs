﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    ///  一张报表里名字唯一
    /// </summary>
    public class NormFinItemDef
    {
        public const string Collection = "fin_item_def";

        public int _id { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>     
        public string code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>      
        public string name { get; set; }

        /// <summary>
        /// 报表代码
        /// </summary>      
        public string rpt { get; set; }

        public string itemcd { get; set; }
    }
}
