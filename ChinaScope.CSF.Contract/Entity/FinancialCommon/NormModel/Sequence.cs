﻿using Norm;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    public class Sequence
    {
        public const string Collection = "ids";

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        /// <remarks></remarks>
        [MongoIdentifier]
        public ObjectId Id { get; set; }

        /// <summary>
        /// Gets or sets the field_name.
        /// </summary>
        /// <value>The field_name.</value>
        /// <remarks></remarks>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        /// <remarks></remarks>
        public long value { get; set; }
    }
}
