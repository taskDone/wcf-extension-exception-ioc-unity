﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    public class NormFinRptAsrepExt:NormFinRptBase
    {        
        public const string Collection = "fin_rpt_asrep11";


        /// <summary>
        /// remark
        /// </summary>       
        public NormRemark rem { get; set; }

        /// <summary>
        /// 会计期间
        /// </summary>
        public string accp { get; set; }

        public List<NormAsrepExtItem> items { get; set; }
    }

    public class NormAsrepExtItem
    {
        /// <summary>
        /// 科目编码
        /// </summary>
        public string cd { get; set; }

        /// <summary>
        /// 原始值
        /// </summary>
        public string ov { get; set; }

        /// <summary>
        /// 原始值美元汇率转换
        /// </summary>
        public string ovu { get; set; }

        /// <summary>
        /// 最新值
        /// </summary>
        public NormRemark lv { get; set; }

        /// <summary>
        /// 最新值美元汇率转换
        /// </summary>
        public string lvu { get; set; }

        /// <summary>
        /// 前段trace的字段
        /// </summary>
        [BsonIgnore]
        public string ori { get; set; }
         
        /// <summary>
        /// 跟踪的原始值
        /// </summary>      
        public List<NormTraceItem> o { get; set; }

        /// <summary>
        /// 跟踪最新值
        /// </summary>      
        public List<NormTraceItem> l { get; set; }

        /// <summary>
        /// 跟踪
        /// </summary>
        public List<string> t { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>
        public NormRemark m { get; set; }

    }
}
