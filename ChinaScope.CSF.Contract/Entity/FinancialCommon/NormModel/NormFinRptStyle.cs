﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using Norm.Attributes;
using System;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    [Serializable, DataContract]
    public class NormFinRptStyle : NormFinStyleBase, ICloneable
    {
        public NormFinRptStyle()
        {
            items = new List<NormStyleItem>();
        }
        public const string Collection = "fin_rpt_style";

        /// <summary>
        /// 明细
        /// </summary>
        [DataMember]
        public List<NormStyleItem> items
        {
            get;
            set;
        }

        /// <summary>
        /// 克隆
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            MemoryStream stream = new MemoryStream();

            BinaryFormatter formatter = new BinaryFormatter();

            formatter.Serialize(stream, this);

            stream.Position = 0;

            var obj = formatter.Deserialize(stream) as NormFinRptStyle;

            return obj;
        }
    }

    [Serializable, DataContract]
    public class NormStyleItem : ICloneable
    {
        public NormStyleItem()
        {
            disp = "1";
        }

        public NormStyleItem(string cd, string szh, string en, int argPos, int argLevel)
        {
            this.cd = cd;
            name = new NormRemark() { szh = szh,en =en };
            pos = argPos;
            level = argLevel;
            disp = "1";
        }
        /// <summary>
        /// 科目编码
        /// </summary>   
        private string _cd;
        [DataMember]
        public string cd
        {
            get
            {
                if (_cd == null)
                {
                    return "";
                }
                return _cd;
            }
            set
            {
                _cd = value;
            }
        }

        /// <summary>
        /// 中英文名
        /// </summary>  
        private NormRemark _name;
        [DataMember]
        public NormRemark name
        {
            get
            {
                if (_name == null)
                {
                    return new NormRemark();
                } 
                else
                {
                    return _name;
                }
            }
            set { _name = value; }
        }


        /// <summary>
        /// 顺序
        /// </summary>    
        [DataMember]
        public int pos { get; set; }

        /// <summary>
        /// 是否有下一层
        /// </summary>     
        //public bool child { get; set; }
        [DataMember]
        public string child { get; set; }

        /// <summary>
        /// 层级
        /// </summary>   
        [DataMember]
        public int level { get; set; }

        /// <summary>
        /// 校验公式
        /// </summary> 
        [DataMember]
        public string check { get; set; }

        /// <summary>
        /// 关闭跟踪
        /// </summary>    
        [DataMember]
        public string bt { get; set; }

        /// <summary>
        /// 显示情况（A股需要, 港股默认强制显示, 1：强制显示  2：非强制显示 （当选择的几个数据期该科目数据都为空，不显示)
        /// </summary>      
        //public int disp { get; set; }
        [DataMember]
        public string disp { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>   
        /// 
        private NormRemark _m;
        [DataMember]
        public NormRemark m
        {
            get
            {
                if (_m == null)
                    return new NormRemark();
                else
                {
                    return _m;
                }
            }
            set { _m = value; }
        }

        [MongoIgnore,BsonIgnore]
        public int Row { get; set; }

   [MongoIgnore, BsonIgnore]
        public string CombCode
        {
            get
            {
                return cd + ":" + Row + ":" + cat;
            }
        }

        [MongoIgnore, BsonIgnore]
        public int CheckRow { get; set; }

         [MongoIgnore, BsonIgnore]
        public string EN
        {
            get { return name.en; }
        }

        [MongoIgnore, BsonIgnore]
        public string nat { get; set; }

         [MongoIgnore, BsonIgnore]
        public string cat { get; set; }


        [MongoIgnore, BsonIgnore]
        public string CN
        {
            get
            {
                if (!string.IsNullOrEmpty(nat))
                {
                    string strNat = "-";
                    switch (nat)
                    {
                        case "1":
                            strNat += "总值";
                            break;
                        case "3":
                            strNat += "减值";
                            break;
                        case "5":
                            strNat += "净值";
                            break;
                        default:
                            strNat = "";
                            break;
                    }
                    return name.szh + strNat;
                }
                else
                {
                    return name.szh;
                }

            }
        }


        public Object Clone()
        {
            MemoryStream stream = new MemoryStream();

            BinaryFormatter formatter = new BinaryFormatter();

            formatter.Serialize(stream, this);

            stream.Position = 0;

            var obj = formatter.Deserialize(stream) as NormStyleItem;

            return obj;
        }
    }
}
