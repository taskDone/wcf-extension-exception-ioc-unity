﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    public class NormTrans
    {      
        public NormAS asrep { get; set; }
    
        public NormTpl tpl { get; set; }
    }

    /// <summary>
    /// AsReport
    /// </summary>
    public class NormAS
    {
        /// <summary>
        /// 据朝科目ID运算
        /// </summary>
        public string gen { get; set; }

        /// <summary>
        /// 金融科目
        /// </summary>
        public string fin { get; set; }
    }

    /// <summary>
    /// Templated
    /// </summary>
    public class NormTpl
    {
        /// <summary>
        /// 通用类型
        /// </summary>
        public string gen { get; set; }

        /// <summary>
        /// 银行科目
        /// </summary>
        public string bank { get; set; }

        /// <summary>
        /// 保险科目
        /// </summary>
        public string ins { get; set; }

        /// <summary>
        /// 证券
        /// </summary>
        public string sec { get; set; }
    }
}
