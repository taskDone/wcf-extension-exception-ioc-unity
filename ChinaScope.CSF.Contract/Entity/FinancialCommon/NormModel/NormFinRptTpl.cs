﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// templated
    /// </summary>
    public class NormFinRptTpl:NormFinRptBase
    {
        /// <summary>
        /// CUR模板表名
        /// </summary>
        public const string Collection = "fin_rpt_tpl_ytd";
        /// <summary>
        /// 备注
        /// </summary>
        public NormRemark rem { get; set; }


        
        public string accp { get; set; }
    
        public List<NormTplItem> items { get; set; }
    }

    public class NormTplItem
    {
        /// <summary>
        /// 科目编码
        /// </summary>
        public string cd { get; set; }

        /// <summary>
        /// 本期值
        /// </summary>
        public string ov { get; set; }

        /// <summary>
        /// 原始值说明
        /// </summary>
        public string ovu { get; set; }

        /// <summary>
        /// 最新值
        /// </summary>
        public string lv { get; set; }

        /// <summary>
        /// 最新值美元汇率转换
        /// </summary>
        public string lvu { get; set; }

        /// <summary>
        /// 运算关系
        /// </summary>
        public string calc { get; set; }

        /// <summary>
        /// 前段trace的字段
        /// </summary>
        [BsonIgnore]
        public string ori { get; set; }

        /// <summary>
        /// trace
        /// </summary>
        public List<string> t { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>
        public NormRemark m { get; set; }

    }

}
