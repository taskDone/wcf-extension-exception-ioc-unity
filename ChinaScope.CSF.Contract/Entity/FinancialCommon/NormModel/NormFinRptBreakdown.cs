﻿using System.Collections.Generic;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    public class NormFinRptBreakdown:NormFinRptBase
    {
        /// <summary>
        /// 年度会计
        /// </summary>
        public int accp { get; set; }

        /// <summary>
        /// Cat
        /// </summary>
        public NormStandard cat { get; set; }

        /// <summary>
        /// 集合
        /// </summary>
        public List<NormBreakdownItem> items { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public NormRemark rem { get; set; }
    }

    public class NormBreakdownItem
    {
        /// <summary>
        /// 项目性质：原值、净值
        /// </summary>
        public string nat { get; set; }

        /// <summary>
        /// 项目编码
        /// </summary>
        public string cd { get; set; }

        /// <summary>
        /// 最新值
        /// </summary>
        public string v { get; set; }

        /// <summary>
        /// 值的美元汇率
        /// </summary>
        public string vu { get; set; }

        /// <summary>
        /// 运算关系
        /// </summary>
        public string calc { get; set; }

        /// <summary>
        /// 跟踪
        /// </summary>
        public List<string> t { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>
        public NormRemark m { get; set; }

    }

}
