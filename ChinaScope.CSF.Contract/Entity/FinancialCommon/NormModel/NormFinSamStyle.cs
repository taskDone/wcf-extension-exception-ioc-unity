﻿using System.Collections.Generic;
using System;
using MongoDB.Bson.Serialization.Attributes;
using Norm.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    [Serializable]
    public class NormFinSamStyle : NormFinStyleBase
    {
        public const string Collection = "fin_sam_style";

        public string status { get; set; }

        /// <summary>
        /// 明细
        /// </summary>
        public List<NormSamStyleItem> items { get; set; }

        public NormFinRptStyle ToRptStyle()
        {
            NormFinRptStyle rptSytle = new NormFinRptStyle();
            rptSytle.exchg = this.exchg;
            rptSytle.rpt = this.rpt;
            rptSytle.tik = this.tik;
            rptSytle.secu = this.secu;
            rptSytle.ctyp = this.ctyp;
            rptSytle.stdtyp = this.stdtyp;
            rptSytle.typ = this.typ;
            rptSytle.accp = this.accp;
            rptSytle.rem = this.rem;
            rptSytle.upu = this.upu;
            rptSytle.crt = this.crt;
            rptSytle.upt = this.upt;

            foreach (NormSamStyleItem item in this.items)
            {
                rptSytle.items.Add(item);
            }

            return rptSytle;
        }
    }
    [Serializable]
    public class NormSamStyleItem : NormStyleItem
    {
        public NormSamStyleItem()
        {
            FiType = "00";
        }
        /// <summary>
        /// fi and type
        /// </summary>
        [BsonElement("fi")]
        public string FiType { get; set; }

        [BsonIgnore]
        public string fi
        {
            get
            {
                if (FiType.Length == 2)
                {
                    return FiType.ToCharArray()[0].ToString();
                }
                return "";
            }
            set
            {
                if (FiType.Length == 2)
                {
                    char[] charArray = FiType.ToCharArray();
                    charArray[0] = char.Parse(value);
                    FiType = new string(charArray);
                }
            }
        }

        /// <summary>
        /// type
        /// </summary>
        [BsonIgnore]
        public string typ
        {
            get
            {
                if (FiType.Length == 2)
                {
                    return FiType.ToCharArray()[1].ToString();
                }
                else if (FiType.StartsWith("m") && FiType.Length == 3)
                {
                    return FiType.Substring(2);
                }
                return "";
            }
            set
            {
                if (FiType.Length == 2)
                {
                    char[] charArray = FiType.ToCharArray();
                    charArray[1] = char.Parse(value);
                    FiType = new string(charArray);
                }
                else if (FiType.StartsWith("m") && FiType.Length == 3)
                {
                    string strFi = FiType.Substring(0, 2);
                    FiType = strFi + value;
                }
            }
        }

        /// <summary>
        /// 拼接起来的CODE 用于数据填充
        /// </summary>
        [BsonIgnore]
        public string CombinationCode
        {
            get
            {
                return fi + "|" + typ + "|" + cd;
            }

        }

    }
}
