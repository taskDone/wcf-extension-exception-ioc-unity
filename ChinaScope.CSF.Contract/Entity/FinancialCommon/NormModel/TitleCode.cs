﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
   public class TitleCode
    {
       /// <summary>
       /// title
       /// </summary>
       public string t { get; set; }

       /// <summary>
       /// code
       /// </summary>
       public string cd { get; set; }
    }
}
