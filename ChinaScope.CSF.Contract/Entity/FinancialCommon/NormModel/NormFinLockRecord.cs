﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// 报表是否被锁定的记录
    /// </summary>
    public class NormFinLockRecord
    {

        public const string Collection = "vsto_ds_lock";

        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        public ObjectId _id { get; set; }
        /// <summary>
        /// 报表名
        /// </summary>
        public string rpt { get; set; }
        /// <summary>
        /// ticker
        /// </summary>
        public string tik { get; set; }
        /// <summary>
        /// 合并类型
        /// </summary>
        public int consolidation { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public int typ { get; set; }
        /// <summary>
        /// 操作用户名
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// 操作用户Code,用户唯一区别        
        /// </summary>
        public string usercode { get; set; }
        /// <summary>
        /// 锁定时间
        /// </summary>
        public DateTime? upt { get; set; }
        /// <summary>
        /// 锁定的环境是(qa/dev/internal)
        /// </summary>
        public string environment { get; set; }
    }
}
