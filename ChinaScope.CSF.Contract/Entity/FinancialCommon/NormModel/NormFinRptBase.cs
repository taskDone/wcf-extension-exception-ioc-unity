﻿using System;
using System.Collections.Generic;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    public class NormFinRptBase
    {
        public int _id { get; set; }

        /// <summary>
        /// 报表类型（is/bs/cf）
        /// </summary>      
        public string rpt { get; set; }

        public bool active { get; set; }

        /// <summary>
        /// ticker
        /// </summary>      
        public string tik { get; set; }

        /// <summary>
        /// 证券代码
        /// </summary>   
        public string secu { get; set; }

        /// <summary>
        /// 机构ID
        /// </summary>      
        public string org { get; set; }

        /// <summary>
        /// 报告日期
        /// </summary>       
        public string y { get; set; }

        /// <summary>
        /// 报告期间3,6,9,12 (1-12的整数) [取报告年度的月度值作为报告期间的值]
        /// </summary>      
        public int p { get; set; }

        /// <summary>
        /// Quarter: Q1 -- Q4
        /// </summary>     
        public string q { get; set; }

        /// <summary>
        /// 合并类型（中英文）1：合并本期；2：合并调整，3：母公司本期，4：母公司调整
        /// </summary>      
        public int ctyp { get; set; }

        /// <summary>
        /// 报告来源
        /// </summary>     
        public List<string> src { get; set; }

        /// <summary>
        /// accounting standard (中英文) : 1: IFRS 2: HKFRS 3: PRC GAAP 4: US GAAP 5: OLD PRC GAAP
        /// </summary>     
        public NormStandard std { get; set; }

        /// <summary>
        /// 准则类型
        /// </summary>     
        public int stdtyp { get; set; }

        /// <summary>
        /// 年度会计
        /// </summary>     
        public string fp { get; set; }

        /// <summary>
        /// 财年相关
        /// </summary>     
        public string fy { get; set; }

        /// <summary>
        /// report type (中英文) : 11: 通用， 21: 银行， 22: 证券， 23: 保险
        /// </summary>      
        public string typ { get; set; }

        /// <summary>
        /// 量级字典表
        /// </summary>       
        public NormScale u { get; set; }

        /// <summary>
        /// (中英文 id) units="CNY" for A shares by default
        /// </summary>      
        public NormStandard c { get; set; }       

        /// <summary>
        /// 录入人
        /// </summary>       
        public string upu { get; set; }

        /// <summary>
        /// 录入时间
        /// </summary>    
        public DateTime? crt { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? upt { get; set; }
    }
}
