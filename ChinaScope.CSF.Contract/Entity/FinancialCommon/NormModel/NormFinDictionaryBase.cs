﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// 字典类基类（Financial Dictionary）
    /// </summary>
    public class NormFinDictionaryBase
    {
        /// <summary>
        /// ID
        /// </summary>     
        public int _id { get; set; }

        /// <summary>
        /// 编码类型
        /// </summary>      
        public string code { get; set; }

        /// <summary>
        /// 英文名称
        /// </summary>       
        public string ename { get; set; }

        /// <summary>
        /// 中文名称
        /// </summary>       
        public string zhsname { get; set; }
    }
}
