﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    public class NormFinItemCalc
    {
        public int _id { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>    
        public string code { get; set; }

        /// <summary>
        /// 转换关系
        /// </summary>    
        public NormTrans trans { get; set; }
    }
}
