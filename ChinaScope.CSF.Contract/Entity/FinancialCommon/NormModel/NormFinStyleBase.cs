﻿using System;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.FinCommon;
using MongoDB.Bson.Serialization.Attributes;
using Norm.Attributes;
using System.Runtime.Serialization;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    [Serializable, DataContract]
    public class NormFinStyleBase
    {
        [DataMember]
        public long _id { get; set; }

        /// <summary>
        /// 判断是A股还是H股，从而决定是否保存样式
        /// </summary>    
        [BsonIgnoreIfNull]
        [DataMember]
        public string exchg { get; set; }

        /// <summary>
        /// data come from which table 0:common 1:style
        /// </summary>
        [DataMember]
        [BsonIgnore]
        public int from { get; set; }

        /// <summary>
        /// 报表类型（is/bs/cf）
        /// </summary>    
        [DataMember]
        public string rpt { get; set; }

        /// <summary>
        /// ticker股票代码
        /// </summary>  
        [DataMember]
        public string tik { get; set; }

        /// <summary>
        /// 安全代码(格式如：601398_SH_EQ)
        /// </summary>   
        [DataMember]
        public string secu { get; set; }

        //public string org { get; set; }

        /// <summary>
        /// 合并准则
        /// </summary>  
        [DataMember]
        public Consolidation ctyp { get; set; }

        /// <summary>
        /// 准则类型
        /// </summary>   
        [DataMember]
        public AccountStandard stdtyp { get; set; }

        /// <summary>
        /// 默认为通用"11"，否则为金融"2*"
        /// </summary> 
        [DataMember]
        public string typ { get; set; }

        [DataMember]
        public string accp { get; set; }

        /// <summary>
        /// remark
        /// </summary>   
        [DataMember]
        public string rem { get; set; }

        /// <summary>
        /// 录入人
        /// </summary>  
        [DataMember]
        public string upu { get; set; }

        /// <summary>
        /// 录入时间
        /// </summary>   
        [DataMember]
        public DateTime? crt { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>    
        [DataMember]
        public DateTime? upt { get; set; }

        [DataMember]
        [BsonIgnoreIfNull]
        public int? stat { get; set; }
    }
}
