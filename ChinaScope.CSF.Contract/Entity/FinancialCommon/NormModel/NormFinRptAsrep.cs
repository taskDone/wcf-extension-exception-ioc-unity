﻿using System.Collections.Generic;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    public class NormFinRptAsrep:NormFinRptBase
    {
        /// <summary>
        /// remark
        /// </summary>       
        public NormRemark rem { get; set; }

        /// <summary>
        /// accounting period 会计期间  (中英文) : 1: YTD  2: CUR  3: LTM
        /// </summary>       
        public string accp { get; set; }
       
        public List<NormAsrepItem> items { get; set; }
    }

    public class NormAsrepItem
    {
        /// <summary>
        /// 科目编码
        /// </summary>      
        public string cd { get; set; }

        /// <summary>
        /// 上期值
        /// </summary>     
        public string pv { get; set; }

        /// <summary>
        /// 本期值
        /// </summary>     
        public string cv { get; set; }

        /// <summary>
        /// 备注
        /// </summary>       
        public NormRemark m { get; set; }
    }
}
