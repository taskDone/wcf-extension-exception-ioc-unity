﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// 中英文名及缩写
    /// </summary>
    public class NormName
    {
        /// <summary>
        /// 英文名
        /// </summary>
        public string en { get; set; }

        /// <summary>
        /// 中文名
        /// </summary>
        public string szh { get; set; }

        /// <summary>
        /// 缩写
        /// </summary>
        public string abbr { get; set; }
    }
}
