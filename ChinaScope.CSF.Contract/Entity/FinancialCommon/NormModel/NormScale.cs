﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    public class NormScale
    {
        public string cd { get; set; }

        /// <summary>
        /// 英文的账号标准
        /// </summary>      
        public string en { get; set; }

        /// <summary>
        /// 中文的账号标准
        /// </summary>     
        public string szh { get; set; }

        /// <summary>
        /// scale
        /// </summary>     
        public string scal { get; set; }
    }
}
