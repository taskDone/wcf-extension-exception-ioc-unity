﻿using System;
using System.Collections.Generic;
namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// 财务科目表
    /// </summary>
    public class NormFinItem
    {
        public const string Collection = "fin_item";

        public int _id { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// 中英缩写名
        /// </summary>
        public NormName name { get; set; }


        /// <summary>
        /// 86张表的哪张表
        /// </summary>
        public string rpt { get; set; }

        /// <summary>
        /// 明细科目使用，属于哪个主要科目
        /// </summary>
        public string parent { get; set; }

        /// <summary>
        /// 1：货币，2：比率；3：倍数
        /// </summary>
        public string nat { get; set; }

        /// <summary>
        /// 小数位数
        /// </summary>
        public int digit { get; set; }

        /// <summary>
        /// 后缀（百分号，倍数符号）
        /// </summary>
        public string suff { get; set; }

        /// <summary>
        /// 数据点追溯类型（0：不做trace，1：数据追溯，2：公式追溯）
        /// </summary>
        public string trace { get; set; }

        /// <summary>
        /// 单位切换
        /// </summary>
        public bool utr { get; set; }

        /// <summary>
        /// 货币切换
        /// </summary>
        public bool ctr { get; set; }

        /// <summary>
        /// 证券代码
        /// </summary>
        public string tik { get; set; }

        /// <summary>
        /// 安全码SecutiryCode
        /// </summary>
        public string secu { get; set; }

        /// <summary>
        /// 对港美股容错
        /// </summary>
        public List<string> rs { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public NormRemark rem { get; set; }

        /// <summary>
        /// upt
        /// </summary>
        public DateTime? upt { get; set; }

        /// <summary>
        /// upu
        /// </summary>
        public string upu { get; set; }

        /// <summary>
        /// stat
        /// </summary>
        public int? stat { get; set; }

    }

}
