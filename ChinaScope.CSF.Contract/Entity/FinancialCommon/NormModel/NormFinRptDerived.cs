﻿using System.Collections.Generic;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// 衍生表
    /// </summary>
    public class NormFinRptDerived:NormFinRptBase
    {
        /// <summary>
        /// 备注
        /// </summary>
        public string rem { get; set; }

        public List<NormDerivedItem> items { get; set; }
    }

    public class NormDerivedItem
    {
        /// <summary>
        /// 项目编码
        /// </summary>
        public string cd { get; set; }

        /// <summary>
        /// 值（数字/NA/NM）
        /// </summary>
        public string v { get; set; }

        /// <summary>
        /// 值的美元汇率转换
        /// </summary>
        public string vu { get; set; }

        /// <summary>
        /// 运算关系
        /// </summary>
        public NormRemark calc { get; set; }

        /// <summary>
        /// 跟踪
        /// </summary>
        public List<string> t { get; set; }

        /// <summary>
        /// 中英文备注
        /// </summary>
        public NormRemark m { get; set; }

    }

}
