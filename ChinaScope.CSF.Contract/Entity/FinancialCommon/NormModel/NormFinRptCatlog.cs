﻿using System.Collections.Generic;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// 分类表（前台页面使用：4套标准分类+每个公司自定义分类）
    /// </summary>
    public class NormFinRptCatlog
    {
        public int _id { get; set; }

        /// <summary>
        /// 编码
        /// </summary>     
        public string code { get; set; }

        /// <summary>
        /// Ticker(标准分类该项取值为空)
        /// </summary>      
        public NormName tik { get; set; }

        /// <summary>
        /// 证券代码
        /// </summary>      
        public string secu { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>      
        public NormRemark name { get; set; }

        /// <summary>
        /// 分类下报表
        /// </summary>      
        public List<string> rpt { get; set; }
    }
}
