﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// 公司性质
    /// </summary>
    public class NormFinCorpNature
    {
        public const string Collection = "fin_corp_nature";//公司性质表

        public int _id { get; set; }

        /// <summary>
        /// 机构ID
        /// </summary>        
        public string orgid { get; set; }

        /// <summary>
        /// 数据源类型（1：juchao）
        /// </summary>      
        public string src { get; set; }

        /// <summary>
        /// 数据源对应ID
        /// </summary> 
        public string sid { get; set; }

        /// <summary>
        /// 组织名称（如：福建省三奥信息科技股份有限公司）
        /// </summary>      
        public string orgname { get; set; }
     
        public string tick { get; set; }

        /// <summary>
        /// security code
        /// </summary>       
        public string secu { get; set; }

        /// <summary>
        /// 类别（1:通用，2：金融）
        /// </summary>
        public string catlog { get; set; }

        /// <summary>
        /// 子类别（11:通用，21：银行，22：证券，23：保险）
        /// </summary>      
        public string subcatlog { get; set; }

        /// <summary>
        /// 标识是港股还是A股
        /// </summary>
        public string exchange { get; set; }

        /// <summary>
        /// 样式
        /// </summary>
        public string style { get; set; }

        /// <summary>
        /// 
        /// </summary>   
        public string upu { get; set; }//类似于00016这样的字符串，2013-05-10 Kelvin要求添加该字段

        /// <summary>
        /// 
        /// </summary>   
        public object upt { get; set; }//2013-05-10 Kelvin要求添加该字段

        public int stat { get; set; }

        public string bid { get; set; }
    }
}
