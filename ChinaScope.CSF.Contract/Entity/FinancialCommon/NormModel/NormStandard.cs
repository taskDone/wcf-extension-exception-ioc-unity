﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// 账号标准（accounting standard）
    /// </summary>
    public class NormStandard
    {      
        public string cd { get; set; }

        /// <summary>
        /// 英文的账号标准
        /// </summary>      
        public string en { get; set; }

        /// <summary>
        /// 中文的账号标准
        /// </summary>     
        public string szh { get; set; }
    }
}
