﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// 财务报表字典表
    /// </summary>
    public class NormFinRpt
    {       
        public int _id { get; set; }

        /// <summary>
        /// 编码
        /// </summary>      
        public string code { get; set; }

        /// <summary>
        /// 报名名称
        /// </summary>      
        public NormName name { get; set; }

        /// <summary>
        /// 对应的表明（此字段不一定用）
        /// </summary>      
        public string tb { get; set; }

        /// <summary>
        /// 相对域名的URl
        /// </summary>      
        public string url { get; set; }

        /// <summary>
        /// 模版样式
        /// </summary>      
        public int style { get; set; }
    }
}
