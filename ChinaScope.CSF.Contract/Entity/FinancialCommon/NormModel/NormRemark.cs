﻿using System;
using System.Runtime.Serialization;
namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.NormModel
{
    /// <summary>
    /// 中英文备注表
    /// </summary>
    [Serializable, DataContract]
    public class NormRemark
    {
        /// <summary>
        /// 英文备注
        /// </summary>
         [DataMember]
        public string en { get; set; }

        /// <summary>
        /// 中文备注
        /// </summary>
         [DataMember]
        public string szh { get; set; }
    }

    /// <summary>
    /// 录入科目跟踪的原始值和最新值
    /// </summary>  
    public class NormTraceItem
    {
        /// <summary>
        /// 关联code
        /// </summary>
         [DataMember]
        public string cd { get; set; }

        /// <summary>
        /// 关联值
        /// </summary>  
         [DataMember]
        public string v { get; set; }
    }
}
