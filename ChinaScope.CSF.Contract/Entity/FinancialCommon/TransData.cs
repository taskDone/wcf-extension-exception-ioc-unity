﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon
{

    /// <summary>
    /// Add by alan 
    /// 2012-9-5 10:00 
    /// 此类是为了传递给服务接口所需参数设计的
    /// 
    /// 由于某些科目有是通过公式计算所得，并却公式中引用到了其他报表中的科目
    /// 在提交后，需要把改变的数据点记录并且传递给特定服务，此服务负责执行将所
    /// 有报表中引用到此数据点的科目进行重新计算 
    /// </summary>
    public class TransData
    {
        //TODO:根据服务端需要接受的参数完善此类
        public string ID { get; set; }
    }
}
