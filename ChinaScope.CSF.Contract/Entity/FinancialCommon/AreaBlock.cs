﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon
{
    public class AreaBlock
    {
        public int Start { get; set; }
        public int End { get; set; }
        public string Name { get; set; }
    }
}
