﻿namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.FinCommon
{
    public static class CommonTemplateConfigure
    {
        #region  TableName
        public static string CurrencyTbl = "dict_currency";
        public static string UnitTbl = "dict_magnitude";
        public static string AccountStandardTbl = "dict_std_type";
        //public static string NatureTbl = "dict_model_nature";
        public static string NatureTbl = "fin_acc_nat";
        #endregion

        //数据列的级别,用于显示不同的Style
        public static string ItemLevel1Col = "G";
        public static string ItemLevel2Col = "H";
        public static string ItemLevel3Col = "I";
        public static string ItemLevel4Col = "J";
        public static string ItemLevel5Col = "K";
        public static string ItemLevel6Col = "L";
        public static string ItemLevel7Col = "M";

        //LTM应用样式
        public static string LTMItemLevel1Col = "B";
        public static string LTMItemLevel2Col = "C";
        public static string LTMItemLevel3Col = "D";
        public static string LTMItemLevel4Col = "E";
        public static string LTMItemLevel5Col = "F";

        //CUR应用样式
        public static string CURItemLevel1Col = "B";
        public static string CURItemLevel2Col = "C";
        public static string CURItemLevel3Col = "D";
        public static string CURItemLevel4Col = "E";
        public static string CURItemLevel5Col = "F";
        public static string CURItemLevel6Col = "G";
        public static string CURItemLevel7Col = "H";

        //Data Area
        public static int FirstColForBindingData = 16;
        public static int LTMFirstColForBindingData = 11;

        //每期显示多少年数据
        public static int DefaultDisplaySeveralYears = 5;
        //总共显示多少期
        public static int AccountQuartors = 4;
    }
}
