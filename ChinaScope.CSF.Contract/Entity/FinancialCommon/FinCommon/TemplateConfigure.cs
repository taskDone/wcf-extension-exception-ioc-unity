﻿using System.ComponentModel;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.FinCommon
{
    public static class TemplateConfigure
    {
        //通用配置
        //--------定义sheet导航----------//
        public const int IsYTDAsRep = 1;
        public const int IsCURAsRep = 2;
        public const int IsLTMAsRep = 3;
        public const int IsHYRAsRep = 4;
        public const int IsYTDAsTpl = 5;
        public const int IsCURAsTpl = 6;
        public const int IsLTMTpl = 7;
        public const int IsHYRAsTpl = 8;
        public const int IsAsSameTpl = 9;

        public const int BSYTDAsRep = 10;
        public const int BSYTDTpl = 11;
        public const int CFYTDRep = 12;
        public const int CFCURAsRep = 13;
        public const int CFLTMAsRep = 14;
        public const int CFHYRAsRep = 15;
        public const int CFYTDTpl = 16;
        public const int Inventories = 17;
        public const int BD252 = 18;
        public const int BD253 = 19;
        public const int BD261 = 20;
        public const int BD262 = 21;
        public const int BD263 = 22;
        public const int BD271 = 23;
        public const int BD281 = 24;
        public const int BD291 = 25;

        public const int B2111 = 26;
        public const int B2161 = 27;
        public const int B251 = 28;
        public const int B311 = 29;
        public const int B321 = 30;
        public const int B331 = 31;
        public const int B341 = 32;
        public const int B351 = 33;
        public const int B361 = 34;
        public const int B411 = 35;
        public const int B421 = 36;
        public const int B511 = 37;
        public const int B711 = 38;
        public const int B721 = 39;

        public const int AdminManagement = 40;

        //check间隔设置
        public static int CheckBlankRowCount = 2;
        public static int CheckDownRowCount = 4;

        //Input Area
        public static int BaseReportTypeRow = 4;
        public static string BaseReportTypeCol = "E";
        public static int BaseAccountStandardRow = 5;
        public static string BaseAccountStandardCol = "E";

        //Load Area
        public static string FirstColForBindingArea= "D";
        public static string SecondColForBindingArea = "E";
        public static int ContentRow = 15;
        public static int QuarterRow = 18;
        public static int CurrencyRow = 21;
        public static int UnitRow = 22;
        public static int FirstBindingRowForLoadArea = 16;
        public static int EndBindingRowForLoadArea = 22;

        //Q4
        public static string FirstColForQ4 = "Q";
        public static string EndColForQ4 = "U";

        //Q3
        public static string FirstColForQ3 = "X";
        public static string EndColForQ3 = "AB";

        //Q2
        public static string FirstColForQ2 = "AE";
        public static string EndColForQ2 = "AI";

        //Q1
        public static string FirstColForQ1 = "AL";
        public static string EndColForQ1 = "AP";

        //Data Area
        public static int FirstBindingRowForDataArea = 26;
        public static int FirstTitleRow = 25;
        public static string ItemLevel1Col = "G";
        public static string ItemLevel2Col = "H";
        public static string ItemLevel3Col = "I";
        public static string ItemLevel4Col = "J";
        public static string ItemLevel5Col = "K";
        public static string ItemLevel6Col = "L";
        public static string ItemLevel7Col = "M";
        public static string ItemENameCol = "N";
        public static string ItemCodeCol = "F";

        public static string LatestText = "Latest";
        public static string PreviousText = "Original";

        public static string[] HildeColumnNames = new string[] { "A", "B", "C", "D", "E", "F" };

        //为LTM模版动态添加验证公式定义的行列数值
        public static int LTMContentRow = 9;
        public static int LTMStartRow = 20;
        public static int LTMEndRow = 87;
        public static int CURTplEndRow = 300;
        public static int LTMShowBindRow = 14;
        public static int YTDTplEndRow = 300;//updateby lh 2012-9-25

        #region  TableName
        public static string CurrencyTbl = "dict_currency";
        public static string UnitTbl = "dict_units";
        public static string MagnitudeTbl = "dict_magnitude";
        #endregion

        //formula Valid row for LTM
        public static int[] formulaValidRows = new int[7] { 16, 17, 18, 19, 21, 22, 23 };

        //YTD，CUR，LTM的枚举
        public enum Accp
        {
            [Description("1")]
            YTD,

            [Description("2")]
            CUR,

            [Description("3")]
            LTM,

            [Description("4")]
            HYR
        }

        /// <summary>
        /// 定义类型11: 通用， 21: 银行， 22: 证券， 23: 保险
        /// 获取本枚举的值，调用Utils类中的GetDescription方法
        /// </summary>
        public enum ClassifyType { 
            [Description("11")]
            General,
            [Description("21")]
            Bank,
            [Description("22")]
            Security,
            [Description("23")]
            insure
        }

        /// <summary>
        /// 对于A股，从这里开始以下的科目为统一计算型科目
        /// </summary>
        public static string ACalcByCommon = "非经常性损益调整";

        public static string customflagtocalc = "clearformula";

        public static string YTDTPLTABLE = "fin_rpt_tpl_ytd";

        public static string VIRTUALNODE = "-";

        public static string SPACE = " ";

        /// <summary>
        /// 模版界面图标文字说明
        /// </summary>
        public static string BtnAdd = "选择显示数据期";
        public static string BtnCheck = "数据正确性检查";
        public static string BtnFormula = "编辑公式";
        public static string BtnSave = "保存数据";
        public static string BtnDel = "删除数据期";
        public static string BtnAddComment = "为选中单元格添加注释";
        public static string BtnLock = "报表锁定";
        public static string BtnDo = "调整面板科目";
        public static string BtnDelComment = "删除注释";
        public static string BtnSettings = "系统默认值设置";
        public static string BtnPublish = "发布";
        public static string BtnClearEnterzone = "清空录入区域";
        public static string BtnAddCol = "添加一期数据期";
        public static string BtnRefreshjsService = "手动刷新数据服务";
        public static string BtnRefreshTotal = "手动刷新小计,【新增】数据期时【必须】先保存再刷新";
        public static string BtnRatioAdjust = "占比分析%调整";

        //锁的相关提示信息
        public static string Locked = "锁定";
        public static string Released = "释放";

        //面板上判断是点击提取还是确定按钮
        public static string StrPickup = "提取";
        public static string StrConfirm = "确定";

        public const string SaveFailMsg = "保存失败，可能由以下原因造成：\r\n 1、没有要保存的数据\r\n 2、不符合保存的业务逻辑";

        public const string LoadFailMsg = "Ticker无效,请重新输入";
        public const string NoSubcatlogMsg = "该ticker不属于11: 通用， 21: 银行， 22: 证券， 23: 保险四类中任何一类，请确保数据正确！";
    }
}
