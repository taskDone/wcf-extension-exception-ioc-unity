﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;
using System.Runtime.Serialization;

using ChinaScope.CSF.Common;

namespace ChinaScope.CSF.Contract.Entity.ShareHolder
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class Shareholder
    {
        public const string Collection = "base_shareholder";

        /// <summary>
        /// _id
        /// </summary>
        [BsonElement("_id")]
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        /// 报告日期
        /// </summary>
        [BsonElement("edt")]
        [DataMember]
        public DateTime ReportDate { get; set; }

        /// <summary>
        /// 股东
        /// </summary>
        [BsonElement("holder")]
        [DataMember]
        public CommonModel Holders { get; set; }

        /// <summary>
        /// 持股数量
        /// </summary>
        [BsonElement("hldg")]
        [DataMember]
        public Int64 Holdings { get; set; }

        /// <summary>
        /// 第一级机构
        /// </summary>
        [BsonElement("org")]
        [DataMember]
        public CommonModel FistOrg { get; set; }

        /// <summary>
        /// 第二级机构
        /// </summary>
        [BsonElement("sup")]
        [DataMember]
        public CDCNEN SupOrg { get; set; }

        /// <summary>
        /// 股东类型
        /// </summary>
        [BsonElement("typ")]
        [DataMember]
        public CDCNEN HolderType { get; set; }

        /// <summary>
        /// 股份性质
        /// </summary>
        [BsonElement("nature")]
        [DataMember]
        public List<CDCNEN> HolderNature { get; set; }

        /// <summary>
        /// Stock
        /// </summary>
        [BsonElement("stk")]
        [DataMember]
        public StkModel Stock { get; set; }

        ///// <summary>
        ///// 10大流通股东
        ///// </summary>
        //[BsonElement("ttyp")]
        //public int Top10Type { get; set; }

        /// <summary>
        /// 10大流通股东
        /// </summary>
        [BsonElement("ttyp")]
        [DataMember]
        public string Top10Type { get; set; }

        /// <summary>
        /// 持股比例
        /// </summary>
        [BsonElement("ratio")]
        [DataMember]
        public string Ratio { get; set; }

        /// <summary>
        /// 股本变更类型
        /// </summary>
        [BsonElement("chgtyp")]
        [DataMember]
        public string ChangeType { get; set; }

        /// <summary>
        /// 创建人
        /// </summary>
        [BsonElement("cru")]
        [DataMember]
        public string CreatePerson { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [BsonElement("crt")]
        [DataMember]
        public DateTime CreateDT { get; set; }

        /// <summary>
        /// 更新人
        /// </summary>
        [BsonElement("upu")]
        [DataMember]
        public string UpdateUser { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [BsonElement("upt")]
        [DataMember]
        public DateTime UpdateDT { get; set; }


        /// <summary>
        /// 数据源类型
        /// </summary>
        [BsonElement("src")]
        [DataMember]
        public string Src { get; set; }

        /// <summary>
        /// 数据源对应 id
        /// </summary>
        [BsonElement("sid")]
        [DataMember]
        public string Sid { get; set; }
    }
}
