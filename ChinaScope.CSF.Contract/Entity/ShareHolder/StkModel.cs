﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using System.Runtime.Serialization;
using MongoDB.Bson;

using ChinaScope.CSF.Common;

namespace ChinaScope.CSF.Contract.Entity.ShareHolder
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class StkModel
    {
        /// <summary>
        /// Tick
        /// </summary>
        [BsonElement("tick")]
        [DataMember]
        public string Tick { get; set; }

        /// <summary>
        /// cd
        /// </summary>
        [BsonElement("cd")]
        [DataMember]
        public string Code { get; set; }

        /// <summary>
        /// id
        /// </summary>
        [BsonElement("id")]
        [DataMember]
        public string IKey { get; set; }
    }
}
