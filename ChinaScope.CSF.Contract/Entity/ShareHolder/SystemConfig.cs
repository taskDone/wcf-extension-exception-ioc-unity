﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using System.Runtime.Serialization;
using MongoDB.Bson;

using ChinaScope.CSF.Common;

namespace ChinaScope.CSF.Contract.Entity.ShareHolder
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class SystemConfig
    {
        [DataMember]
        public string Host { get; set; }

        [DataMember]
        public string DB { get; set; }

        [DataMember]
        public string User { get; set; }

        [DataMember]
        public string Pwd { get; set; }

        [DataMember]
        public string MatchService { get; set; }
    }
}
