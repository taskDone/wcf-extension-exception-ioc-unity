﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System.Runtime.Serialization;

using ChinaScope.CSF.Common;

namespace ChinaScope.CSF.Contract.Entity.ShareHolder
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class Nature
    {
        public const string Collection = "dict_nature";

        /// <summary>
        /// id
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        [DataMember]
        public ObjectId Id { get; set; }

        /// <summary>
        /// english
        /// </summary>
        [BsonElement("enname")]
        [DataMember]
        public string Ename { get; set; }

        /// <summary>
        /// chinese
        /// </summary>
        [BsonElement("szhname")]
        [DataMember]
        public string Cname { get; set; }

        /// <summary>
        /// code
        /// </summary>
        [BsonElement("code")]
        [DataMember]
        public string Code { get; set; }

        /// <summary>
        /// 对应巨潮数据srccode
        /// </summary>
        [BsonElement("srccode")]
        [DataMember]
        public string SrcCode { get; set; }
    }
}
