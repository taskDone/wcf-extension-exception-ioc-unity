﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;
using System.Runtime.Serialization;
using MongoDB.Bson;

using ChinaScope.CSF.Common;

namespace ChinaScope.CSF.Contract.Entity.ShareHolder
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class MessageFromWS
    {
        [DataMember]
        public string code { get; set; }

        [DataMember]
        public string type { get; set; }

        [DataMember]
        public MessageNode message { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class MessageNode
    {
        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public List<SimpleShareHolder> resultList { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class SimpleShareHolder
    {
        [DataMember]
        public SimpleHolder holder { get; set; }

        [DataMember]
        public SimpleCommonObject sup { get; set; }

        [DataMember]
        public SimpleCommonObject typ { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class SimpleCommonObject
    {
        [DataMember]
        public string cd { get; set; }

        [DataMember]
        public string en { get; set; }

        [DataMember]
        public string szh { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class SimpleHolder
    {
        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string en { get; set; }

        [DataMember]
        public string szh { get; set; }
    }
}
