﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;
using System.Runtime.Serialization;
using MongoDB.Bson;

using ChinaScope.CSF.Common;

namespace ChinaScope.CSF.Contract.Entity.ShareHolder
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class Stock
    {
        public const string Collection = "base_stock";

        /// <summary>
        /// ID
        /// </summary>
        [BsonElement("_id")]
        [DataMember]
        public string TKey { get; set; }


        /// 股票交易代码
        /// </summary>
        [BsonElement("tick")]
        [DataMember]
        public string Ticker { get; set; }

        /// <summary>
        /// 股票交易总数
        /// </summary>
        [BsonElement("share")]
        [DataMember]
        public long Share { get; set; }

        /// <summary>
        /// CSF自动拼接的名称由交易代码 + 市场代码 + 类别代码
        /// </summary>
        [BsonElement("code")]
        [DataMember]
        public string SecCode { get; set; }

        /// <summary>
        /// 公司名称
        /// </summary>
        [BsonElement("org")]
        [DataMember]
        public IDCNEN CompanyName { get; set; }
    }
}
