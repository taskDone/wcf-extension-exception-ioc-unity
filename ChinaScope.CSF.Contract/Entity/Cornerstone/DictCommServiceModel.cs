﻿using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.Cornerstone
{
    /// <summary>
    /// java服务获取DictCommon的Jason对象
    /// </summary>
    public class DictCommServiceModel 
    {

        public string code { get; set; }
        public string type { get; set; }

        public List<DictCommon> message { get; set; }

    }
}
