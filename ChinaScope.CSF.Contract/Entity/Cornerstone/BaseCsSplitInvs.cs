﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChinaScope.CSF.Common;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Cornerstone
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class BaseCsSplitInvs : InvesObj
    {
        /// <summary>
        /// 关联到基石表的code
        /// 唯一性code
        /// </summary>
        [DataMember]
        public string code { get; set; }

        /// <summary>
        /// 中文名
        /// </summary>
        [DataMember]
        public string szhname { get; set; }

        /// <summary>
        /// 发布状态
        /// </summary>
        [DataMember]
        public int stat { get; set; }

        /// <summary>
        /// 发布标识符
        /// </summary>
        [DataMember]
        public string bid { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        [BsonElement("upu")]
        [DataMember]
        public string upu { get; set; }
    }
}
