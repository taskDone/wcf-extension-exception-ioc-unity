﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Cornerstone
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class BaseCsInvestment
    {
        public const string Collection = "base_cs_investment";

        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        [DataMember]
        public ObjectId _id { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        [BsonElement("code")]
        [DataMember]
        public string code { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [BsonElement("name")]
        [DataMember]
        public CNEN name { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [BsonElement("desc")]
        [DataMember]
        public CNEN desc { get; set; }

        /// <summary>
        /// 类型（关联表类型）
        /// 人物、机构等
        /// </summary>
        [BsonElement("cat")]
        [DataMember]
        public string cat { get; set; }

        /// <summary>
        /// 基石投资者本身的机构类型
        /// </summary>
        [BsonElement("typ")]
        [DataMember]
        public string typ { get; set; }



        /// <summary>
        /// 关联基础表ID
        /// </summary>
        [BsonElement("baseid")]
        [DataMember]
        public string baseid { get; set; }


        /// <summary>
        /// 投资详情
        /// </summary>
        [BsonElement("inv")]
        [DataMember]
        public List<InvesObj> inv { get; set; }

        /// <summary>
        /// 投资次数
        /// </summary>
        [BsonElement("times")]
        [DataMember]
        public int times { get; set; }

        /// <summary>
        /// 总额
        /// </summary>
        [BsonElement("sum")]
        [DataMember]
        public double? sum { get; set; }

        /// <summary>
        /// 平均数
        /// </summary>
        [BsonElement("avg")]
        [DataMember]
        public double? avg { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [BsonElement("crt")]
        [DataMember]
        public DateTime? crt { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        [BsonElement("upt")]
        [DataMember]
        public DateTime? upt { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        [BsonElement("upu")]
        [DataMember]
        public string upu { get; set; }

        /// <summary>
        /// 发布状态
        /// </summary>
        [BsonElement("stat")]
        [DataMember]
        public int stat { get; set; }

        /// <summary>
        /// 发布时的标识符
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonElement("bid")]
        [DataMember]
        public string bid { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class InvesObj
    {
        /// <summary>
        /// 指标代码
        /// </summary>
        [BsonElement("secu")]
        [DataMember]
        public string secu { get; set; }

        /// <summary>
        /// 投资日期
        /// </summary>
        [BsonElement("date")]
        [DataMember]
        public DateTime? date { get; set; }

        /// <summary>
        /// 投资的数量与币种
        /// </summary>
        [BsonElement("amt")]
        [DataMember]
        public  ConerStoneAmt amt { get; set; }

        /// <summary>
        /// 价格
        /// </summary>
        [BsonElement("price")]
        [DataMember]
        public Vc  price { get; set; }


        /// <summary>
        /// 股票
        /// </summary>
        [BsonElement("share")]
        [DataMember]
        public long? share { get; set; }

        /// <summary>
        /// 比率
        /// </summary>
        [BsonElement("ratio")]
        [DataMember]
        public double? ratio { get; set; }

        /// <summary>
        /// 解禁日期
        /// </summary>
        [BsonElement("rls")]
        [DataMember]
        public DateTime? rls { get; set; }

        /// <summary>
        /// 频率
        /// </summary>
        [BsonElement("frz")]
        [DataMember]
        public double? frz { get; set; }

        /// <summary>
        /// 投资类型
        /// </summary>
        [BsonElement("invtyp")]
        [DataMember]
        public string invtyp { get; set; }

        /// <summary>
        /// 认购方式
        /// </summary>
        [BsonElement("acq")]
        [DataMember]
        public string acq { get; set; }

        /// <summary>
        /// 直接投资者
        /// </summary>
        [BsonElement("entity")]
        [DataMember]
        public CNEN entity { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class ConerStoneAmt
    {
          /// <summary>
        /// 美元汇率
        /// </summary>
        [BsonElement("usv")]
        [DataMember]
        public double? usv { get; set; }

          /// <summary>
        /// 值
        /// </summary>
        [BsonElement("v")]
        [DataMember]
        public double? v { get; set; }

          /// <summary>
        /// 币种
        /// </summary>
        [BsonElement("c")]
        [DataMember]
        public string c { get; set; }
    }
}
