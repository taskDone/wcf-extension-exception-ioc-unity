﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ChinaScope.CSF.Contract.Entity.FinancialCommon.FinCommon;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace ChinaScope.CSF.Contract.Entity.Publish
{
    public class PublishModel
    {

        public PublishModel()
        {
            DB = "ada";
        }
        public string Table { get; set; }

        public string DB { get; set; }

        public PublishOption Type { get; set; }

        public string Ns
        {
            get
            {
                return DB + "." + Table;
            }
        }

        public string Op { get; set; }

        public Dictionary<string, object> Query { get; set; }

        [BsonElement("indexes")]
        public OplogIndexes Indexers { get; set; }

        [BsonElement("caches")]
        public OplogCaches Caches { get; set; }
    }

    public enum PublishOption
    {
        [EnumMember]
        Local = 1,
        [EnumMember]
        OutWard = 2,

        [EnumMember]
        SH = 3,
        [EnumMember]
        AWS = 4
    }

    /// <summary>
    /// 用于异步发布封装对象
    /// </summary>
    public class PublishParameter
    {
        public Dictionary<string, object> Queries { get; set; }
        public List<OplogIndexer> Indexs { get; set; }
    }

}
