﻿using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Publish
{
    public class OplogIndexer
    {
        public OplogIndexer()
        {
            Params = new Dictionary<string, object>();
        }

        public string indexer { get; set; }

        public string op { get; set; }

        public string method { get; set; }

        [BsonIgnore]
        public Dictionary<string, object> Params { get; set; }

        [IgnoreDataMember, BsonElement("params")]
        public BsonDocument BsonParams
        {
            get
            {
                if (Params != null)
                {
                    return new BsonDocument(Params);
                }
                return null;
            }

        }

    }

    public class OplogIndexes
    {

        public OplogIndexes()
        {
            indexes = new List<OplogIndexer>();
        }


        public int n { get; set; }
      

        public List<OplogIndexer> indexes { get; set; }
    }
}
