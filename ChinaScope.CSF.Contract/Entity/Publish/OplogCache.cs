﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Publish
{
    public class OplogCache
    {
        [BsonIgnoreIfNull]
        public string key { get; set; }
        [BsonIgnoreIfNull]
        public string fields { get; set; }
        [BsonIgnoreIfNull]
        public bool hash { get; set; }
    }

    public class OplogCaches
    {

        public OplogCaches()
        {
            caches = new List<OplogCache>();
        }


        public int n
        {
            get;
            set;
        }

        public List<OplogCache> caches { get; set; }
    }
}
