﻿using System.Runtime.Serialization;
using ChinaScope.CSF.Common;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class MongoMkt : Mongols
    {
        [DataMember]
        public string abbr { get; set; }
    }
}
