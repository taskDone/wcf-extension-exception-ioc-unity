﻿using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    public class ResearchOrg
    {
        public const string Collection = "rr_research_org";

        public ObjectId _id { get; set; }

        public string code { get; set; }

        public CNEN name { get; set; }

        public CNEN abbr { get; set; }

        public string cov { get; set; }

        public List<string> rs { get; set; }

        public int stat { get; set; }   //5.28 jason.su

        [BsonIgnore]
        public string Cn
        {
            get { return name.szh; }
        }

        [BsonIgnore]
        public string En
        {
            get { return name.en; }
        }

        [BsonIgnore]
        public string AbbrCn
        {
            get { return abbr.szh; }
        }

        [BsonIgnore]
        public string AbbrEn
        {
            get { return abbr.en; }
        }
    }
}
