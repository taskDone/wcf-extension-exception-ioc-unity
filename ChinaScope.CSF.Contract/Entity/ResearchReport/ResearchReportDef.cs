﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    public class ResearchReportDef
    {
        public const string Collection = "research_report_def";
        public ObjectId _id { get; set; }

        public CNEN titl { get; set; }
        public string rdt { get; set; }
        public string src { get; set; }
        public string typ { get; set; }
        public string secu { get; set; }
        public CNEN bio { get; set; }
        public string upu { get; set; }
        public DateTime? upt { get; set; }

        public int? stat { get; set; }

        public string url { get; set; }

        public string vn { get; set; }
    }
}
