﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    public class ResearchRpt
    {

        public ResearchRpt()
        {
            aut = new List<CDCNEN>();
            csfr = new CDCNEN();
            ffmt = string.Empty;
            fn = new CNEN();
            ind = new List<Industry>();
            org = new CDCNEN();
            ratchg = string.Empty;
            rating = new CDCNEN();
            reg = new CDCNEN();
            rem = new CNEN();
            secu = new CDCNEN();
            src = new CDCNEN();
            titl = new CNEN();
            typ = new CDCNEN();
            url = string.Empty;
            whoa = string.Empty;
            whopa = string.Empty;
            whop = string.Empty;
            pid = string.Empty;
             


        }
        public const string Collection = "research_report";
        public long _id { get; set; }

        public List<CDCNEN> aut { get; set; }


        private DateTime? ct;
        public DateTime? crt {
            get {
                if (ct == null)
                {
                    return new DateTime();
                }
                else {
                    return ct;
                }
            }
            set {
                ct = value;
            }
        }

        public CDCNEN csfr { get; set; }

        public Int64 ds { get; set; }

        public string ffmt { get; set; }

        public CNEN fn { get; set; }

        public List<Industry> ind { get; set; }

        public CDCNEN org { get; set; }

        public long pgn { get; set; }

        public string ratchg { get; set; }

        public CDCNEN rating { get; set; }

        public DateTime rdt { get; set; }

        public CDCNEN reg { get; set; }

        public CNEN rem { get; set; }

        public CDCNEN secu { get; set; }

        public long size { get; set; }

        public CDCNEN src { get; set; }

        public CNEN titl { get; set; }

        public CDCNEN typ { get; set; }

        public string url { get; set; }

        public string whop { get; set; }

        public string whopa { get; set; }

        public string whoa { get; set; }

        public string pid { get; set; }

        public int? stat { get; set; }
    }

    public class Industry
    {
        public string std { get; set; }
        public List<string> code { get; set; }
        public List<string> en { get; set; }
        public List<string> szh { get; set; } 
    }
}
