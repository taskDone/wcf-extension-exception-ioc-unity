﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
     [DataContract(Namespace = Constants.DataContractNamespace)]
    public class Mongols : CNEN
    {
         [DataMember]
        public string code { get; set; }

         //将Mongo时间转换为本地时间
         [BsonIgnoreIfNull]
         [DataMember]
         [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime? dt {
             get; set;
        }

        //将Mongo时间转换为本地时间
        [BsonIgnoreIfNull]
        [DataMember]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
         public DateTime? edt
         {
             get;
             set;
         }
    }

     //update by 2014-05-07(only apply in base_stock's ls field)
     [DataContract(Namespace = Constants.DataContractNamespace)]
     public class MongolsStr : CNEN
     {
         [DataMember]
         public string code { get; set; }

         [BsonIgnoreIfNull]
         [DataMember]
         public string dt
         {
             get;
             set;
         }

         [BsonIgnoreIfNull]
         [DataMember]
         public string edt
         {
             get;
             set;
         }
     }
}
