﻿using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class MongoAbbr : CNEN
    {
        /// <summary>
        /// py
        /// </summary>
        [BsonElement("py"),BsonIgnoreIfNull]
        [DataMember]
        public string py { get; set; }
    }
}
