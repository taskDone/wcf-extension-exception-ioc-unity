﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    public class Researcher
    {
        public const string Collection = "base_researcher";

        public ObjectId _id { get; set; }

        public string code { get; set; }

        public CNEN name { get; set; }

        public string org { get; set; }

        public string pid { get; set; }

        public string certn { get; set; }

        public int stat { get; set; }

        public string upu { get; set; }

        [BsonIgnoreIfNull]
        public DateTime? upt { get; set; }

        [BsonIgnore]
        public string Cn
        {
            get { return name.szh; }
        }

        [BsonIgnore]
        public string En
        {
            get { return name.en; }
        }

        [BsonIgnore]
        public string orgen
        {
            get
            {
                if (CachesCollection.ResearchOrgs != null)
                {
                    ResearchOrg _org = CachesCollection.ResearchOrgs.Find(x => x.code == org);
                    if (_org != null)
                    {
                        return _org.En;
                    }
                    else
                    {
                        return "";
                    }
                }
                return "";
            }
        }

        [BsonIgnore]
        public string orgcn
        {
            get
            {
                if (CachesCollection.ResearchOrgs != null)
                {
                    ResearchOrg _org = CachesCollection.ResearchOrgs.Find(x => x.code == org);
                    if (_org != null)
                    {
                        return _org.Cn;
                    }
                    else
                    {
                        return "";
                    }
                }
                return "";
            }
        }
    }
}
