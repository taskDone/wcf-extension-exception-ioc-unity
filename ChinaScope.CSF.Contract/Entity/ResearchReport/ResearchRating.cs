﻿using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    public class ResearchRating
    {
        public const string Collection = "rr_rating";

        public int _id { get; set; }

        public string org { get; set; }

        public string rpttyp { get; set; }

        public string rating { get; set; }

        public CDCNEN csfr { get; set; }

        public int stat { get; set; }

        [BsonIgnore]
        public string Cd
        {
            get { return csfr.cd; }
        }

        [BsonIgnore]
        public string Cn
        {
            get { return csfr.szh; }
        }

        [BsonIgnore]
        public string En
        {
            get { return csfr.en; }
        }     
    }
}
