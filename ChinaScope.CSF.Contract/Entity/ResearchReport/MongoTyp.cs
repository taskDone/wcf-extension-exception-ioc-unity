﻿using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    public class MongoTyp
    {
        public const string Collection = "rr_rpttype";

        public ObjectId _id { get; set; }

        public string code { get; set; }

        public List<RS> rs { get; set; }

        public CNEN typ { get; set; }
       
        public int stat { get; set; }

        [BsonIgnore]
        public string EN
        {
            get { return typ.en; }
        }

        [BsonIgnore]
        public string CN
        {
            get { return typ.szh; }
        }



    }

    public class RS {

        public string typ { get; set; }

        public string src { get; set; }
    }
}
