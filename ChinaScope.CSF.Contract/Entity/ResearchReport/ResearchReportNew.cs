﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    public class ResearchReportNew
    {
        public const string Collection = "research_report_new";
        public long _id { get; set; }

        public CNEN titl { get; set; }
        public string rdt { get; set; }
        public string src { get; set; }
        public List<string> aut { get; set; }
        public string typ { get; set; }
        public string csfr { get; set; }
        public string secu { get; set; }
        public Dictionary<string,string> ind { get; set; }
        public string bio { get; set; }//关联到摘要表的id
        public string url { get; set; }
        public string vn { get; set; }//原始披露信息拼凑的字符串

        public int? stat { get; set; }

        public string bid { get; set; }//发布标识

        public string upu { get; set; }
        public DateTime? upt { get; set; }
    }
}
