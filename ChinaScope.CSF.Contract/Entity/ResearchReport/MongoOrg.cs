﻿using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class MongoOrg
    {
       public MongoOrg()
       {
           Code = string.Empty;
           en = string.Empty;
           szh = string.Empty;
       }
        [DataMember]
       [BsonElement("id")]
       public string Code { get; set; }
          [DataMember]
       public string en { get; set; }
          [DataMember]
       public string szh { get; set; }
     
    }
}
