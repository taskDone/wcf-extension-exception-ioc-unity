﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.ResearchReport
{
    public class RrResearcher
    {
        public const string Collection = "rr_researcher";

        public ObjectId _id { get; set; }

        public string code { get; set; }

        public CNEN name { get; set; }

        public int? sex { get; set; }

        public int? edu { get; set; }

        public List<Cert> certn { get; set; }

        public string pht { get; set; }

        public string sid { get; set; }

        public string pid { get; set; }

        public int? stat { get; set; }

        public string upu { get; set; }

        [BsonIgnoreIfNull]
        public DateTime? upt { get; set; }

    }

    public class Cert
    {
        public string no { get; set; }

        public string org { get; set; }

        public int? post { get; set; }

        public string gdt { get; set; }

        public int? ext { get; set; }
    }
}
