﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.KeyWords
{
    /// <summary>
    /// 通用字典表
    /// 这个表中只定义最通用的选项（项目少于20的字典表），用cat来判别不同选项的字典表分类
    /// </summary>
    public class NewsAnalyse
    {
        public const string Collection = "subnews_analyse";

        public ObjectId _id { get; set; }

        //抓取日期
        public string dt { get; set; }

        //关键短语
        public List<KP> kp { get; set; }

        //标题；字符类型
        public string t { get; set; }

        //新闻正文存储的目录地址；字符类型
        public string f { get; set; }

        //行业分类
        public string cat { get; set; }

        //新闻来源
        public string auth { get; set; }

        //新闻权重
        public float pg { get; set; }

        //行业分类
        public List<string> ind { get; set; }

        //主体列表
        public List<string> kw { get; set; }

        //关键词列表
        public List<string> kn { get; set; }

        //网址
        public string url { get; set; }

        //新闻摘要
        public string sum { get; set; }

        //新闻关键字及对应的分类
        public List<KC> kc { get; set; }

        //新闻关键字对应的相关词及分类
        public List<RC> rc { get; set; }

        //是否编辑
        public int edt { get; set; }

        //发布状态
        [BsonIgnoreIfNull]
        public int? stat { get; set; }

        //新闻转载的次数
        public int rn { get; set; }

        //又新加的字段update2014-11-11
        public List<string> inds { get; set; }

        //又新加的字段update2014-11-14
        //主要用于比对url的md5值,查起来比url快
        public string uid { get; set; }

        /// <summary>
        /// 公司代码列表
        /// </summary>
        public List<CW> cw { get; set; }

        //updateby 2014-12-2
        public DateTime upt { get; set; }

        public string upu{ get; set; }

    }

    public  class KP
    {
        //关键短语，字符类型
        public string p { get; set; }

        //关键短语的频率，整型
        public int n { get; set; }

        //关键短语所属类型；列表类型	
        public List<int> c { get; set; }
    }

    public class KC
    {
        //关键短语，字符类型
        public string w { get; set; }

        //关键词所属类型，列表类型
        public List<int> c { get; set; }

        //正负面（-1：负面，1：正面，0：中性）
        public int p { get; set; }

        //关键字对于产品的sam容错
        public List<string> s { get; set; } 
        
        //是否是主体（0否-1是）
        public int i { get; set; }
    }

    public class RC
    {
        //关键短语
        public string w { get; set; }

        //股票代码
        public List<string> tik { get; set; }

        //关键词所属类型，列表类型
        public List<RS> rs { get; set; }
    }

    public class RS
    {
        //相关词
        public string r { get; set; }

        //关键词所属类型，列表类型
        public List<int> c { get; set; }
    }

    public class CW
    {
        //公司代码列表
        public List<string> c { get; set; }
    }
}
