﻿using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.KeyWords
{
    /// <summary>
    /// 通用字典表
    /// 这个表中只定义最通用的选项（项目少于20的字典表），用cat来判别不同选项的字典表分类
    /// </summary>
    public class HotSpot
    {
        public const string Collection = "crawl_hotspot";

        public ObjectId _id { get; set; }

        //抓取日期
        public string dt { get; set; }

        //关键短语
        public string kp { get; set; }

        //来源
        public string src { get; set; }

        //相似来源 similar src
        public string ssrc { get; set; }

        //相似网址
        public string surl { get; set; }

        //title
        public string t { get; set; }

        //标签
        public string tags { get; set; }

        //网址
        public string url { get; set; }

        //权重
        public int w { get; set; }

        //是否编辑
        public int est { get; set; }

        //发布状态
        [BsonIgnoreIfNull]
        public int? stat { get; set; }

        //正文标签
        public string mtgs { get; set; }

        //文章正负面
        //1:正面，0:中立，-1:负面
        [BsonIgnoreIfNull]
        public int? sen { get; set; }

        //作者
        [BsonIgnoreIfNull]
        public string auth { get; set; }

        //分类
        [BsonIgnoreIfNull]
        public string cat { get; set; }

        //主体列表
        [BsonIgnoreIfNull]
        public List<KW> kw { get; set; }



    }

    public class KW
    {
        //主体名称
        //1:正面，0:中立，-1:负面
        [BsonIgnoreIfNull]
        public string o { get; set; }

        //正负面
        [BsonIgnoreIfNull]
        public int? c { get; set; }
    }
}
