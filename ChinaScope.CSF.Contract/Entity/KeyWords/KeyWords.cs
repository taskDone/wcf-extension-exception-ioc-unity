﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.KeyWords
{
    /// <summary>
    /// 通用字典表
    /// 这个表中只定义最通用的选项（项目少于20的字典表），用cat来判别不同选项的字典表分类
    /// </summary>
    public class Keywords
    {
        public const string Collection = "keyword_dict";

        public ObjectId _id { get; set; }

        public string word { get; set; }

        public string prop { get; set; }

        //褒贬中性
        public int? pos { get; set; }

        public int weight { get; set; }

        public List<string> syno { get; set; }

        public List<string> rel { get; set; }

        public int? cat { get; set; }

        [BsonIgnoreIfNull]
        public int? stat { get; set; }

        [BsonIgnoreIfNull]
        public DateTime? upt { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [BsonIgnore]
        public bool? IsDel { get; set; }

    }
}
