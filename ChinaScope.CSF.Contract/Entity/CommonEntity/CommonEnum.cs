﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.FinCommon
{
    /// <summary>
    /// 报表类型
    /// </summary>
    public enum Consolidation
    {
        合并报表 = 1,
        母公司报表 = 2
    }

    /// <summary>
    /// 会计准则变更
    /// </summary>
    public enum AccountStandard
    {
        Type1 = 1,
        Type2 = 2
    }

    /// <summary>
    /// 会计准则变更
    /// </summary>


    /// <summary>
    /// 报表类型
    /// </summary>
    public enum Role
    {
        管理员 = 0,
        可编辑 = 2,
        只读 = 1
    }

    /// <summary>
    /// 角色
    /// </summary>
    public enum Lock
    {
        数据浏览 = 1,
        数据编辑 = 2
    }

    /// <summary>
    /// 季度
    /// </summary>
    public enum Quarter
    {
        Q1 = 1,
        Q2 = 2,
        Q3 = 3,
        Q4 = 4
    }

    /// <summary>
    /// 本期或上期
    /// </summary>
    public enum LastCurrent
    {
        Last = 1,
        Current = 2
    }

    /// <summary>
    /// 数据有效性
    /// </summary>
    public enum Content
    {
        No = 1,
        Yes = 2
    }

    public enum ColumnNamesForQ4
    {
        Q = 16,
        R = 17,
        S = 18,
        T = 19,
        U = 20
    }

    public enum ColumnNamesForQ3
    {
        X = 23,
        Y = 24,
        Z = 25,
        AA = 26,
        AB = 27
    }

    public enum ColumnNamesForQ2
    {
        AE = 30,
        AF = 31,
        AG = 32,
        AH = 33,
        AI = 34
    }

    public enum ColumnNamesForQ1
    {
        AL = 37,
        AM = 38,
        AN = 39,
        AO = 40,
        AP = 41
    }

    /// <summary>
    /// 定义分类类型，默认为0，主要用于关联科目时指定类型做排除
    /// </summary>
    public enum Classify
    {
        Default = 0,//默认分类
        Aaging2_5 = 1,//按账龄分类
        ObjCfy2_5 = 3,//按对象分类
        Imp2_5_2 = 2,//按减值计提方式分类
        CapexFix2_6_3 = 4,//固定资产本期增加
        CapexCip2_6_3 = 5,//在建工程
        CapexIntang2_6_3 = 6,//无形资产本期增加
        CapexLongterm2_6_3 = 7,//长期待摊费用本期增加
        InvPeriod2_7_1 = 8,//按持有期间分类
        InvCategory2_7_1 = 9,//按会计科目分类
        InvHoldper2_7_1 = 10,//按投资类型分类
        DebtPay2_8_1 = 11,//按付款时间分类
        DebtCategory2_8_1 = 12,//按担保类型分类
        DebtCurrency2_8_1 = 13,//按货币分类
        CmtExp2_9_1 = 14,//按经验租赁承诺分类
        CmtOther2_9_1 = 15//按其他承诺事项分类
    }

    /// <summary>
    /// 定义报表类型，一个报表类型又分YTD,CUR,LTM三类
    /// 获取本枚举的值，调用Utils类中的GetDescription方法
    /// </summary>
    public enum ReportSheetName
    {
        [Description("2.1.1")]
        AS_REPORT_211,
        [Description("2.1.2")]
        TEMPLATED_212,
        [Description("2.2.1")]
        AS_REPORT_221,
        [Description("2.2.2")]
        TEMPLATED_222,
        [Description("2.3.1")]
        AS_REPORT_231,
        [Description("2.3.2")]
        TEMPLATED_232
    }

}
