﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.CommonEntity
{
    /// <summary>
    /// 所有通用的类对象
    /// </summary>
    [DataContract(Namespace=Constants.DataContractNamespace)]
    public class CommonModel
    {
    }
    /// <summary>
    /// 中英文
    /// </summary>
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class CNEN
    {
        public CNEN()
        {
            en = "";
            szh = "";
        }
        /// <summary>
        /// 英文备注
        /// </summary>
        [BsonElement("en")]
        [DataMember]
        public string en { get; set; }

        /// <summary>
        /// 中文备注
        /// </summary>
        [BsonElement("szh")]
        [DataMember]
        public string szh { get; set; }

        #region 方法

        public override string ToString()
        {
            return string.Format("{0}-{1}", en, szh);
        }

        #endregion
    }

    public class CNENABBR : CNEN
    {
        /// <summary>
        /// 缩写
        /// </summary>
        [BsonElement("abbr")]
        public string abbr { get; set; }
    }

    public class CNENTIK : CNEN
    {
        public string tik { get; set; }

        public CNENTIK()
        {
            tik = string.Empty;
        }
    }

    public class PYCNEN : CNEN
    {
        public string py { get; set; }

        public PYCNEN()
        {
            py = string.Empty;
        }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class IDCNEN
    {
        public IDCNEN()
        {
            IDAlian = "";
            en = "";
            cn = "";
        }
        /// <summary>
        /// id
        /// </summary>
        [BsonElement("id")]
        [DataMember]
        public string IDAlian { get; set; }

        /// <summary>
        /// english
        /// </summary>
        [BsonElement("en")]
        [DataMember]
        public string en { get; set; }

        /// <summary>
        /// chinese
        /// </summary>
        [BsonElement("szh")]
        [DataMember]
        public string cn { get; set; }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class CDCNEN
    {

        public CDCNEN()
        {
            cd = string.Empty;
            en = string.Empty;
            szh = string.Empty;
        }
        /// <summary>
        /// code
        /// </summary>
        [BsonIgnoreIfNull, BsonElement("code")]
        [DataMember]
        public string code { get; set; }

        /// <summary>
        /// cd 
        /// </summary>
        [BsonIgnoreIfNull, BsonElement("cd")]
        [DataMember]
        public string cd { get; set; }

        /// <summary>
        /// 英文备注
        /// </summary>
        [BsonElement("en")]
        [DataMember]
        public string en { get; set; }

        /// <summary>
        /// 中文备注
        /// </summary>
        [BsonElement("szh")]
        [DataMember]
        public string szh { get; set; }

        #region 方法
        
        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}", cd, en, szh);
        }

        #endregion
    }

    public class TypCDCNEN : CDCNEN
    {
        public string typ { get; set; }

        public TypCDCNEN()
        {
            typ = string.Empty;
        }
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class TypIDCNEN : IDCNEN
    {
         [DataMember]
        public string typ { get; set; }

        //public TypIDCNEN()
        //{
        //    typ = string.Empty;
        //}
    }

    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class CommonCollection
    {
        public List<BsonDocument> ListDocument { get; set; }

        //public TypIDCNEN()
        //{
        //    typ = string.Empty;
        //}
    }

    /// <summary>
    /// 类型与值
    /// </summary>
    public class TypAmt
    {
        /// <summary>
        /// typ
        /// </summary>
        [BsonElement("typ")]
        public string typ { get; set; }

        /// <summary>
        /// amt
        /// </summary>
        [BsonElement("amt")]
        public string amt { get; set; }
    }

    public class CurrAmt
    {
        public CurrAmt()
        {
            curr = string.Empty;
            amt = string.Empty;
        }
        public string curr { get; set; }
        public  string amt { get; set; }
    }

    /// <summary>
    /// 资金数与币种
    /// </summary>
    public class Vc
    {
        /// <summary>
        /// 资金数
        /// </summary>
        [BsonElement("v")]
        [DataMember]
        public double? v { get; set; }

        /// <summary>
        /// 币种
        /// </summary>
        [BsonElement("c")]
        [DataMember]
        public string c { get; set; }
    }

    /// <summary>
    /// 获取自增对象
    /// </summary>
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class Sequence
    {
        public const string Collection = "ids";

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        /// <remarks></remarks>
         [DataMember]
        public ObjectId Id { get; set; }

        /// <summary>
        /// Gets or sets the field_name.
        /// </summary>
        /// <value>The field_name.</value>
        /// <remarks></remarks>
         [DataMember]
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        /// <remarks></remarks>
         [DataMember]
        public long value { get; set; }
    }

    //from pevc
    public class CustomCell
    {
        public string FixRow { get; set; }

        public string Cloumn { get; set; }

        private object _value;

        public object Value
        {
            get { return _value ?? (_value = ""); }
            set { _value = value; }
        }
    }

    /// <summary>
    /// 缓存变量类
    /// </summary>
    public static class CachesCollection
    {
        //TODO:暂时把一些小的缓存放在此处
        private static List<DictShareholderType> _shareholdertypescaches = null;
        /// <summary>
        /// 股东类别表的缓存
        /// </summary>
        public static List<DictShareholderType> ShareHolderTypesCaches
        {
            get
            {
                return _shareholdertypescaches;
            }
            set
            {
                _shareholdertypescaches = value;
            }
        }

        /// <summary>
        /// 研报类型
        /// </summary>
        public static List<ResearchOrg> researchOrgs = null;

        public static List<ResearchOrg> ResearchOrgs
        {
            get
            {
                return researchOrgs;
            }
            set
            {
                researchOrgs = value;
            }
        }


        /// <summary>
        /// 中介类型
        /// </summary>
        public static List<DictIntermediaryType> intermediarytyps = null;

        public static List<DictIntermediaryType> InterMediaryTyps
        {
            get
            {
                return intermediarytyps;
            }
            set
            {
                intermediarytyps = value;
            }
        }

        private static List<BaseStock> baseStocks;


        public static List<BaseStock> BaseStocks
        {
            get
            {
                return baseStocks;
            }
            set
            {
                baseStocks = value;
            }
        }
    }


    /// <summary>
    /// 获取自增对象
    /// </summary>
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class OrgTick
    {
        [DataMember]
        public IDCNEN OrgInfo { get; set; }

        [DataMember]
        public string Tick { get; set; }
    }

    /// <summary>
    /// 录入科目跟踪的原始值和最新值
    /// </summary>
    [Serializable]
    public class TraceItem
    {

        public TraceItem()
        {
            Code = "";
            Value = "";
        }
        /// <summary>
        /// 关联code
        /// </summary>
        [BsonElement("cd")]
        public string Code { get; set; }

        /// <summary>
        /// 关联值
        /// </summary>
        [BsonElement("v")]
        public string Value { get; set; }
    }

    public class CodeCnnRow
    {
        public string Code;
        public int Row;

        public override string ToString()
        {
            return string.Format("{0};{1}", Code, Row);
        }
    }

    public class Name2Value : ICloneable
    {
        public string Name;
        public string Value1;
        public string Value2;
        public string Code;
        public bool IsNew;

        public CodeCnnRow CdeRow { get; set; }

        /// <summary>
        /// 0:关联了标准科目的未match
        /// 1：match 
        /// 2:没有关联标准科目的未match
        /// </summary>
        public int State;

        public string EnterCode;

        public string EnName;

        /// <summary>
        /// nature是为了区分不同数据块而设置的标志
        /// 由于会出现code不同而 名称相同的科目
        /// 而原始科目与标准科目的映射关系是通过名称去识别的
        /// 现在，将 名称+Nature 作为映射项的识别键
        /// </summary>
        public string Nature;

        /// <summary>
        /// 用来表示按什么什么分类的玩意... 0 1 2 3  没有分类就是0 
        /// </summary>
        public string Cat;

        public virtual Object Clone()
        {
            return (Object)this.MemberwiseClone();
        }
    }

    public class BDName2Value : Name2Value
    {
        public BDName2Value()
        {
            BDValue1 = new List<ValueNature>();
            BDValue2 = new List<ValueNature>();
        }
        public List<ValueNature> BDValue1;
        public List<ValueNature> BDValue2;
    }

    public class ValueNature
    {
        public string Value { get; set; }
        public int Nature { get; set; }
        public string Disp { get; set; }
    }

    public class SIName2Value : BDName2Value
    {
        public string FI;

        public string SIType;

        public string CombCode
        {
            get
            {
                return FI + "|" + SIType + "|" + Code;
            }
        }

        public override Object Clone()
        {
            return (Object)this.MemberwiseClone();
        }
    }

    public class QuaterColumus
    {
        public int Q1;
        public int Q2;
        public int Q3;
        public int Q4 = 16;

        public int Columns;

        public QuaterColumus(int accountYears)
        {
            Columns = accountYears;
            Q4 = 16;
            Q2 = Q4 + accountYears + 2;
            Q3 = Q2 + accountYears + 2;
            Q1 = Q3 + accountYears + 2;
        }

        public QuaterColumus(int accountYears, int beginColumnNum)
        {
            Columns = accountYears;
            Q4 = beginColumnNum;
            Q2 = Q4 + accountYears + 2;
            Q3 = Q2 + accountYears + 2;
            Q1 = Q3 + accountYears + 2;
        }

        //just for ltm
        public QuaterColumus(int accountYears, string TempalteName)
        {
            //第一条为隐藏的样式条目
            if (TempalteName == "LTM")
            {
                Columns = accountYears;
                Q4 = 11;
                Q2 = Q4 + accountYears + 2;
                Q3 = Q2 + accountYears + 2;
                Q1 = Q3 + accountYears + 2;
            }
            if (TempalteName == "CURTpl")
            {
                Columns = accountYears;
                Q4 = 11;
                Q2 = Q4 + accountYears + 2;
                Q3 = Q2 + accountYears + 2;
                Q1 = Q3 + accountYears + 2;
            }
        }

    }

    public class CellValueRecord
    {
        public int row { get; set; }
        public int col { get; set; }
        //public int col { get; set; }//目前code统一存放在A列
        public string value { get; set; }
    }
  
    public class NameCnnNature
    {
        public string Name;
        public string Nature;

        public override string ToString()
        {
            return string.Format("{0}-{1}", Name, Nature);
        }

        public class EqualityComparer : IEqualityComparer<NameCnnNature>
        {

            public bool Equals(NameCnnNature x, NameCnnNature y)
            {
                return x.Name == y.Name && x.Nature == y.Nature;
            }

            public int GetHashCode(NameCnnNature x)
            {
                return x.Name.GetHashCode() ^ x.Nature.GetHashCode();
            }
        }
    }

    /// <summary>
    /// 用于保存ItemDef的值
    /// </summary>
    public class NormFinItemValue
    {

        public NormFinItemValue(string Value1, string Value2)
        {
            LastestValue1 = Value1;
            LastestValue2 = Value2;
        }
        /// <summary>
        /// 最新值1
        /// </summary>
        public string LastestValue1 { get; set; }

        /// <summary>
        /// 最新值2
        /// </summary>
        public string LastestValue2 { get; set; }
    }

    public class TitleCode
    {
        /// <summary>
        /// title
        /// </summary>
        public string t { get; set; }

        /// <summary>
        /// code
        /// </summary>
        public string cd { get; set; }
    }

    /// <summary>
    /// 类型与值
    /// </summary>
    public class HidAndHolder
    {
        /// <summary>
        /// hid
        /// </summary>
        public string hid { get; set; }

        /// <summary>
        /// holdername
        /// </summary>
        public string holdername { get; set; }
    }
}
