﻿using ChinaScope.CSF.Contract.Entity.CommonEntity;

namespace ChinaScope.CSF.Contract.Entity.FinancialCommon.FinCommon
{
    /// <summary>
    /// 查询样式表的条件参数
    /// </summary>
    public struct SelectSytleArgs
    {
        public string Ticker;//Ticker

        public Consolidation Ctype;//合并类型

        public AccountStandard StardType;//准则类型

        public string Type;//默认为通用"11"

        public string RptCode; //报表编码

        public string Accp; //YTD、CUR、LTM

        public string Exchg; //A股还是港股
    }

    /// <summary>
    /// 查询数据期数据
    /// </summary>
    public struct SelectPeriodDataArgs
    {
        public string Ticker; //Tocker
        public int Ctype;//合并类型
        public int StandarType;//准则类型
        public int Type;//Latest还是Previous
        public string Report;
        public string Table;

        /// <summary>
        /// 11: 通用， 21: 银行， 22: 证券， 23: 保险
        /// </summary>
        public string ReportType;
    }

    
}
