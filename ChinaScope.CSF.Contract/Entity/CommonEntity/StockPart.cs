﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Security.Tokens;
using System.Text;

namespace ChinaScope.CSF.Contract.Entity.CommonEntity
{
    public class StockPart
    {

        public string _id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string OrgId { get; set; }

        public string OrgName { get; set; }
    }
}
