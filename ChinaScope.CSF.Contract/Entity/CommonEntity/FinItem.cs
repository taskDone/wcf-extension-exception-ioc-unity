﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.CommonEntity
{
    /// <summary>
    /// 财务科目表
    /// </summary>
    public class FinItem : ICloneable
    {
        public const string Collection = "fin_item";

        [BsonElement("_id")]
        public long Id { get; set; }

        /// <summary>
        /// 科目编码
        /// </summary>
        [BsonElement("code")]
        public string ItemCode { get; set; }

        /// <summary>
        /// 中英缩写名
        /// </summary>
        [BsonElement("name")]
        public CNENABBR TransName { get; set; }

        /// <summary>
        /// 86张表的哪张表
        /// </summary>
        [BsonElement("rpt")]
        public string Rpt { get; set; }

        /// <summary>
        /// 明细科目使用，属于哪个主要科目
        /// </summary>
        [BsonElement("parent")]
        public string Parent { get; set; }

        /// <summary>
        /// 1：货币，2：比率；3：倍数
        /// </summary>
        [BsonElement("nat")]
        public string Nat { get; set; }

        /// <summary>
        /// 小数位数
        /// </summary>
        [BsonElement("digit")]
        public int Digit { get; set; }

        /// <summary>
        /// 后缀（百分号，倍数符号）
        /// </summary>
        [BsonElement("suff")]
        public string Suffix { get; set; }

        /// <summary>
        /// 数据点追溯类型（0：不做trace，1：数据追溯，2：公式追溯）
        /// </summary>
        [BsonElement("trace")]
        public string TraceType { get; set; }

        /// <summary>
        /// 单位切换
        /// </summary>
        [BsonElement("utr")]
        public bool UnitTrans { get; set; }

        /// <summary>
        /// 货币切换
        /// </summary>
        [BsonElement("ctr")]
        public bool CurrencyTrans { get; set; }

        /// <summary>
        /// 证券代码
        /// </summary>
        [BsonElement("tik")]
        public string Tik { get; set; }

        /// <summary>
        /// 安全码SecutiryCode
        /// </summary>
        [BsonElement("secu")]
        public string SecurtyCode { get; set; }

        /// <summary>
        /// 对港美股容错
        /// </summary>
        [BsonElement("rs")]
        public List<string> RelationSet { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [BsonElement("rem")]
        public CNEN Remark { get; set; }

        /// <summary>
        ///upt
        /// </summary>
        [BsonElement("upt")]
        public DateTime? upt { get; set; }

        /// <summary>
        /// 安全码SecutiryCode
        /// </summary>
        [BsonElement("upu")]
        public string upu { get; set; }

        /// <summary>
        /// 发布状态
        /// </summary>
        [BsonElement("stat")]
        public int? stat { get; set; }

        /// <summary>
        /// 中文名
        /// </summary>
        [BsonIgnore]
        public string CN
        {
            get
            {
                return TransName.szh;
            }

        }

        /// <summary>
        /// 英文名
        /// </summary>
        [BsonIgnore]
        public string EN
        {
            get
            {
                return TransName.en;
            }

        }

        public Object Clone()
        {
            return (Object)this.MemberwiseClone();
        }
    }
}
