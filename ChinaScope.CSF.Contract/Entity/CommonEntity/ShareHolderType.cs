﻿using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.CommonEntity
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class ShareHolderType
    {
        public const string Collection = "dict_shareholder_type";

        /// <summary>
        /// id
        /// </summary>
        [BsonRepresentation(BsonType.ObjectId)]
        [DataMember]
        public ObjectId Id { get; set; }

        /// <summary>
        /// english
        /// </summary>
        [BsonElement("enname")]
        [DataMember]
        public string Ename { get; set; }

        /// <summary>
        /// chinese
        /// </summary>
        [BsonElement("szhname")]
        [DataMember]
        public string Cname { get; set; }

        /// <summary>
        /// code
        /// </summary>
        [BsonElement("code")]
        [DataMember]
        public string Code { get; set; }
    }
}
