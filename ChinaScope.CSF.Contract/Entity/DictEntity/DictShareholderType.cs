﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    public class DictShareholderType
    {
        public const string Collection = "dict_shareholder_type";

        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        public ObjectId _id { get; set; }

        /// <summary>
        /// code
        /// </summary>
        [BsonElement("code")]
        public string code { get; set; }

        /// <summary>
        /// enname
        /// </summary>
        [BsonElement("enname")]
        public string enname { get; set; }

        /// <summary>
        /// </summary>
        [BsonElement("szhname")]
        public string szhname { get; set; }

        /// <summary>
        /// cat
        /// </summary>
        [BsonElement("cat")]
        public string cat { get; set; }
    }
}
