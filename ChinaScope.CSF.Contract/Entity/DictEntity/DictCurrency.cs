﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    public class DictCurrency
    {
        public const string Collection = "dict_currency";

        public ObjectId _id { get; set; }

        public string code { get; set; }

        public string name { get; set; }

        public string zhsname { get; set; }

        public string region { get; set; }

        public string cov { get; set; }

        public int order { get; set; }
    }
}
