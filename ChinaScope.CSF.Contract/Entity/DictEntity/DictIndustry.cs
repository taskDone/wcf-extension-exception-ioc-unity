﻿using System;
using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    /// <summary>
    /// 对应研报CSFIndustry（行业）
    /// </summary>
    /// <remarks></remarks>
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class DictIndustry
    {
        public const string Collection = "dict_industry";

        [BsonElement("_id")]
        [DataMember]
        public ObjectId Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        /// <remarks></remarks>
        [BsonElement("code")]
        [DataMember]
        public string code { get; set; }

        [BsonElement("ancestors")]
        [DataMember]
        public string[] ancestors { get; set; }

        /// <summary>
        /// Gets or sets the publisher.
        /// </summary>
        /// <value>The publisher.</value>
        /// <remarks></remarks>
        /// <summary>
        /// Gets or sets the cn_name.
        /// </summary>
        /// <value>The cn_name.</value>
        /// <remarks></remarks>
        [BsonElement("zhsname")]
        [DataMember]
        public string zhsname { get; set; }

        /// <summary>
        /// Gets or sets the en_name.
        /// </summary>
        /// <value>The en_name.</value>
        /// <remarks></remarks>
        [BsonElement("ename")]
        [DataMember]
        public string ename { get; set; }

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        /// <value>The parent.</value>
        /// <remarks></remarks>
        [BsonElement("parent")]
        [DataMember]
        public string parent { get; set; }

        [BsonElement("publisher")]
        [DataMember]
        public string publisher { get; set; }

        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        /// <value>The level.</value>
        /// <remarks></remarks>
        [BsonElement("level")]
        [DataMember]
        public long level { get; set; }

        /// <summary>
        /// Gets or sets the en_def.
        /// </summary>
        /// <value>The en_def.</value>
        /// <remarks></remarks>
        [BsonElement("en_def")]
        [DataMember]
        public string en_def { get; set; }

        /// <summary>
        /// Gets or sets the cn_def.
        /// </summary>
        /// <value>The cn_def.</value>
        /// <remarks></remarks>
        [BsonElement("cn_def")]
        [DataMember]
        public string cn_def { get; set; }

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        /// <value>The index.</value>
        /// <remarks></remarks>
        [BsonElement("index")]
        [DataMember]
        public string index { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        /// <value>The comment.</value>
        /// <remarks></remarks>
        [BsonElement("comment")]
        [DataMember]
        public string comment { get; set; }

        /// <summary>
        /// Gets or sets the start_dt.
        /// </summary>
        /// <value>The start_dt.</value>
        /// <remarks></remarks>
        [BsonElement("start_dt")]
        [DataMember]
        public string start_dt { get; set; }

        /// <summary>
        /// Gets or sets the end_dt.
        /// </summary>
        /// <value>The end_dt.</value>
        /// <remarks></remarks>
        [BsonElement("end_dt")]
        [DataMember]
        public string end_dt { get; set; }

        /// <summary>
        /// Gets or sets the gics_code.
        /// </summary>
        /// <value>The gics_code.</value>
        /// <remarks></remarks>
        [BsonElement("gics_code")]
        [DataMember]
        public string gics_code { get; set; }

        /// <summary>
        /// Gets or sets the upd_stmp.
        /// </summary>
        /// <value>The upd_stmp.</value>
        /// <remarks></remarks>
        private DateTime? upds;
        [BsonElement("upd_stmp")]
        [DataMember]
        public DateTime? upd_stmp {
            get
            {
                if (upds == null)
                {
                    return new DateTime();
                }
                else
                {
                    return upds;
                }
            }
            set
            {
                upds = value;
            }
        }
    }
}