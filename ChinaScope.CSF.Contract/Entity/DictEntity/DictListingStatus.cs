﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    public class DictListingStatus
    {
        public const string Collection = "dict_listing_status";

        public string _id { get; set; }

        public string code { get; set; }

        public string ename { get; set; }

        public string zhsname { get; set; }

    }
}
