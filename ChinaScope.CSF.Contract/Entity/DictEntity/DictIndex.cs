﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    public class DictIndex
    {
        public const string Collection = "dict_index";

        public ObjectId _id { get; set; }

        public string idxcd { get; set; }

        public CNEN idxnm { get; set; }

        public List<string> ancestors { get; set; }

        public string parent { get; set; }

        public int type { get; set; }

        public int level { get; set; }

        public string serie { get; set; }

        public string c { get; set; }

        public string samcode { get; set; }

        public int? stat { get; set; }

        public int edit { get; set; } //是否编辑

        public string upu { get; set; }

        public DateTime? upt { get; set; }

        [BsonIgnoreIfNull]
        public string bid { get; set; }

        [BsonIgnore]
        public int? tempid { get; set; }
    }
}
