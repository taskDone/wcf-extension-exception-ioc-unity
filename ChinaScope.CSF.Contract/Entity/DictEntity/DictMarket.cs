﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    public class DictMarket
    {
        public const string Collection = "dict_market";

        public ObjectId _id { get; set; }

        public string abbr { get; set; }
        public CNEN board { get; set; }
        public CDCNEN catlog { get; set; }
        public string code { get; set; }
        public string country { get; set; }
        public string cov { get; set; }
        public string ename { get; set; }
        public CNEN exchange { get; set; }
        public string ls { get; set; }
        public string mkt { get; set; }
        public long order { get; set; }
        public CNEN shorten { get; set; }
        public string zhsname { get; set; }
    }
}
