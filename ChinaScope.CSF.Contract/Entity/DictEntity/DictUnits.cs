﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    public class DictUnits 
    {
        public const string Collection = "dict_units";

        public ObjectId _id { get; set; }

        public string ename { get; set; }

        public string zhsname { get; set; }

        public string abbr { get; set; }

        public string type { get; set; }

        public double ratio { get; set; }

        [BsonElement("ref")]
        public string reference { get; set; }

        public string description { get; set; }

        public string code { get; set; }
    }
}
