﻿using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
   public class DictExecutivePosition
   {
       public const string Collection = "dict_executive_position";

       public ObjectId _id { get; set; }

       public string szhname { get; set; }

       public string enname { get; set; }

       public string code { get; set; }

       public CDCNEN dept { get; set; }

       public int level { get; set; }

       public string type { get; set; }

       public string rs { get; set; }
   }
}
