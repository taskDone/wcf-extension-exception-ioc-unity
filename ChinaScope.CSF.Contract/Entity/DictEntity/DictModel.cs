﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    public class DictModel
    {
        /// <summary>
        /// cd
        /// </summary>
        [BsonElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// en
        /// </summary>
        [BsonElement("ename")]
        public string EName { get; set; }

        /// <summary>
        /// Chinese name
        /// </summary>
        [BsonElement("zhsname")]
        public string CName { get; set; }
    }

    public class DictModelType : DictModel
    {
        /// <summary>
        /// 区分明细表和其他报表，然后绑定字典时可以分类查询
        /// </summary>      
        [BsonElement("Type")]
        public string TYPE
        {
            get;
            set;
        }
    }
}
