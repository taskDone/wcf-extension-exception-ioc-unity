﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
   public class DictRegion
    {
        public const string Collection = "dict_region";

        public ObjectId Id { get; set; }

        public string code { get; set; }
        public string ename { get; set; }
        public string enfull { get; set; }
        public string abbr { get; set; }
        public string zhsname { get; set; }
        public string cnfull { get; set; }
        public long level { get; set; }
        public string[] type { get; set; }
        public string remarks { get; set; }
        public long? order { get; set; }

        public string[] ancestors { get; set; }
        public string parent { get; set; }
    }
}
