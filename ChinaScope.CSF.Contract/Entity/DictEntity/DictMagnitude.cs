﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    public class DictMagnitude
    {
        public const string Collection = "dict_magnitude";

        public ObjectId _id { get; set; }

        public string code { get; set; }

        public string ename { get; set; }

        public string name { get; set; }

        public double scale { get; set; }

        public string zhsname { get; set; }

    }
}
