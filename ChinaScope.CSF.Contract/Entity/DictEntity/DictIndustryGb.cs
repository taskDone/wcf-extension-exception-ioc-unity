﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    public class DictIndustryGb
    {
        public const string Collection = "dict_industry_gb";

        public int _id { get; set; }

        public string year { get; set; }

        public string code { get; set; }

        public string ename { get; set; }

        public string zhsname { get; set; }

        public List<string> ancestors { get; set; }

        public string parent { get; set; }

        public int level { get; set; }

        public string desc { get; set; }

    }
}
