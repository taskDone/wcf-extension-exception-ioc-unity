﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.DictEntity
{
    /// <summary>
    /// 通用字典表
    /// 这个表中只定义最通用的选项（项目少于20的字典表），用cat来判别不同选项的字典表分类
    /// </summary>
    public class DictCommon 
    {
        public const string Collection = "dict_common";
         
        public ObjectId _id { get; set; }

        public string cat { get; set; }

        public bool active { get; set; }

        public string code { get; set; }
        //public string cd { get; set; }

        public CNEN name { get; set; }

        //由于接口返回的是en和szh，故此处不予schema一致而与接口一致
        //public string en { get; set; }

        //public string szh { get; set; }

        public int order { get; set; }

        [BsonIgnoreIfNull]
        public int? stat { get; set; }

        [BsonIgnoreIfNull]
        public string bid { get; set; }

    }
}
