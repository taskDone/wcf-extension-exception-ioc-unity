﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ChinaScope.CSF.Common;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Comps
{
    [DataContract(Namespace = Constants.DataContractNamespace)]
    public class FinCompsMetrics
    {
        public const string Collection = "fin_comps_metrics";

        /// <summary>
        /// id
        /// </summary>
        [BsonElement("_id")]
        [DataMember]
        public ObjectId _id { get; set; }

        /// <summary>
        /// 指标代码
        /// </summary>
        [BsonElement("code")]
        [DataMember]
        public string code { get; set; }

        /// <summary>
        /// 父类节点
        /// </summary>
        [BsonElement("parent")]
        [DataMember]
        public string parent { get; set; }

        /// <summary>
        /// 父类路径
        /// </summary>
        [BsonElement("ancestors")]
        [DataMember]
        public List<string> ancestors { get; set; }

        /// <summary>
        /// 父类路径代码转中文显示
        /// </summary>
        [BsonIgnore]
        [DataMember]
        public List<string> szhancestors { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [BsonElement("name")]
        [DataMember]
        public CNEN name { get; set; }

        /// <summary>
        /// 表名
        /// </summary>
        [BsonElement("tb")]
        [DataMember]
        public string tb { get; set; }

        /// <summary>
        /// 字段名/科目名
        /// </summary>
        [BsonElement("field")]
        [DataMember]
        public string field { get; set; }

        /// <summary>
        /// 显示顺序
        /// </summary>
        [BsonElement("order")]
        [DataMember]
        public long? order { get; set; }

        /// <summary>
        /// 数组
        /// </summary>
        [BsonElement("catlog")]
        [DataMember]
        public List<string> catlog { get; set; }

        /// <summary>
        /// 样式类型
        /// </summary>
        [BsonElement("type")]
        [DataMember]
        public string type { get; set; }

        /// <summary>
        /// 1, - trading  2 - valuation,  3 - financials, 4 - analysis
        /// </summary>
        [BsonElement("class")]
        [DataMember]
        public string alianclass { get; set; }

        /// <summary>
        /// 数据点格式
        /// </summary>
        [BsonElement("display")]
        [DataMember]
        public string display { get; set; }

        /// <summary>
        /// 是否需要被统计
        /// </summary>
        [BsonElement("statis")]
        [DataMember]
        public string statis { get; set; }

        /// <summary>
        /// crt
        /// </summary>
        [BsonElement("crt")]
        [DataMember]
        public DateTime? crt { get; set; }

        /// <summary>
        /// upt
        /// </summary>
        [BsonElement("upt")]
        [DataMember]
        public DateTime? upt { get; set; }

        /// <summary>
        /// upu
        /// </summary>
        [BsonElement("upu")]
        [DataMember]
        public string upu { get; set; }

        /// <summary>
        /// stat
        /// </summary>
        [BsonElement("stat")]
        [DataMember]
        public int? stat { get; set; }

        /// <summary>
        /// bid
        /// </summary>
        [BsonElement("bid")]
        [DataMember]
        public string bid { get; set; }

        /// <summary>
        /// remark备注
        /// update20140828
        /// </summary>
        [BsonElement("remark")]
        [DataMember]
        public CNEN remark { get; set; }

    }
}
