﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.Entity.Index
{
    public class IndexSpecimenStock
    {
        public const string Collection = "index_specimen_stock";


        public ObjectId _id { get; set; }

        public string idxcd { get; set; }

        public List<SpcimenStock> secus { get; set; }

        public string st { get; set; }

        public string et { get; set; }

        public string upu { get; set; }

        public DateTime? upt { get; set; }

        public int stat { get; set; }

    }

    public class SpcimenStock
    {
        public string secu { get; set; }

        /// <summary>
        /// 1：样本股 0：备选股
        /// </summary>
        public int typ { get; set; }

        /// <summary>
        /// 复权因子
        /// </summary>
        public double stfr { get; set; }

    }
}
