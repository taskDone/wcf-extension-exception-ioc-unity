﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Index
{
    public class IndexChange
    {
        public const string Collection = "index_change_his";

        public ObjectId _id { get; set; }

        public string idxcd { get; set; }

        /// <summary>
        /// 变更日期
        /// </summary>
        public string trdt { get; set; }

        public Dictionary<string, Change> chg { get; set; } 

        public string upu { get; set; }

        public DateTime? upt { get; set; }

        public int stat { get; set; }

    }

    public class Change
    {
        public string rz { get; set; }
        public string ele { get; set; }
        public string rt { get; set; }
    }
}
