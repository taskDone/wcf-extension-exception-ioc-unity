﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ChinaScope.CSF.Contract.Entity.Index
{
    public class IndexExChange
    {
        public const string Collection = "index_ex_change";

        public ObjectId _id { get; set; }

        /// <summary>
        /// 公司
        /// </summary>
        public string secu { get; set; }

        /// <summary>
        /// 公司
        /// </summary>
        [BsonIgnore]
        public string secuname { get; set; }

        /// <summary>
        /// 变更日期
        /// </summary>
        public string trdt { get; set; }

        /// <summary>
        /// 变更依据来源
        /// </summary>
        public string src { get; set; }

        /// <summary>
        /// 调整后股本数
        /// </summary>
        public double vary { get; set; }

        /// <summary>
        /// 新市值
        /// </summary>
        public double v { get; set; }

        /// <summary>
        /// 股权报价
        /// </summary>
        public double exv { get; set; }

        public string upu { get; set; }

        public DateTime? upt { get; set; }

        public int stat { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [BsonIgnore]
        public string isdelte { get; set; }

        [BsonIgnore]
        public string statstr
        {
            get
            {
                if (stat == 3)
                {
                    return "已发布";
                }
                else
                {
                    return "未发布";

                }
            }
        }


        private string pid = "";
        [BsonIgnore]
        public string tempid
        {
            get
            {
                int i_id = 0;

                int.TryParse(pid.ToString(), out i_id);

                if (i_id < 0)
                {
                    return pid;
                }
                else
                {
                    pid = string.Format("{0}", _id.ToString());
                    return pid;
                }

            }
            set { pid = value; }
        }

    }
}
