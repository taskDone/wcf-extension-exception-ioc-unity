﻿using System;

namespace ChinaScope.CSF.Contract.Entity.EquityPrice
{
    public class EquityPriceNeeq
    {
        public int ID { get; set; }

        public DateTime TradeDate { get; set; }

        public string SecuCode { get; set; }

        public double Price { get; set; }

        public int Volume { get; set; }

        public double Amount { get; set; }

        public string Buyer { get; set; }

        public string Seller { get; set; }

        public DateTime Upt { get; set; }
    }
}
