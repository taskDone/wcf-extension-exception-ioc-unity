﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.ShareHolder;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.Portfolio
{
    public class DeriveBasePortfolio:BasePortfolio
    {
        /// <summary>
        /// 英文名
        /// </summary>
        public string en
        {
            get { return name.en; }
            set { name.en = value; }
        }

        /// <summary>
        /// 中文名
        /// </summary>
        public string szh
        {
            get { return name.szh; }
            set { name.szh = value; }
        }

        /// <summary>
        /// 机构代码
        /// </summary>
        public string supcd
        {
            get { return sup.cd; }
            set { sup.cd = value; }
        }

        /// <summary>
        /// 机构中文名
        /// </summary>
        public string supszh
        {
            get { return sup.szh; }
            set { sup.szh = value; }
        }
        /// <summary>
        /// 机构英文名
        /// </summary>
        public string supen
        {
            get { return sup.en; }
            set { sup.en = value; }
        }
  
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int)Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int)Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int)Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int)Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int)Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int)Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int)Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int)Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }

        /// <summary>
        /// 类型
        /// </summary>
        public string typstr
        {
            get
            {
                if (CachesCollection.ShareHolderTypesCaches != null)
                {
                    foreach (var typitem in CachesCollection.ShareHolderTypesCaches)
                    {
                        if (typitem.code == typ)
                        {
                            return typitem.szhname;
                        }
                    }
                }

                return typ;
            }
            set
            {
                if (CachesCollection.ShareHolderTypesCaches != null)
                {
                    foreach (var typitem in CachesCollection.ShareHolderTypesCaches)
                    {
                        if (typitem.szhname == value)
                        {
                            typ = typitem.code;
                            break;
                        }
                    }
                }
                else
                {
                    typ = value;
                }
            }
        }


        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                return _id;
            }
            set { _id = value; }
        }
    }
}
