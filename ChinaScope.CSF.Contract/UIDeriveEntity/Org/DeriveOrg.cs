﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using MongoDB.Bson.Serialization.Conventions;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.Org
{
    public class DeriveOrg : BaseOrg
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int)Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int)Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int)Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int)Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int)Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int)Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int)Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int)Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }

        public string tempid
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string CN
        {
            get
            {
                return name.szh;
            }
        }

        public string EN
        {
            get
            {
                return name.en;
            }
        }

        public string SCN
        {
            get
            {
                return abbr.szh;
            }
        }

        public string SEN
        {
            get
            {
                return abbr.en;
            }
        }

        public string CSF
        {
            get
            {
                if (ind != null && ind.csf != null && ind.csf.Count > 0)
                {
                    return ind.csf[0].szh.Last();
                }
                return "";
            }
        }

        public string CSRC
        {
            get
            {
                if (ind != null && ind.csrc != null && ind.csrc.Count > 0)
                {
                    return ind.csrc[0].szh.Last();
                }
                return "";
            }
        }


        public string HS
        {
            get
            {
                if (ind != null && ind.hs != null && ind.hs.Count > 0)
                {
                    return ind.hs[0].szh.Last();
                }
                return "";
            }
        }

        public string ICB
        {
            get
            {
                if (ind != null && ind.icb != null && ind.icb.Count > 0)
                {
                    return ind.icb[0].szh.Last();
                }
                return "";
            }
        }

        public string SYWG
        {
            get
            {
                if (ind != null && ind.sywg != null && ind.sywg.Count > 0)
                {
                    return ind.sywg[0].szh.Last();
                }
                return "";
            }
        }

        public string GB
        {
            get
            {
                if (ind != null && ind.gb != null && ind.gb.Count > 0)
                {
                    return ind.gb[0].szh.Last();
                }
                return "";
            }
        }

        public string RegAddrCn
        {
            get
            {
                if (regaddr != null)
                {
                    return regaddr.szh;
                }

                return "";
            }
        }

        public string RegAddrEn
        {
            get
            {
                if (regaddr != null)
                {
                    return regaddr.en;
                }

                return "";
            }
        }

        public string DT
        {
            get
            {
                if (dt != null)
                {
                    return dt.ToString();
                }

                return "";

            }
        }

        public string RegCn
        {
            get
            {
                if (reg != null)
                {
                    return reg.szh;
                }
                return "";
            }
        }

        public string ContactAddrCn
        {
            get
            {
                if (contact != null && contact.addr != null)
                {
                    return contact.addr.szh;
                }
                return "";
            }
        }

        public string ContactAddrEn
        {
            get
            {
                if (contact != null && contact.addr != null)
                {
                    return contact.addr.en;
                }
                return "";
            }
        }

        public string Fax
        {
            get
            {
                if (contact != null && contact.fax != null)
                {
                    return contact.fax;
                }
                return "";
            }
        }

        public string Url
        {
            get
            {
                if (contact != null && contact.url != null)
                {
                    return contact.url;
                }
                return "";
            }
        }

        public string Tel
        {
            get
            {
                if (contact != null && contact.tel != null)
                {
                    return contact.tel;
                }
                return "";
            }
        }

        public string Mail
        {
            get
            {
                if (contact != null && contact.addr != null)
                {
                    return contact.mail;
                }
                return "";
            }
        }

        public string DescCn
        {
            get
            {
                if (desc != null)
                {
                    return desc.szh;
                }

                return "";

            }
        }

        public string DescEn
        {
            get
            {
                if (desc != null)
                {
                    return desc.en;
                }

                return "";

            }
        }


        /// <summary>
        /// 是否删除
        /// </summary>
        public string del
        {
            get
            {
                if (stat == (int)Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int)Common.Common.ReleaseStatus.Del;
                }
            }
        }


        public string logodesc { get; set; }

    }
}
