﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CorpNature;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.CorpNature
{
    public class DeriveFinCorpNature : FinCorpNature
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int) Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int) Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int) Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int) Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int) Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int) Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }

        /// <summary>
        /// 是否同步巨潮
        /// </summary>
        public string synJCstr
        {
            get
            {
                if (!string.IsNullOrEmpty(sid))
                {
                    return Common.Common.SzhYes;
                }
                return Common.Common.SzhNo;
            }
        }

        /// <summary>
        /// 一级类型
        /// </summary>
        public string typ1level
        {
            get
            {
                string showstat = string.Empty;
                switch (catlog)
                {
                    case "1":
                        showstat = "非金融";
                        break;
                    case "2":
                        showstat = "金融";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "非金融":
                        catlog = "1";
                        break;
                    case "金融":
                        catlog = "2";
                        break;
                }
            }
        }

        /// <summary>
        /// 二级类型
        /// </summary>
        public string typ2level
        {
            get
            {
                string showstat = string.Empty;
                switch (subcatlog)
                {
                    case "11":
                        showstat = "非金融";
                        break;
                    case "21":
                        showstat = "银行";
                        break;
                    case "22":
                        showstat = "证券";
                        break;
                    case "23":
                        showstat = "保险";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "非金融":
                        subcatlog = "11";
                        break;
                    case "银行":
                        subcatlog = "21";
                        break;
                    case "证券":
                        subcatlog = "22";
                        break;
                    case "保险":
                        subcatlog = "23";
                        break;
                }
            }
        }

        /// <summary>
        /// 是否删除
        /// </summary>
        public string del
        {
            get
            {
                if (stat == (int) Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int) Common.Common.ReleaseStatus.Del;
                }
            }
        }

        /// <summary>
        /// 临时ID
        /// </summary>
        public string tempid
        {
            get
            {
                return _id.ToString();
            }
            set { _id = Convert.ToInt64(value); }
        }
    }
}
