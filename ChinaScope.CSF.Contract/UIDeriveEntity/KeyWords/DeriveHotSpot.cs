﻿using System.Collections.Generic;
using System.Linq;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.KeyWords;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.KeyWords
{
    public class DeriveHotSpot : NewsAnalyse
    {
        /// <summary>
        /// 编辑状态
        /// </summary>
        public string editstr
        {
            get; set;
        }

        public string dtstr
        {
            get
            {
                string showdt = string.Empty;
                if (!string.IsNullOrEmpty(dt) && dt.Length >= 14)
                {
                    showdt += dt.Substring(0, 14);
                }
                return showdt;
            }
        }

        /// <summary>
        /// 原始关键词
        /// </summary>
        public string kwlst
        {
            get
            {
                string showkws = string.Empty;
                if (kw != null && kw.Count > 0)
                {
                    foreach (var k in kw)
                    {
                        showkws += k + ",";
                    }
                }
                return showkws;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string[] array = value.Split(',');
                    if (array.Any())
                    {
                        kw.Clear();
                        foreach (var k in array)
                        {
                            if (!string.IsNullOrEmpty(k))
                            {
                                kw.Add(k);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 新增关键词
        /// </summary>
        public string knstr
        {
            get
            {
                string showkns = string.Empty;
                if (kn != null && kn.Count > 0)
                {
                    foreach (var k in kn)
                    {
                        showkns += k + ",";
                    }
                }
                return showkns;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string[] array = value.Split(',');
                    if (array.Any())
                    {
                        kn.Clear();
                        foreach (var k in array)
                        {
                            if (!string.IsNullOrEmpty(k))
                            {
                                kn.Add(k);
                            }
                        }
                    }
                }
                else
                {
                    if (kn != null && kn.Count > 0)
                        kn.Clear();
                    else
                    {
                        kn = new List<string>();
                    }
                }
            }
        }

        /// <summary>
        /// 文章主体
        /// 对于未编辑的新闻：主体来源于kc的满足行业和公司新闻的类别的关键词+正负面
        /// 对于已编辑的新闻：主体来源于编辑的关键词+正负面
        /// </summary>
        public string kwstr
        {
            get;
            set;
        }

        /// <summary>
        /// 标准产品容错
        /// </summary>
        public string kwcstr
        {
            get;
            set;
        }

        /// <summary>
        /// 公司列表字符串
        /// </summary>
        public string cwstr
        {
            get;
            set;
        }

        /// <summary>
        /// 行业
        /// 此行业的值来源于kc中的s,所以设置为只读的
        /// update2014-11-19
        /// </summary>
        public string indstr
        {
            get
            {
                string showind = string.Empty;
                if (ind != null && ind.Count > 0)
                {
                    foreach (var i in ind)
                    {
                        showind += i + ",";
                    }
                }
                return showind;
            }
        }


        /// <summary>
        /// 是否删除
        /// </summary>
        public string isdel
        {
            get
            {
                if (stat == (int) Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int) Common.Common.ReleaseStatus.Del;
                }
            }
        }



        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                if (!string.IsNullOrEmpty(_id.ToString()))
                {
                    _tempid = _id.ToString();
                }
                return _tempid;
            }
            set { _tempid = value; }
        }
    }
}
