﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using ChinaScope.CSF.Contract.Entity.KeyWords;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.KeyWords
{
    public class DeriveKeyWords : Keywords
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                if (_id != BsonObjectId.Empty)
                {
                    showstat = "已编辑";
                }
                else
                {
                    showstat = "新增";
                }
                return showstat;
            }
        }

        /// <summary>
        /// 近义词
        /// </summary>
        public string synostr
        {
            get
            {
                if (syno != null && syno.Count > 0)
                {
                    return string.Join(",", syno);
                }
                return "";
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string[] lst = value.Split(',');
                    if (lst.Any())
                    {
                        syno = lst.ToList();
                    }
                }
                else
                {
                    syno=new List<string>();
                }
            }
        }

        /// <summary>
        /// 相关词
        /// </summary>
        public string relstr
        {
            get
            {
                if (rel != null && rel.Count > 0)
                {
                    return string.Join(",", rel);
                }
                return "";
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    string[] lst = value.Split(',');
                    if (lst.Any())
                    {
                        rel = lst.ToList();
                    }
                }
                else
                {
                    rel = new List<string>();
                }
            }
        }

        /// <summary>
        /// 褒贬
        /// </summary>
        public string posstr
        {
            get
            {
                string showpos = string.Empty;
                switch (pos)
                {
                    case 0:
                        showpos = "中性词";
                        break;
                    case 1:
                        showpos = "褒义词";
                        break;
                    case -1:
                        showpos = "贬义词";
                        break;
                }

                return showpos;
            }
            set
            {
                switch (value)
                {
                    case "中性词":
                        pos = 0;
                        break;
                    case "褒义词":
                        pos = 1;
                        break;
                    case "贬义词":
                        pos = -1;
                        break;
                }
            }
        }

        /// <summary>
        /// 类型的中文名称显示
        /// </summary>
        public string catstr
        {
            get;
            set;
        }


        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                if (_id != BsonObjectId.Empty)
                {
                    _tempid = _id.ToString();
                }
                return _tempid;
            }
            set { _tempid = value; }
        }

        /// <summary>
        /// 褒贬
        /// </summary>
        public string delstr
        {
            get
            {
                if (IsDel != null && (bool) IsDel)
                    return "是";
                return "否";
            }
            set
            {
                switch (value)
                {
                    case "否":
                        IsDel = false;
                        break;
                    case "是":
                        IsDel = true;
                        break;
                }
            }
        }
    }
}
