﻿using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.Comps;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.Comps
{
    public class DeriveFinCompsMetrics : FinCompsMetrics
    {
        /// <summary>
        /// 英文名
        /// </summary>
        public string en
        {
            get { return name.en; }
            set { name.en = value; }
        }

        /// <summary>
        /// 中文名
        /// </summary>
        public string szh
        {
            get { return name.szh; }
            set { name.szh = value; }
        }

        /// <summary>
        /// 拉平的分类路径
        /// </summary>
        public string ancestorstr
        {
            get
            {
                return Common.Common.ConverListToStr(ancestors);
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    ancestors = Common.Common.ConverStrToList(value);
                else
                {
                    ancestors = new List<string>();
                }
            }
        }

        /// <summary>
        /// 拉平的公司类型（用逗号隔开）
        /// </summary>
        public string catlogstr
        {
            get
            {
                return Common.Common.ConverListToStrBySC(catlog);
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    catlog = Common.Common.ConverStrToListBySC(value);
                else
                {
                    catlog = new List<string>();
                }
            }
        }

        /// <summary>
        /// 是否显示
        /// </summary>
        public string statistr
        {
            get
            {
                if (statis != null && statis.ToUpper() == Common.Common.EnYes)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    statis = Common.Common.EnYes;
                }
                else
                {
                    statis = Common.Common.EnNo;
                }
            }
        }

        /// <summary>
        /// 是否删除
        /// </summary>
        public string del
        {
            get
            {
                if (stat == (int)Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int)Common.Common.ReleaseStatus.Del;
                }
            }
        }

        /// <summary>
        /// 发布状态(仅get的时候有效)
        /// </summary>
        public string publish
        {
            get
            {
                string showstate = string.Empty;
                switch (stat)
                {
                    case (int)Common.Common.ReleaseStatus.UnPublish:
                        Common.Common.DiffPublishStatus.TryGetValue((int)Common.Common.ReleaseStatus.UnPublish, out showstate);
                        break;
                    case (int)Common.Common.ReleaseStatus.Published:
                        Common.Common.DiffPublishStatus.TryGetValue((int)Common.Common.ReleaseStatus.Published, out showstate);
                        break;
                    case (int)Common.Common.ReleaseStatus.Del:
                        Common.Common.DiffPublishStatus.TryGetValue((int)Common.Common.ReleaseStatus.Del, out showstate);
                        break;
                    case (int)Common.Common.ReleaseStatus.Audited:
                        Common.Common.DiffPublishStatus.TryGetValue((int)Common.Common.ReleaseStatus.Audited, out showstate);
                        break;
                }
                return showstate;
            }
        }

        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                if (_id != BsonObjectId.Empty)
                {
                    _tempid = _id.ToString();
                }
                return _tempid;
            }
            set { _tempid = value; }
        }
    }
}
