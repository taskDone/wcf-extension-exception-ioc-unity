﻿using System;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.ServiceInterface;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.CED
{
    public class DictCedIndicatorTypePart : IMapping
    {
        public DictCedIndicatorTypePart()
        {
            active = false;
            code = string.Empty;
        }

        [ExcelBinding(ColumnNo = 14)]
        public string _id { get; set; }

        //[ModelAlias(EnumType = typeof(ActiveType))]
        public bool active { get; set; }


        [ExcelBinding(ColumnNo = 9)]
        public string isactive
        {
            get
            {
                if (active)
                    return "否";
                return "是";
            }
            set
            {
                if (value.Equals("是"))
                    active = false;
                else if (value.Equals("否"))
                {
                    active = true;
                }
            }
        }

        [ExcelBinding(ColumnNo = 8)]
        public string code { get; set; }

        [ExcelBinding(ColumnNo = 6)]
        public string szh { get; set; }

        [ExcelBinding(ColumnNo = 7)]
        public string en { get; set; }

        [ExcelBinding(ColumnNo = 5)]
        public string catcd { get; set; }

        [ExcelBinding(ColumnNo = 3)]
        public string catszh { get; set; }

        [ExcelBinding(ColumnNo = 4)]
        public string caten { get; set; }

        public string upu { get; set; }

        public DateTime? upt { get; set; }

        [ModelAlias(EnumType = typeof (PublishType))]
        [ExcelBinding(ColumnNo = 2)]
        public int? stat { get; set; }

        public string bid { get; set; }

        /// <summary>
        /// 由于涉及到两张表的操作，添加一个表字段
        /// </summary>
        [ExcelBinding(ColumnNo = 10)]
        public string frTable { get; set; }

        /// <summary>
        /// 二级排序
        /// </summary>
        [ExcelBinding(ColumnNo = 11)]
        public string order { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [ExcelBinding(ColumnNo = 12)]
        public string isdel { get; set; }
    }

    /// <summary>
    /// CED字典分类信息
    /// </summary>
    public class DiffTableCatsInfo
    {
        //所属表名
        public string Table { get; set; }

        //分类信息（包括中文、英文、编码）
        public CDCNEN Cat { get; set; }
    }

}
