﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.ServiceInterface;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.CED
{
    public class CedIndicatorDataPart : IMapping
    {
        //[ModelAlias(EnumType = typeof(PublishType))]
        //[ExcelBinding(ColumnNo = 2)]
        //public int? stat { get; set; }

        [ExcelBinding(ColumnNo = 3)]
        public string _id { get; set; }

        [ExcelBinding(ColumnNo = 4)]
        public string NameSzh { get; set; }

        [ExcelBinding(ColumnNo = 5)]
        public string Src { get; set; }

        [ExcelBinding(ColumnNo = 6)]
        public string Frm { get; set; }

        [ExcelBinding(ColumnNo = 7)]
        public string Sts { get; set; }

        [ExcelBinding(ColumnNo = 8)]
        public string Sid { get; set; }

        [ExcelBinding(ColumnNo = 9)]
        public string Ytd { get; set; }

        [ExcelBinding(ColumnNo = 10)]
        public string Rec { get; set; }

        [ExcelBinding(ColumnNo = 11)]
        public string S { get; set; }

        [ExcelBinding(ColumnNo = 12)]
        public string U { get; set; }

        [ExcelBinding(ColumnNo = 13)]
        public string C { get; set; }

        [ExcelBinding(ColumnNo = 14)]
        public string Chgyoy { get; set; }

        [ExcelBinding(ColumnNo = 15)]
        public string Chgmom { get; set; }

        public string bid { get; set; }
    }

    //数据一期
    //一条日期对应多个指标code和值
    public class CedIndicatorDataItemPart : IComparable
    {
        //数据期
        public string y { get; set; }

        //指标及数据
        public List<Datacdv> cdv { get; set; }


        public int CompareTo(object obj)
        {
            var entity = obj as CedIndicatorDataItemPart;
            if (entity != null) 
                return string.CompareOrdinal(entity.y, this.y);
            return 1;
        }
    }

    //指标及其值
    public class Datacdv
    {
        //数据值
        public string v { get; set; }

        //对应指标code
        public long _id { get; set; }

        //备注
        public string rem { get; set; }
    }
}
