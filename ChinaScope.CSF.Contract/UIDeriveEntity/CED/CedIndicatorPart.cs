﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.ServiceInterface;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.CED
{
    public class CedIndicatorPart : IMapping
    {
        [ModelAlias(EnumType = typeof(PublishType))]
        [ExcelBinding(ColumnNo = 2)]
        public int? stat { get; set; }

        [ExcelBinding(ColumnNo = 3)]
        public string _id { get; set; }

        [ExcelBinding(ColumnNo = 4)]
        public string NameSzh { get; set; }

        [ExcelBinding(ColumnNo = 5)]
        public string NameEn { get; set; }

        [ExcelBinding(ColumnNo = 6)]
        public string IndCd { get; set; }

        [ExcelBinding(ColumnNo = 7)]
        public string Ind { get; set; }



        [ExcelBinding(ColumnNo = 8)]
        public string TypCd { get; set; }

        [ExcelBinding(ColumnNo = 9)]
        public string Typ { get; set; }

        public List<string> PrdCd;

        [ExcelBinding(ColumnNo = 10)]
        public string PrdCdStr
        {
            get
            {
                if (PrdCd == null)
                {
                    PrdCd = new List<string>();
                }
                return string.Join(",", PrdCd);
            }
            set
            {
                PrdCd = value.Split(',').ToList();
            }
        }

        [ExcelBinding(ColumnNo = 11)]
        public string Prd { get; set; }





        public List<string> RegCd;

        [ExcelBinding(ColumnNo = 12)]
        public string RegCdStr
        {
            get
            {
                if (RegCd == null)
                {
                    RegCd = new List<string>();
                }
                return string.Join(",", RegCd);
            }
            set
            {
                RegCd = value.Split(',').ToList();
            }
        }

        [ExcelBinding(ColumnNo = 13)]
        public string Reg { get; set; }

        public List<string> SrcCd;

        [ExcelBinding(ColumnNo = 14)]
        public string SrcCdStr
        {
            get
            {
                if (SrcCd == null)
                {
                    SrcCd = new List<string>();
                }
                return string.Join(",", SrcCd);
            }
            set
            {
                SrcCd = value.Split(',').ToList();
            }
        }

        [ExcelBinding(ColumnNo = 15)]
        public string Src { get; set; }


        [ExcelBinding(ColumnNo = 16)]
        public string Frq { get; set; }

        public string FrqCd { get; set; }

        [ExcelBinding(ColumnNo = 17)]
        public string S { get; set; }

        public string SCd { get; set; }

        [ExcelBinding(ColumnNo = 18)]
        public string U { get; set; }

        public string UCd { get; set; }

        [ExcelBinding(ColumnNo = 19)]
        public string C { get; set; }

        [ModelAlias(EnumType = typeof(Sts))]
        [ExcelBinding(ColumnNo = 20)]
        public int? Sts { get; set; }

        [ExcelBinding(ColumnNo = 21)]
        public string Fstpd { get; set; }

        [ExcelBinding(ColumnNo = 22)]
        public string Lstpd { get; set; }

        [ExcelBinding(ColumnNo = 23)]
        public string Rdt { get; set; }


        public List<string> RegsCd;

        [ExcelBinding(ColumnNo = 24)]
        public string RegsCdStr
        {
            get
            {
                if (RegsCd == null)
                {
                    RegsCd = new List<string>();
                }
                return string.Join(",", RegsCd);
            }
            set
            {
                RegsCd = value.Split(',').ToList();
            }
        }


        [ExcelBinding(ColumnNo = 25)]
        public string Regs { get; set; }


        [ExcelBinding(ColumnNo = 26)]
        public string Sid { get; set; }

        [ExcelBinding(ColumnNo = 27)]
        public string Condition { get; set; }

        /// <summary>
        /// 是否累积
        /// </summary>
        [ExcelBinding(ColumnNo = 28)]
        public string Ytd { get; set; }

        /// <summary>
        /// 是否标识
        /// </summary>
        [ExcelBinding(ColumnNo = 29)]
        public string Rec { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public string Upu { get; set; }


        /// <summary>
        /// 指标排序
        /// </summary>
        [ExcelBinding(ColumnNo = 30)]
        public string Order { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [ExcelBinding(ColumnNo = 31)]
        public string IsDel { get; set; }

        public string bid { get; set; }

        public string Dr { get; set; }
    }
}
