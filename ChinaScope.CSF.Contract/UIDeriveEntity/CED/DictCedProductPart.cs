﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.ServiceInterface;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.CED
{
    public class DictCedProductPart : IMapping
    {
        public DictCedProductPart()
        {
            RsStr = string.Empty;
            SamCode = string.Empty;

        }

        [ExcelBinding(ColumnNo = 1)]
        public string _id { get; set; }

        [ModelAlias(EnumType = typeof (PublishType))]
        [ExcelBinding(ColumnNo = 2)]
        public int? stat { get; set; }

        [ExcelBinding(ColumnNo = 3)]
        public string NameSzh { get; set; }

        [ExcelBinding(ColumnNo = 4)]
        public string NameEn { get; set; }

        [ExcelBinding(ColumnNo = 5)]
        public string RsStr { get; set; }


        [ExcelBinding(ColumnNo = 6)]
        public string ProductType { get; set; }

        [ExcelBinding(ColumnNo = 7)]
        public long? ParentCode { get; set; }

        [ExcelBinding(ColumnNo = 8)]
        public string ProductSequence { get; set; }

        [ExcelBinding(ColumnNo = 9)]
        public string Active { get; set; }

        [ExcelBinding(ColumnNo = 10)]
        public string SamCode { get; set; }

        public string Upu { get; set; }

        [ExcelBinding(ColumnNo = 12)]
        public string CSFCode { get; set; }

        [ExcelBinding(ColumnNo = 13)]
        public string CSFInd { get; set; }

        [ExcelBinding(ColumnNo = 14)]
        public string GBCode { get; set; }

        [ExcelBinding(ColumnNo = 15)]
        public string GBInd { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        [ExcelBinding(ColumnNo = 11)]
        public string IsDel { get; set; }

        public string bid { get; set; }
    }
}
