﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.Index;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.Index
{
    public class DeriveIndexSpecimenStock : IndexSpecimenStock
    {
        private string pid="";

        public string id
        {
            get
            {
                int i_id = 0;

                int.TryParse(pid.ToString(), out i_id);

                if (i_id < 0)
                {
                    return pid;
                }
                else
                {
                    pid = string.Format("{0}_{1}_{2}_{3}", _id.ToString(),st,et,secu);
                    return pid;
                }
                
            }
            set { pid = value; }
        }

        //public string id { get; set; }

        public string secu { get; set; }
        public string secuname { get; set; }

        public string idxnm { get; set; }

        public double stfr { get; set; }

        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int)Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int)Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int)Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int)Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
        }


        public double stfrmarketcap
        {
            get
            {
                double _marketcap = marketcap * stfr;
                return _marketcap;
            }
        }

        public double marketcap { get; set; }

        public string isspecimenstock { get; set; }

        public string isdelete
        {
            get
            {
                if (stat == (int)Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int)Common.Common.ReleaseStatus.Del;
                }
            }
        }

    }
}
