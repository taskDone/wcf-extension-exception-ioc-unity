﻿using System;
using System.Collections.Generic;
using ChinaScope.CSF.Contract.Entity.Index;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.Index
{
    public class DeriveIndexChange : IndexChange
    {
        private string pid="";

        public string id
        {
            get
            {
                int i_id = 0;

                int.TryParse(pid.ToString(), out i_id);

                if (i_id < 0)
                {
                    return pid;
                }
                else
                {
                    pid = string.Format("{0}_{1}_{2}", _id.ToString(),secu,idxcd);
                    return pid;
                }
                
            }
            set { pid = value; }
        }

        //public string id { get; set; }

        public string secu { get; set; }
        public string secuname { get; set; }

        public string idxnm { get; set; }

        public string rz { get; set; }
        public string ele { get; set; }
        public string rt { get; set; }

        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int)Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int)Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int)Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int)Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
        }

        public string isdelete { get; set; }

    }
}
