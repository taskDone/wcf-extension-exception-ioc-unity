﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.Stock
{
    public class DeriveStock : BaseStock
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int) Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int) Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int) Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int) Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int) Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int) Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }

        /// <summary>
        /// 是否覆盖
        /// </summary>
        public string covstr
        {
            get
            {
                if (cov == Common.Common.IntYes)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    cov = Common.Common.IntYes;
                }
                else if (value.Equals(Common.Common.SzhNo))
                {
                    cov = Common.Common.IntNo;
                }
            }
        }


        /// <summary>
        /// 简介英文名称
        /// </summary>
        public string abbrEN
        {
            get
            {
                if (abbr != null)
                    return abbr.en;
                return "";
            }
            set
            {
                if (abbr != null)
                    abbr.en = value;
                else
                {
                    abbr = new MongoAbbr() {en = value};
                }
            }
        }

        /// <summary>
        /// 简介拼音
        /// </summary>
        public string abbrPY
        {
            get
            {
                if (abbr != null)
                    return abbr.py;
                return "";
            }
            set
            {
                if (abbr != null)
                    abbr.py = value;
                else
                {
                    abbr = new MongoAbbr() {py = value};
                }
            }
        }

        /// <summary>
        /// 名称英文
        /// </summary>
        public string EN
        {
            get
            {
                if (name != null)
                    return name.en;
                return "";
            }
            set
            {
                if (name != null)
                    name.en = value;
                else
                {
                    name = new CNEN() {en = value};
                }
            }
        }

        /// <summary>
        /// 市场
        /// </summary>
        public string mktCN
        {
            get
            {
                if (mkt != null)
                    return mkt.szh;
                return "";
            }
            set
            {
                if (mkt != null)
                    mkt.szh = value;
                else
                {
                    mkt = new MongoMkt() {szh = value};
                }
            }
        }

        /// <summary>
        /// 上市日期
        /// </summary>
        public string lscd
        {
            get
            {
                if (ls != null && !string.IsNullOrEmpty(ls.code))
                    return ls.code.Trim();
                else
                {
                    return "";
                }
            }
            set
            {
                if (ls != null)
                    ls.code = value;
                else
                {
                    ls = new MongolsStr() {code = value};
                }
            }
        }

        /// <summary>
        /// 上市日期
        /// </summary>
        public string lsdt
        {
            get
            {
                if (ls != null && ls.dt != null)
                    return ls.dt.ToString();
                else
                {
                    return "";
                }
            }
            set
            {
                //DateTime setdt;
                if (ls != null)
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        ls.dt = null;
                    }
                   // else if (DateTime.TryParse(value, out setdt))
                    else
                    {
                        ls.dt = value;
                    }
                }
                else
                {
                    //DateTime dt;
                    //if (DateTime.TryParse(value, out dt))
                        ls = new MongolsStr() { dt = value };
                }
            }
        }

        /// <summary>
        /// 退市日期
        /// </summary>
        public string lsedt
        {
            get
            {
                if (ls != null && ls.edt != null)
                    return ls.edt.ToString();
                else
                {
                    return "";
                }
            }
            set
            {

                if (ls != null)
                {
                    DateTime setdt;
                    if (string.IsNullOrEmpty(value))
                    {
                        ls.edt = null;
                    }
                    else //if (DateTime.TryParse(value, out setdt))
                    {
                        ls.edt = value;
                    }
                }
                else
                {
                    //DateTime dt;
                    //if (DateTime.TryParse(value, out dt))
                    ls = new MongolsStr() {edt = value};
                }
            }
        }

        /// <summary>
        /// 是否ADR
        /// </summary>
        public string adrstr
        {
            get
            {
                if (adr)
                {
                    return Common.Common.SzhYes;
                }
                if (!adr)
                {
                    return Common.Common.SzhNo;
                }
                return "";
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    adr = true;
                }
                else if (value.Equals(Common.Common.SzhNo))
                {
                    adr = false;
                }
            }
        }

        /// <summary>
        /// 是否删除
        /// </summary>
        public string del
        {
            get
            {
                if (stat == (int) Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int) Common.Common.ReleaseStatus.Del;
                }
            }
        }
        
        /// <summary>
        /// 是否两地上市
        /// </summary>
        public string dualstr
        {
            get
            {
                if (dual)
                {
                    return Common.Common.SzhYes;
                }
                return Common.Common.SzhNo;
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    dual = true;
                }
                else
                {
                    dual = false;
                }
            }
        }


        /// <summary>
        /// 是否沪港通
        /// </summary>
        public string shhkstr
        {
            get
            {
                if (shhk == "10")
                {
                    return Common.Common.SHTong;
                }
                else if (shhk == "20")
                {
                    return Common.Common.HKTong;
                }
                return "";
            }
            set
            {
                if (value.Equals(Common.Common.SHTong))
                {
                    shhk = "10";
                }
                else if (value.Equals(Common.Common.HKTong))
                {
                    shhk = "20";
                }
                else
                {
                    shhk = "";
                }
            }
        }

        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                return _id;
            }
            set { _id = value; }
        }
    }
}
