﻿using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.PreAnnounceEarning
{
    public class DerivePreAnnEarning : BaseEarningsPreannouncement
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int) Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int) Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int) Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int) Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int) Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int) Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }

        /// <summary>
        /// 是否最新记录
        /// </summary>
        public string newstr
        {
            get
            {
                if (isnew == "1") //1
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    isnew = "1";
                }
                else
                {
                    isnew = "0";
                }
            }
        }

        /// <summary>
        /// 是否删除
        /// </summary>
        public string del
        {
            get
            {
                if (stat == (int)Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int)Common.Common.ReleaseStatus.Del;
                }
            }
        }


        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                if (_id != BsonObjectId.Empty)
                {
                    _tempid = _id.ToString();
                }
                return _tempid;
            }
            set { _tempid = value; }
        }
    }
}
