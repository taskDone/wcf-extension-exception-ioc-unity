﻿using System;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.Cornerstone;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.Conerstone
{
    public class DeriveBaseCsSplitInvs:BaseCsSplitInvs
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int) Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int)Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int)Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int)Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int)Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int)Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int)Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int)Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }

        /// <summary>
        /// 投资实体英文名
        /// </summary>
        public string enentity
        {
            get
            {
                if (entity != null)
                    return entity.en;
                return "";
            }
            set
            {
                if (entity == null)
                    entity = new CNEN();
                entity.en = value;
            }
        }

        /// <summary>
        /// 投资实体中文名
        /// </summary>
        public string szhentity
        {
            get
            {
                if (entity != null)
                    return entity.szh;
                return "";
            }
            set
            {
                if (entity == null)
                    entity = new CNEN();
                entity.szh = value;
            }
        }

        //public string baseidtyp
        //{
        //    get
        //    {
        //        string showtyp = string.Empty;
        //        switch ( typ)
        //        {
        //            case "1":
        //                showtyp = "机构";
        //                break;
        //            case "2":
        //                showtyp = "基金";
        //                break;
        //            case "3":
        //                showtyp = "组合投资";
        //                break;
        //            case "4":
        //                showtyp = "人物";
        //                break;
        //        }
        //        return showtyp;
        //    }
        //    set
        //    {
        //        typ = string.IsNullOrEmpty(value) ? "" : value.Trim();
        //    }
        //}

        public string invtypstr
        {
            get
            {
                string showtyp = string.Empty;
                switch (invtyp)
                {
                    case "1":
                        showtyp = "财务投资者";
                        break;
                    case "2":
                        showtyp = "战略投资者";
                        break;
                    case "3":
                        showtyp = "定向增发";
                        break;
                    case "4":
                        showtyp = "公司配售";
                        break;
                    case "5":
                        showtyp = "机构投资者";
                        break;
                    case "6":
                        showtyp = "基石投资者";
                        break;
                }
                return showtyp;
            }
            set
            {
                string typstr = string.Empty;
                switch (value)
                {
                    case "财务投资者":
                        typstr = "1";
                        break;
                    case "战略投资者":
                        typstr = "2";
                        break;
                    case "定向增发":
                        typstr = "3";
                        break;
                    case "公司配售":
                        typstr = "4";
                        break;
                    case "机构投资者":
                        typstr = "5";
                        break;
                    case "基石投资者":
                        typstr = "6";
                        break;
                }
                invtyp = typstr;
            }
        }

        public string acqstr
        {
            get
            {
                string showacq = string.Empty;
                switch (acq)
                {
                    case "1":
                        showacq = "现金";
                        break;
                    case "2":
                        showacq = "债券";
                        break;
                    case "3":
                        showacq = "股票";
                        break;
                    case "4":
                        showacq = "其他金融资产";
                        break;
                    case "5":
                        showacq = "非金融资产";
                        break;
                }
                return showacq;
            }
            set
            {
                string acqstr = string.Empty;
                switch (value)
                {
                    case "现金":
                        acqstr = "1";
                        break;
                    case "债券":
                        acqstr = "2";
                        break;
                    case "股票":
                        acqstr = "3";
                        break;
                    case "其他金融资产":
                        acqstr = "4";
                        break;
                    case "非金融资产":
                        acqstr = "5";
                        break;
                }
                acq = acqstr;
            }
        }

        /// <summary>
        /// 投资金额（美元）
        /// </summary>
        public string amtusv
        {
            get
            {
                if (amt != null && amt.usv != null)
                    return amt.usv.ToString();
                return "";
            }
            set
            {
                if (amt == null)
                    amt = new ConerStoneAmt();
                if (!string.IsNullOrEmpty(value))
                    amt.usv = Convert.ToDouble(value);
            }
        }


        /// <summary>
        /// 投资数量
        /// </summary>
        public string amtv
        {
            get
            {
                if (amt != null && amt.v != null)
                    return amt.v.ToString();
                return "";
            }
            set
            {
                if (amt == null)
                    amt = new ConerStoneAmt();
                if (!string.IsNullOrEmpty(value))
                    amt.v = Convert.ToDouble(value);
            }
        }

        /// <summary>
        /// 投资币种
        /// </summary>
        public string amtc
        {
            get
            {
                if (amt != null)
                    return amt.c;
                return "";
            }
            set
            {
                if (amt == null)
                    amt = new ConerStoneAmt();
                amt.c = value;
            }
        }

        /// <summary>
        /// 价格数量
        /// </summary>
        public string pricev
        {
            get
            {
                if (price != null)
                    return price.v.ToString();
                return "";
            }
            set
            {
                if (price == null)
                    price = new Vc();
                if (!string.IsNullOrEmpty(value))
                    price.v = Convert.ToDouble(value);
            }
        }

        /// <summary>
        /// 价格币种
        /// </summary>
        public string pricec
        {
            get
            {
                if (price != null)
                    return price.c;
                return "";
            }
            set
            {
                if (price == null)
                    price = new Vc();
                price.c = value;
            }
        }

        /// <summary>
        /// 是否删除
        /// </summary>
        public string isdel
        {
            get
            {
                if (stat == (int) Common.Common.DelStatus.del)
                    return "是";
                else
                {
                    return "否";
                }
            }
            set
            {
                if (value == "是")
                    stat = (int)Common.Common.DelStatus.del;
            }
        }



        /// <summary>
        /// 中文简介
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                if (!string.IsNullOrEmpty(code) && string.IsNullOrEmpty(_tempid))
                {
                    string prefx = Common.Common.GenerateStringID();
                    _tempid = code + prefx;
                }
                return _tempid;
            }
            set { _tempid = value; }
        }

        /// <summary>
        /// 中文简介
        /// </summary>
        private string _comment = string.Empty;
        public string comment
        {
            get
            {
                return "";
            }
            set { _comment = value; }
        }
    }
}
