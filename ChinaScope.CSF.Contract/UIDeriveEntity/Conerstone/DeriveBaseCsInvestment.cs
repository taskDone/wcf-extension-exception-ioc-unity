﻿using System;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.Cornerstone;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.Conerstone
{
    public class DeriveBaseCsInvestment : BaseCsInvestment
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int) Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int) Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int) Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int) Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int) Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int) Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }

        /// <summary>
        /// 英文名
        /// </summary>
        public string en
        {
            get
            {
                if (name != null)
                    return name.en;
                return "";
            }
            set
            {
                if (name == null)
                    name = new CNEN();
                name.en = value;
            }
        }

        /// <summary>
        /// 中文名
        /// </summary>
        public string szh
        {
            get
            {
                if (name != null)
                    return name.szh;
                return "";
            }
            set
            {
                if (name == null)
                    name = new CNEN();
                name.szh = value;
            }
        }

        /// <summary>
        /// 关联类型
        /// </summary>
        public string baseidcat
        {
            get
            {
                string showcat = string.Empty;
                switch (cat)
                {
                    case "1":
                        showcat = "机构";
                        break;
                    case "2":
                        showcat = "基金";
                        break;
                    case "3":
                        showcat = "组合投资";
                        break;
                    case "4":
                        showcat = "人物";
                        break;
                    case "5":
                        showcat = "PEVC机构";
                        break;
                    case "6":
                        showcat = "PEVC基金";
                        break;
                }
                return showcat;
            }
            set
            {
                string catstr = string.Empty;
                switch (value)
                {
                    case "机构":
                        catstr = "1";
                        break;
                    case "基金":
                        catstr = "2";
                        break;
                    case "组合投资":
                        catstr = "3";
                        break;
                    case "人物":
                        catstr = "4";
                        break;
                    case "PEVC机构":
                        catstr = "5";
                        break;
                    case "PEVC基金":
                        catstr = "6";
                        break;
                }
                cat = catstr;
            }
        }

        /// <summary>
        /// 英文简介
        /// </summary>
        public string endes
        {
            get
            {
                if (desc != null)
                    return desc.en;
                return "";
            }
            set
            {
                if (desc == null)
                    desc = new CNEN();
                desc.en = value;
            }
        }

        /// <summary>
        /// 中文简介
        /// </summary>
        public string szhdes
        {
            get
            {
                if (desc != null)
                    return desc.szh;
                return "";
            }
            set
            {
                if (desc == null)
                    desc = new CNEN();
                desc.szh = value;
            }
        }

        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                if (_id != BsonObjectId.Empty)
                {
                    _tempid = _id.ToString();
                }
                return _tempid;
            }
            set { _tempid = value; }
        }

         /// <summary>
        /// 是否删除
        /// </summary>
        public string isdel
        {
            get
            {
                if (stat == (int)Common.Common.DelStatus.del)
                    return "是";
                else
                {
                    return "否";
                }
            }
            set
            {
                if (value == "是")
                    stat = (int)Common.Common.DelStatus.del;
            }
        }    /// <summary>
        /// 关联类型
        /// </summary>
        public string typstr
        {
            get
            {
                string showtyp = string.Empty;
                switch (typ)
                {
                    case "1":
                        showtyp = "风险投资/私募股权投资";
                        break;
                    case "2":
                        showtyp = "母基金";
                        break;
                    case "3":
                        showtyp = "公共养老金";
                        break;
                    case "4":
                        showtyp = "家族基金";
                        break;
                    case "5":
                        showtyp = "大学及基金会";
                        break;
                    case "6":
                        showtyp = "银行保险信托";
                        break;
                    case "7":
                        showtyp = "引导基金";
                        break;
                    case "8":
                        showtyp = "政府机构";
                        break;
                    case "9":
                        showtyp = "主权财富基金";
                        break;
                    case "10":
                        showtyp = "资产管理公司";
                        break;
                    case "11":
                        showtyp = "券商直投";
                        break;
                    case "12":
                        showtyp = "企业投资者";
                        break;
                    case "13":
                        showtyp = "个人投资者";
                        break;
                    case "14":
                        showtyp = "公募基金";
                        break;
                }
                return showtyp;
            }
            set
            {
                string typestr = string.Empty;
                switch (value)
                {
                    case "风险投资/私募股权投资":
                        typestr = "1";
                        break;
                    case "母基金":
                        typestr = "2";
                        break;
                    case "公共养老金":
                        typestr = "3";
                        break;
                    case "家族基金":
                        typestr = "4";
                        break;
                    case "大学及基金会":
                        typestr = "5";
                        break;
                    case "银行保险信托":
                        typestr = "6";
                        break;
                    case "引导基金":
                        typestr = "7";
                        break;
                    case "政府机构":
                        typestr = "8";
                        break;
                    case "主权财富基金":
                        typestr = "9";
                        break;
                    case "资产管理公司":
                        typestr = "10";
                        break;
                    case "券商直投":
                        typestr = "11";
                        break;
                    case "企业投资者":
                        typestr = "12";
                        break;
                    case "个人投资者":
                        typestr = "13";
                        break;
                    case "公募基金":
                        typestr = "14";
                        break;
                }
                typ = typestr;
            }
        }
    }
}
