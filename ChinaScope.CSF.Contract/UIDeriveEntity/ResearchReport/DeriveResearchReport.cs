﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.ResearchReport;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.ResearchReport
{
    public class DeriveResearchReport : ResearchReportNew
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int) Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int) Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int) Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int) Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int) Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int) Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }

        /// <summary>
        /// 研报标题
        /// </summary>
        public string titlstr
        {
            get
            {
                if (titl != null && !string.IsNullOrEmpty(titl.szh))
                    return titl.szh;
                return "";
            }
            set
            {
                if (titl != null)
                {
                    titl.szh = value;
                }
                else
                {
                    titl = new CNEN() {szh = value};
                }
            }
        }

        /// <summary>
        /// 摘要
        /// </summary>
        public string biostr
        {
            get
            {
                if (!string.IsNullOrEmpty(bio))
                {
                    return "打开";
                }
                else
                {
                    return "";
                }
            }
        }

        /// <summary>
        /// 是否删除
        /// </summary>
        public string isdel
        {
            get
            {
                if (stat == (int)Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int)Common.Common.ReleaseStatus.Del;
                }
            }
        }

        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                return _id.ToString();
            }
            set
            {
                long tempid = 0;
                if (long.TryParse(value, out tempid))
                {
                    _id = tempid;
                }
            }
        }

        /// <summary>
        /// 研报类型
        /// </summary>
        public string typstr
        {
            get;
            set;
        }

        /// <summary>
        /// 研报来源机构
        /// </summary>
        public string srcstr
        {
            get;
            set;
        }

        /// <summary>
        /// 研究员姓名
        /// </summary>
        public string autstr
        {
            get;
            set;
        }

        /// <summary>
        /// 数库评级
        /// </summary>
        public string csfrstr
        {
            get;
            set;
        }

        /// <summary>
        /// 数库行业
        /// </summary>
        public string csfindstr { get; set; }
    }
}
