﻿using ChinaScope.CSF.Contract.Entity.BaseEntity;
using ChinaScope.CSF.Contract.Entity.CommonEntity;
using ChinaScope.CSF.Contract.Entity.DictEntity;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.InterMediary
{
    public class DeriveInterMediary : BaseInterMediary
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int) Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int) Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int) Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int) Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int) Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int) Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }

        /// <summary>
        /// 中介ID
        /// </summary>
        public string interID
        {
            get
            {
                if (interOrg != null && !string.IsNullOrEmpty(interOrg.IDAlian))
                {
                    return interOrg.IDAlian;
                }
                return "";
            }
            set
            {
                if (interOrg == null)
                {
                    interOrg = new TypIDCNEN();
                }
                interOrg.IDAlian = value;
            }
        }

        /// <summary>
        /// 中介中文名
        /// </summary>
        public string interCn
        {
            get
            {
                if (interOrg != null && !string.IsNullOrEmpty(interOrg.cn))
                {
                    return interOrg.cn;
                }
                return "";
            }
            set
            {
                if (interOrg == null)
                {
                    interOrg = new TypIDCNEN();
                }
                interOrg.cn = value;
            }
        }

        /// <summary>
        /// 中介英文名
        /// </summary>
        public string interEn
        {
            get
            {
                if (interOrg != null && !string.IsNullOrEmpty(interOrg.en))
                {
                    return interOrg.en;
                }
                return "";
            }
            set
            {
                if (interOrg == null)
                {
                    interOrg = new TypIDCNEN();
                }
                interOrg.en = value;
            }
        }

        /// <summary>
        /// 类型名称
        /// </summary>
        private string _typszh = string.Empty;
        public string TypSzh
        {
            get
            {
                DictIntermediaryType typentity = null;
                if (CachesCollection.InterMediaryTyps != null &&
                    CachesCollection.InterMediaryTyps.Count > 0)
                {
                    typentity = CachesCollection.InterMediaryTyps.Find(x => x.code == interOrg.typ);
                }
                if (typentity != null)
                {
                    _typszh = typentity.zhsname;
                }

                return _typszh;
            }
            set
            {
                _typszh = value;
            }
        }

        /// <summary>
        /// 机构中文名
        /// </summary>
        public string szh
        {
            get
            {
                if (org != null && !string.IsNullOrEmpty(org.cn))
                {
                    return org.cn;
                }
                return "";
            }
            set
            {
                if (org == null)
                {
                    org = new IDCNEN();
                }
                org.cn = value;
            }
        }

        /// <summary>
        /// 英文名
        /// </summary>
        public string en
        {
            get
            {
                if (org != null && !string.IsNullOrEmpty(org.en))
                {
                    return org.en;
                }
                return "";
            }
            set
            {
                if (org == null)
                {
                    org = new IDCNEN();
                }
                org.en = value;
            }
        }

        /// <summary>
        /// 是否删除
        /// </summary>
        public string del
        {
            get
            {
                if (stat == (int) Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int) Common.Common.ReleaseStatus.Del;
                }
            }
        }

        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                if (!string.IsNullOrEmpty(_id))
                {
                    _tempid = _id;
                }
                return _tempid;
            }
            set { _tempid = value; }
        }
    }
}
