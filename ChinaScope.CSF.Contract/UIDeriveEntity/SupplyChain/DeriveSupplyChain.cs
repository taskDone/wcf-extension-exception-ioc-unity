﻿using ChinaScope.CSF.Contract.Entity.BaseEntity;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.SupplyChain
{
    public class DeriveSupplyChain : SupplyChainRelation
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int) Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int) Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int) Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int) Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int) Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int) Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        public string isvalid
        {
            get
            {
                if (valid == (int) Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhNo;
                }
                else
                {
                    return Common.Common.SzhYes;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhNo))
                {
                    valid = (int)Common.Common.DelStatus.del;
                }
                else
                {
                    valid = (int)Common.Common.DelStatus.notdel;
                }
            }
        }


        /// <summary>
        /// 是否删除
        /// </summary>
        public string isdel
        {
            get
            {
                if (stat == (int) Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int) Common.Common.ReleaseStatus.Del;
                }
            }
        }

        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                if (_id != BsonObjectId.Empty)
                {
                    _tempid = _id.ToString();
                }
                return _tempid;
            }
            set
            {
                _tempid = value;
            }
        }

        /// <summary>
        /// 相关产品中文名（上游|下游）
        /// </summary>
        public string rszh
        {
            get;
            set;
        }

        /// <summary>
        /// 相关产品英文名（上游|下游）
        /// </summary>
        public string ren
        {
            get;
            set;
        }

        /// <summary>
        /// 相关产品code（上游|下游）
        /// </summary>
        public string rcode
        {
            get;
            set;
        }

        /// <summary>
        ///上游|下游类别
        /// </summary>
        public string upordown
        {
            get;
            set;
        }
    }
}
