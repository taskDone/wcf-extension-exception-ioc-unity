﻿using ChinaScope.CSF.Contract.Entity.BaseEntity;
using MongoDB.Bson;

namespace ChinaScope.CSF.Contract.UIDeriveEntity.SupplyChain
{
    public class DeriveSupplyDriver : SupplyChainDriverConvert
    {
        /// <summary>
        /// 发布状态
        /// </summary>
        public string statstr
        {
            get
            {
                string showstat = string.Empty;
                switch (stat)
                {
                    case (int) Common.Common.ReleaseStatus.UnPublish:
                        showstat = "未发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Published:
                        showstat = "已发布";
                        break;
                    case (int) Common.Common.ReleaseStatus.Audited:
                        showstat = "已审核";
                        break;
                    case (int) Common.Common.ReleaseStatus.Del:
                        showstat = "已删除";
                        break;
                }
                return showstat;
            }
            set
            {
                switch (value)
                {
                    case "未发布":
                        stat = (int) Common.Common.ReleaseStatus.UnPublish;
                        break;
                    case "已发布":
                        stat = (int) Common.Common.ReleaseStatus.Published;
                        break;
                    case "已审核":
                        stat = (int) Common.Common.ReleaseStatus.Audited;
                        break;
                    case "已删除":
                        stat = (int) Common.Common.ReleaseStatus.Del;
                        break;
                }
            }
        }


        /// <summary>
        /// 是否删除
        /// </summary>
        public string isdel
        {
            get
            {
                if (stat == (int) Common.Common.ReleaseStatus.Del)
                {
                    return Common.Common.SzhYes;
                }
                else
                {
                    return Common.Common.SzhNo;
                }
            }
            set
            {
                if (value.Equals(Common.Common.SzhYes))
                {
                    stat = (int) Common.Common.ReleaseStatus.Del;
                }
            }
        }

        public string typstr
        {
            get
            {
                string showstr = string.Empty;
                switch (type)
                {
                    case 11:
                        showstr = "月度产量";
                        break;
                    case 12:
                        showstr = "月度销量";
                        break;
                    case 13:
                        showstr = "月度价格";
                        break;
                    case 21:
                        showstr = "季度产量";
                        break;
                    case 22:
                        showstr = "季度销量";
                        break;
                    case 23:
                        showstr = "季度价格";
                        break;
                    case 31:
                        showstr = "年度产量";
                        break;
                    case 32:
                        showstr = "年度销量";
                        break;
                    case 33:
                        showstr = "年度价格";
                        break;
                }
                return showstr;
            }
            set
            {
                switch (value)
                {
                    case "月度产量":
                        type = 11;
                        break;
                    case "月度销量":
                        type = 12;
                        break;
                    case "月度价格":
                        type = 13;
                        break;
                    case "季度产量":
                        type = 21;
                        break;
                    case "季度销量":
                        type = 22;
                        break;
                    case "季度价格":
                        type = 23;
                        break;
                    case "年度产量":
                        type = 31;
                        break;
                    case "年度销量":
                        type = 32;
                        break;
                    case "年度价格":
                        type = 33;
                        break;
                }
            }
        }

        /// <summary>
        /// 临时ID
        /// </summary>
        private string _tempid = string.Empty;

        public string tempid
        {
            get
            {
                if (_id != BsonObjectId.Empty)
                {
                    _tempid = _id.ToString() + type.ToString();
                }
                return _tempid;
            }
            set
            {
                _tempid = value;
            }
        }

        public string codestr { get; set; }
        public string cedstr { get; set; }
       
    }
}
